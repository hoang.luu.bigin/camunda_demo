using CloudKit.Domain;
using System;

namespace Module.Sales.Domain
{
    public class Invoice : EntityBase<string>
    {
        public string Name { get; set; }
        public string OrderId { get; set; }

        public InvoiceStatus Status { get; set; } = InvoiceStatus.New;
        public DateTime? DueDate { get; set; }
    }

    public enum InvoiceStatus
    {
        New,
        Approved,
        Rejected,
        Auditor,
    }
}