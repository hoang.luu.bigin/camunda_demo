using CloudKit.Domain;

namespace Module.Sales.Domain
{
    public class Order : EntityBase<string>
    {
        public string Name { get; set; }
        public string CustomerCode { get; set; }

        public OrderStatus Status { get; protected set; }
    }

    public enum OrderStatus
    {
        New,
        OfferCreated,
        NoHeroesAvailable,
        Accepted,
        Rejected,
        Cancelled
    }
}