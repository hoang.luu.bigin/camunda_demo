﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Module.Sales.Domain;
using Module.Sales.Infrastructure.Persistent;
using MongoDB.Driver;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Commands.Invoices
{
    public class ChangeInvoiceStatusCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public InvoiceStatus Status { get; set; }
    }

    public class ChangeInvoiceStatusCommandValidator : AbstractValidator<ChangeInvoiceStatusCommand>
    {
        public ChangeInvoiceStatusCommandValidator()
        {
        }
    }

    public class ChangeInvoiceStatusCommandHandler : IRequestHandler<ChangeInvoiceStatusCommand, bool>
    {
        private readonly SalesDbContext _salesDbContext;

        public ChangeInvoiceStatusCommandHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<bool> Handle(ChangeInvoiceStatusCommand command, CancellationToken cancellationToken)
        {
            var invoice = await _salesDbContext.Invoices.Find(x => x.Id == command.Id).FirstOrDefaultAsync();
            invoice.Status = command.Status;
            invoice.SetUpdatedInfo(DateTime.UtcNow, command.GetEntityId());
            await _salesDbContext.Invoices.ReplaceOneAsync(x => x.Id == invoice.Id, invoice);

            return true;
        }
    }
}
