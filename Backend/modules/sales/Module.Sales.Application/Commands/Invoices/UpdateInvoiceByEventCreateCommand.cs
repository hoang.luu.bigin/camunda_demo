﻿using FluentValidation;
using MediatR;
using Module.Sales.Infrastructure.Persistent;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Commands.Invoices
{
    public class UpdateInvoiceByEventCreateCommand : IRequest<bool>
    {
        public string Id { get; set; }
    }

    public class UpdateInvoiceByEventCreateCommandValidator : AbstractValidator<UpdateInvoiceByEventCreateCommand>
    {
        public UpdateInvoiceByEventCreateCommandValidator()
        {
        }
    }

    public class UpdateInvoiceByEventCreateCommandHandler : IRequestHandler<UpdateInvoiceByEventCreateCommand, bool>
    {
        private readonly SalesDbContext _salesDbContext;

        public UpdateInvoiceByEventCreateCommandHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<bool> Handle(UpdateInvoiceByEventCreateCommand command, CancellationToken cancellationToken)
        {
            return true;
        }
    }
}
