﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Module.Sales.Domain;
using Module.Sales.Infrastructure.Persistent;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Commands.Invoices
{
    public class CreateInvoiceCommand : AuthRequestBase<string>
    {
        public string Name { get; set; }
        public string OrderId { get; set; }
    }

    public class CreateInvoiceCommandValidator : AbstractValidator<CreateInvoiceCommand>
    {
        public CreateInvoiceCommandValidator()
        {
        }
    }

    public class CreateInvoiceCommandHandler : IRequestHandler<CreateInvoiceCommand, string>
    {
        private readonly SalesDbContext _salesDbContext;

        public CreateInvoiceCommandHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<string> Handle(CreateInvoiceCommand command, CancellationToken cancellationToken)
        {
            var invoice = command.Adapt<Invoice>();
            await _salesDbContext.Invoices.InsertOneAsync(invoice);

            return invoice.Id;
        }
    }
}
