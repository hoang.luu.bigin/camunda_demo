﻿using FluentValidation;
using MediatR;
using Module.Sales.Infrastructure.Persistent;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Commands.Orders
{
    public class CancelOrderCommand : IRequest<bool>
    {
        public string OrderId { get; set; }
        public string InvoiceId { get; set; }
    }

    public class CancelOrderCommandValidator : AbstractValidator<CancelOrderCommand>
    {
        public CancelOrderCommandValidator()
        {
        }
    }

    public class CancelOrderCommandHandler : IRequestHandler<CancelOrderCommand, bool>
    {
        private readonly SalesDbContext _salesDbContext;

        public CancelOrderCommandHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<bool> Handle(CancelOrderCommand command, CancellationToken cancellationToken)
        {
            return true;
        }
    }
}
