﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Module.Sales.Domain;
using Module.Sales.Infrastructure.Persistent;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Queries.Orders
{
    public class GetOrdersAllQuery : AuthRequestBase<List<Order>>
    {
    }

    public class GetOrdersAllQueryValidator : AbstractValidator<GetOrdersAllQuery>
    {
        public GetOrdersAllQueryValidator()
        {
        }
    }

    public class GetOrdersAllQueryHandler : IRequestHandler<GetOrdersAllQuery, List<Order>>
    {
        private readonly SalesDbContext _salesDbContext;

        public GetOrdersAllQueryHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<List<Order>> Handle(GetOrdersAllQuery query, CancellationToken cancellationToken)
        {
            var orders = await _salesDbContext.Orders
                .Find(x => true)
                .ToListAsync();

            return orders;
        }

    }
}
