﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Module.Sales.Domain;
using Module.Sales.Infrastructure.Persistent;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Queries.Invoices
{
    public class GetInvoicesAllQuery : AuthRequestBase<List<Invoice>>
    {
    }

    public class GetInvoicesAllQueryValidator : AbstractValidator<GetInvoicesAllQuery>
    {
        public GetInvoicesAllQueryValidator()
        {
        }
    }

    public class GetInvoicesAllQueryHandler : IRequestHandler<GetInvoicesAllQuery, List<Invoice>>
    {
        private readonly SalesDbContext _salesDbContext;

        public GetInvoicesAllQueryHandler(SalesDbContext salesDbContext)
        {
            _salesDbContext = salesDbContext;
        }

        public async Task<List<Invoice>> Handle(GetInvoicesAllQuery query, CancellationToken cancellationToken)
        {            
            var invoices = await _salesDbContext.Invoices
                .Find(x => true)
                .ToListAsync();

            return invoices;
        }

    }
}
