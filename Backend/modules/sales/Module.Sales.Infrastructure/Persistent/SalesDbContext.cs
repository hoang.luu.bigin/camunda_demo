﻿using CloudKit.Infrastructure.Data.MongoDb;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Module.Sales.Domain;
using MongoDB.Driver;

namespace Module.Sales.Infrastructure.Persistent
{
    public class SalesDbSettings : CloudKit.Infrastructure.Data.MongoDb.MongoDatabaseSettings
    {
    }

    public class SalesDbContext : MongoDbBaseContext
    {
        protected override string PrefixTable => "Sales";

        public SalesDbContext(
            IOptionsMonitor<SalesDbSettings> settings,
            ILoggerFactory loggerFactory
            ) : base(settings, loggerFactory)
        {
        }

        public IMongoCollection<Order> Orders { get { return GetCollection<Order>(); } }
        public IMongoCollection<Invoice> Invoices { get { return GetCollection<Invoice>(); } }
    }
}
