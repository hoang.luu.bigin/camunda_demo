﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Module.Sales.Application.Queries.Orders;
using Module.Sales.Infrastructure.Persistent;

[assembly: HostingStartup(typeof(Sales.API.HostingStartup))]
namespace Sales.API
{
    public class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {

                services.Scan(scan =>
                      scan
                          .FromAssembliesOf(typeof(GetOrdersAllQuery))
                          .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                              .AsImplementedInterfaces()
                              .WithTransientLifetime());
                services.AddValidatorsFromAssemblyContaining(typeof(GetOrdersAllQuery), ServiceLifetime.Transient);

                services.AddSingleton<SalesDbContext>();
                services.Configure<SalesDbSettings>(context.Configuration.GetSection("SalesDbSettings"));
            });

            builder.ConfigureAppConfiguration((context, config) =>
            {
            });
        }
    }
}
