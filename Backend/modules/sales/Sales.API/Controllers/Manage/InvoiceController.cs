﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Module.Sales.Application.Commands.Invoices;
using Module.Sales.Application.Queries.Invoices;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace Sales.API.Controllers.Manage
{
    //[EnableCors("AllowOriginsPolicy")]
    [ApiController]
    [Route("v1/Manage/[controller]")]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class InvoiceController : ControllerBase
    {
        private readonly IMediator _mediator;

        public InvoiceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [SwaggerOperation(
            Tags = new[] { "Invoice.Manage" }
        )]
        public async Task<IActionResult> GetAllAsync()
        {
            var rs = await _mediator.Send(new GetInvoicesAllQuery());
            return Ok(rs);
        }

        [HttpPost]
        [SwaggerOperation(
            Tags = new[] { "Invoice.Manage" }
        )]
        public async Task<IActionResult> AddAsync([FromBody] CreateInvoiceCommand command)
        {
            var rs = await _mediator.Send(command);
            return Ok(rs);
        }

        [HttpPut("Status")]
        [SwaggerOperation(
            Tags = new[] { "Invoice.Manage" }
        )]
        public async Task<IActionResult> ChangeStatusAsync([FromBody] ChangeInvoiceStatusCommand command)
        {
            var rs = await _mediator.Send(command);
            return Ok(rs);
        }
    }
}
