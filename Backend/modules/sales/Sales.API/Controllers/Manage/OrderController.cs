﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Module.Sales.Application.Queries.Orders;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace Sales.API.Controllers.Manage
{
    //[EnableCors("AllowOriginsPolicy")]
    [ApiController]
    [Route("v1/Manage/[controller]")]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [SwaggerOperation(
            Tags = new[] { "Order.Manage" }
        )]
        public async Task<IActionResult> GetAllAsync()
        {
            var rs = await _mediator.Send(new GetOrdersAllQuery());
            return Ok(rs);
        }
    }
}
