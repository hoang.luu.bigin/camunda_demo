﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Module.Identity.Domain
{
    public class ShortLiveTokenProviderOptions
    : DataProtectionTokenProviderOptions
    {
        public ShortLiveTokenProviderOptions()
        {
            // update the defaults
            Name = "ShortLiveTokenProvider";
            TokenLifespan = TimeSpan.FromHours(1);
        }
    }
}
