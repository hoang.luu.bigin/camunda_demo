﻿using CloudKit.Domain;
using System;
using System.Collections.Generic;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class AppNavigation : AggregateRootBase<Guid>
    {
        public string? Icon { get; set; }
        public Guid? ParentId { get; set; }
        public NavigationType Type { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
        public string PermissionCode { get; set; }
        public virtual List<AppNavigationLocalization> Localizations { get; set; }

    }

    [Serializable]
    public class AppNavigationLocalization : LocalizationEntityBase
    {
        public Guid AppNavigationId { get; set; }
        public string Name { get; set; }
    }

    public enum NavigationType
    {
        Group = 0,
        Page = 1,
        PageWithRole = 10
    }
} 
