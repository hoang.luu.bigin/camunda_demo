﻿using CloudKit.Domain;
using System;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class AppPermission : AggregateRootBase<Guid>
    {
        public string? Description { get; set; }
        public string RoleName { get; set; }
        public string? Group { get; set; }
        public string Code { get; set; }
    }
}
