﻿namespace Module.Identity.Infrastructure
{
    public class AdminRoles
    {
        public const string TopAdmin = "TopAdmin";
        public const string OverallAdmin = nameof(OverallAdmin);
        public const string DefaultAdmin = "SystemAdmin";
    }
}
