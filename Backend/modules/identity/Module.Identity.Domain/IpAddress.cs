﻿using CloudKit.Domain;
using System;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class IpAddress 
    {
        public string Ip { get; set; }
        public string? Country { get; set; }
        public string? Region { get; set; }
        public string? City { get; set; }
        public string ToAddress()
        {
            string address = "";

            if (Country != null)
                address += Country;
            if (Region != null)
                address += @" / " + Region;
            if (City != null)
                address += @" / " + City;

            return address;
        }
    }
}
