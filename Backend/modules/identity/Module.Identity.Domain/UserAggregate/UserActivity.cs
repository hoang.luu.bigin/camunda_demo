﻿using CloudKit.Domain;
using System;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class UserActivity : AggregateRootBase<Guid>
    {
        public string UserId { get; set; }
        public string? Email { get; set; }
        public ActivityType Type { get; set; } = ActivityType.Common;
        public string Ip { get; set; }
        public string? Location { get; set; }
        public string? Device { get; set; }
        public ActivityResult Result { get; set; } = ActivityResult.Ok;
        public string? Content { get; set; }
        public AppUser User { get; set; }
        public IpAddress IpAddress { get; set; }
    }

    public enum ActivityType
    {
        Common = 0,
        Login = 1,
        LoginWithTwoFactor = 2,
        Logout = 3,
        Register = 4,
        RegisterConfirm = 5,
        ConfirmIp = 6,
        ForgotPassword = 7,
        ResetPassword = 8,
        ChangePasswrod = 9,
        TwoFactorEnable = 10,
        TwoFactorDisable = 11,
        ChangeSettings = 12,
        DeleteDevice = 13,

        AdminChangeUserPassword = 14,
        AdminActiveUser = 15,
        AdminDeleteUser = 16,
        AdminDisableTwoFactor = 17,
        AdminSendEmailConfirm = 18,

        LockDevice = 30,
    }

    public enum ActivityResult
    {
        Fail = 0,
        Ok = 1
    }
}
