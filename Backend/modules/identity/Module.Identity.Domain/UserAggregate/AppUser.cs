﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class AppUser : IdentityUser
    {
        public AppUser() : base() { }
        public string? FullName { get; set; }
        public string? PreferencedLang { get; set; }
        public string? PreferencedTimeZone { get; set; }
        public string? Avatar { get; set; }

        public string EntityType { get; set; }
        public byte SystemId { get; set; } = 0;
        public string EntityCode { get; set; }
        public string? EntityId { get; set; }
        public bool IsActive { get; set; } = true;
        public bool IsDeleted { get; set; } = false;
        public bool ForceChangePassword { get; set; } 
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
        public DateTime? FirstLoginAt { get; set; } 

        public virtual IList<IdentityUserRole<string>> UserRoles { get; set; }

    }

}
