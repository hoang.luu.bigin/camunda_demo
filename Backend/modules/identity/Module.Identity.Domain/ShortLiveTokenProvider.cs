﻿
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Module.Identity.Domain.Models;

namespace Module.Identity.Domain
{
    public class ShortLiveTokenProvider : DataProtectorTokenProvider<AppUser>
    {
        public ShortLiveTokenProvider(
            IDataProtectionProvider dataProtectionProvider,
            IOptions<ShortLiveTokenProviderOptions> options,
            ILogger<ShortLiveTokenProvider> logger)
            : base(dataProtectionProvider, options, logger)
        {

        }
    }
}
