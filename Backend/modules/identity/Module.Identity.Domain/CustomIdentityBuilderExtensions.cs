﻿using Microsoft.AspNetCore.Identity;

namespace Module.Identity.Domain
{
    public static class CustomIdentityBuilderExtensions
    {
        public static IdentityBuilder AddShortLiveTokenProvider(this IdentityBuilder builder)
        {
            return builder.AddTokenProvider("ShortLiveTokenProvider", typeof(ShortLiveTokenProvider));
        }
    }
}
