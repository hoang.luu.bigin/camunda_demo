﻿namespace Module.Identity.Infrastructure
{
    public static class ActivityMessage
    {
        public const string Reset_password_successfull = "Reset password successfully";
        public const string Verification_code_is_invalid = "Verification code is invalid";
        public const string Password_is_not_valid = "Password is not valid";
        public const string Change_password_is_successfull = "Change password is successfully";
        public const string Unknown_Device = "Unknown Device";
        public const string Lock_Device = "Lock Device";
        public const string Log_in_successfull = "Log in successfully";
        public const string Device_confirmation_successfull = "Device confirmation successfully";
        public const string Two_factor_disable_successfull = "Two factor disable successfully";
        public const string Two_factor_enable_successfull = "Two factor enable successfully";
        public const string Logout_successfull = "Logout successfully";
        public const string You_forgot_password = "You forgot password ?";
        public const string Login_with_two_factor_successfull = "Login with two factor successfully";
        public const string Register_successfull = "Register successfully";
        public const string Register_confirmation_successfull = "Register confirmation successfully";
        public const string Login_with_x_account = "Login with {0} account";
        public const string Register_with_x_account = "Register with {0} account";
    }
}
