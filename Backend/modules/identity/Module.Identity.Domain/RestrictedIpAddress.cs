﻿using System;

namespace Module.Identity.Domain.Models
{
    [Serializable]
    public class RestrictedIpAddress
    {
        public string UserId { get; set; }
        public string Ip { get; set; }
        public string Device { get; set; }
        public Guid Token { get; set; }
        public bool IsConfirmed { get; set; }
        public virtual IpAddress IpAddress { get; set; }
    }
}
