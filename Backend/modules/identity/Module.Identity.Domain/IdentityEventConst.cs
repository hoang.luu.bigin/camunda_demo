﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module.Identity.Infrastructure
{
    public static class IdentityEventConst
    {
        public const string GroupName = "Identity";

        public const string ConfirmDistrustIP = "admin-confirm-distrust-ip";
        public const string WebResetPassword = "admin-reset-password";

        public const string UserRegisterConfirm = "user-register-confirm";
        public const string UserResetPassword = "user-reset-password";

        //
        public const string GroupNameWallet = "Wallet";

        public const string WalletChanges = "wallet-changes";
    }
}
