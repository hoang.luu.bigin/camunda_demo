﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Identity.Infrastructure.Migrations
{
    public partial class AppIdentityDbContext_ChangeTypeOfEntityId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "EntityId",
                table: "identity_asp_net_users",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "char(36)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "identity_histories",
                columns: table => new
                {
                    HistoryId = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_histories", x => x.HistoryId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "identity_histories");

            migrationBuilder.AlterColumn<Guid>(
                name: "EntityId",
                table: "identity_asp_net_users",
                type: "char(36)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
