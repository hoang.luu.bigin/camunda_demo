﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Identity.Infrastructure.Migrations
{
    public partial class AppIdentityDbContext_AddForceResetPass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PasswordToken",
                table: "identity_asp_net_users");

            migrationBuilder.DropColumn(
                name: "PasswordTokenExpiration",
                table: "identity_asp_net_users");

            migrationBuilder.AddColumn<bool>(
                name: "ForceChangePassword",
                table: "identity_asp_net_users",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ForceChangePassword",
                table: "identity_asp_net_users");

            migrationBuilder.AddColumn<string>(
                name: "PasswordToken",
                table: "identity_asp_net_users",
                type: "varchar(256) CHARACTER SET utf8mb4",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PasswordTokenExpiration",
                table: "identity_asp_net_users",
                type: "datetime(6)",
                nullable: true);
        }
    }
}
