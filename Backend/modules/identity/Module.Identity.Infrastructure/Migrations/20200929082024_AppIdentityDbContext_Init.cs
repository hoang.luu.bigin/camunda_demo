﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Identity.Infrastructure.Migrations
{
    public partial class AppIdentityDbContext_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "identity_app_navigations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Icon = table.Column<string>(maxLength: 256, nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Url = table.Column<string>(maxLength: 256, nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    PermissionCode = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_app_navigations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "identity_app_permisstions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 128, nullable: true),
                    RoleName = table.Column<string>(maxLength: 64, nullable: false),
                    Group = table.Column<string>(maxLength: 64, nullable: true),
                    Code = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_app_permisstions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_roles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 256, nullable: true),
                    PreferencedLang = table.Column<string>(maxLength: 8, nullable: true),
                    PreferencedTimeZone = table.Column<string>(maxLength: 16, nullable: true),
                    PasswordToken = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordTokenExpiration = table.Column<DateTime>(nullable: true),
                    EntityType = table.Column<string>(nullable: false),
                    SystemId = table.Column<byte>(nullable: false),
                    EntityCode = table.Column<string>(nullable: false),
                    EntityId = table.Column<Guid>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "identity_ip_addresses",
                columns: table => new
                {
                    Ip = table.Column<string>(maxLength: 16, nullable: false),
                    Country = table.Column<string>(maxLength: 512, nullable: true),
                    Region = table.Column<string>(maxLength: 512, nullable: true),
                    City = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_ip_addresses", x => x.Ip);
                });

            migrationBuilder.CreateTable(
                name: "identity_app_navigation_localizations",
                columns: table => new
                {
                    Lang = table.Column<string>(maxLength: 8, nullable: false),
                    AppNavigationId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_app_navigation_localizations", x => new { x.Lang, x.AppNavigationId });
                    table.ForeignKey(
                        name: "FK_identity_app_navigation_localizations_identity_app_navigatio~",
                        column: x => x.AppNavigationId,
                        principalTable: "identity_app_navigations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_role_claims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_role_claims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_identity_asp_net_role_claims_identity_asp_net_roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "identity_asp_net_roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_user_claims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_user_claims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_identity_asp_net_user_claims_identity_asp_net_users_UserId",
                        column: x => x.UserId,
                        principalTable: "identity_asp_net_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_user_logins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_user_logins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_identity_asp_net_user_logins_identity_asp_net_users_UserId",
                        column: x => x.UserId,
                        principalTable: "identity_asp_net_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_user_roles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_user_roles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_identity_asp_net_user_roles_identity_asp_net_roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "identity_asp_net_roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_identity_asp_net_user_roles_identity_asp_net_users_UserId",
                        column: x => x.UserId,
                        principalTable: "identity_asp_net_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_asp_net_user_tokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_asp_net_user_tokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_identity_asp_net_user_tokens_identity_asp_net_users_UserId",
                        column: x => x.UserId,
                        principalTable: "identity_asp_net_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "identity_user_activities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 32, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Ip = table.Column<string>(maxLength: 16, nullable: false),
                    Location = table.Column<string>(maxLength: 512, nullable: true),
                    Device = table.Column<string>(maxLength: 128, nullable: true),
                    Result = table.Column<int>(nullable: false),
                    Content = table.Column<string>(maxLength: 1024, nullable: true),
                    IpAddressIp = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_identity_user_activities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_identity_user_activities_identity_ip_addresses_IpAddressIp",
                        column: x => x.IpAddressIp,
                        principalTable: "identity_ip_addresses",
                        principalColumn: "Ip",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_identity_user_activities_identity_asp_net_users_UserId",
                        column: x => x.UserId,
                        principalTable: "identity_asp_net_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_identity_app_navigation_localizations_AppNavigationId",
                table: "identity_app_navigation_localizations",
                column: "AppNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_identity_asp_net_role_claims_RoleId",
                table: "identity_asp_net_role_claims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "identity_asp_net_roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_identity_asp_net_user_claims_UserId",
                table: "identity_asp_net_user_claims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_identity_asp_net_user_logins_UserId",
                table: "identity_asp_net_user_logins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_identity_asp_net_user_roles_RoleId",
                table: "identity_asp_net_user_roles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "identity_asp_net_users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "identity_asp_net_users",
                column: "NormalizedUserName");

            migrationBuilder.CreateIndex(
                name: "IX_identity_asp_net_users_NormalizedUserName_SystemId",
                table: "identity_asp_net_users",
                columns: new[] { "NormalizedUserName", "SystemId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_identity_user_activities_IpAddressIp",
                table: "identity_user_activities",
                column: "IpAddressIp");

            migrationBuilder.CreateIndex(
                name: "IX_identity_user_activities_UserId",
                table: "identity_user_activities",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "identity_app_navigation_localizations");

            migrationBuilder.DropTable(
                name: "identity_app_permisstions");

            migrationBuilder.DropTable(
                name: "identity_asp_net_role_claims");

            migrationBuilder.DropTable(
                name: "identity_asp_net_user_claims");

            migrationBuilder.DropTable(
                name: "identity_asp_net_user_logins");

            migrationBuilder.DropTable(
                name: "identity_asp_net_user_roles");

            migrationBuilder.DropTable(
                name: "identity_asp_net_user_tokens");

            migrationBuilder.DropTable(
                name: "identity_user_activities");

            migrationBuilder.DropTable(
                name: "identity_app_navigations");

            migrationBuilder.DropTable(
                name: "identity_asp_net_roles");

            migrationBuilder.DropTable(
                name: "identity_ip_addresses");

            migrationBuilder.DropTable(
                name: "identity_asp_net_users");
        }
    }
}
