﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Identity.Infrastructure.Migrations
{
    public partial class AppIdentityDbContext_AddFirstLoginAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FirstLoginAt",
                table: "identity_asp_net_users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstLoginAt",
                table: "identity_asp_net_users");
        }
    }
}
