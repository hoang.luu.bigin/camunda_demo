﻿using _Shared.Application;
using CloudKit.Common.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data.EntityConfigurations;
using System;

namespace Module.Identity.Infrastructure.Data
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser, AppRole, string>
    {
        private const string  _PREFIX = "identity";
        private readonly byte _systemId;

        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options, ISystemIdProvider systemIdProvider)
            : base(options)
        {
            _systemId = systemIdProvider.SystemId;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new AppUserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppRoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new IpAddressEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppPermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppNavigationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppNavigationLocalizationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserActivityEntityTypeConfiguration());

            modelBuilder.Entity<AppUser>()
                .HasQueryFilter(b => (_systemId == 0 || b.SystemId == _systemId) && !b.IsDeleted);

            foreach (IMutableEntityType entity in modelBuilder.Model.GetEntityTypes())
            {
                if (entity.IsOwned())
                    entity.SetTableName((_PREFIX + entity.GetTableName()).ToUnderscoreCase());
            }

            foreach (IMutableEntityType entity in modelBuilder.Model.GetEntityTypes())
            {
                if (!entity.IsOwned())
                    entity.SetTableName((_PREFIX + entity.GetTableName()).ToUnderscoreCase());
            }
        }

        public DbSet<IpAddress> IpAddresses { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<AppPermission> AppPermisstions { get; set; }
        public DbSet<AppNavigation> AppNavigations { get; set; }
        public DbSet<AppNavigationLocalization> AppNavigationLocalizations { get; set; }
        public DbSet<History> Histories { get; set; }
    }

    public class History
    {
        public string HistoryId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
