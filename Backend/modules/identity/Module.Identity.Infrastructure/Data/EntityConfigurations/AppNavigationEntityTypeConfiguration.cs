﻿using CloudKit.Infrastructure.Data.EfCore.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Identity.Domain.Models;

namespace Module.Identity.Infrastructure.Data.EntityConfigurations
{
    internal class AppNavigationEntityTypeConfiguration : EntityConfigurationBase<AppNavigation>
    {
        public override void Configure(EntityTypeBuilder<AppNavigation> builder)
        {
            base.Configure(builder);
            builder.Property(x => x.Icon).HasMaxLength(256);
            builder.Property(x => x.PermissionCode).HasMaxLength(64);
            builder.Property(x => x.Url).HasMaxLength(256);
        }
    }

    internal class AppNavigationLocalizationEntityTypeConfiguration : LocalizationEntityConfigurationBase<AppNavigationLocalization>
    {
        public override void Configure(EntityTypeBuilder<AppNavigationLocalization> builder)
        {
            base.Configure(builder);
            builder.HasKey(x => new {x.Lang, x.AppNavigationId});
            builder.Property(x => x.Name).HasMaxLength(256);
        }
    }
}
