﻿using CloudKit.Infrastructure.Data.EfCore.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Identity.Domain.Models;

namespace Module.Identity.Infrastructure.Data.EntityConfigurations
{
    internal class AppPermissionEntityTypeConfiguration : EntityConfigurationBase<AppPermission>
    {
        public override void Configure(EntityTypeBuilder<AppPermission> builder)
        {
            base.Configure(builder);
            builder.Property(x => x.Code).HasMaxLength(64);
            builder.Property(x => x.Group).HasMaxLength(64);
            builder.Property(x => x.Description).HasMaxLength(128);
            builder.Property(x => x.RoleName).HasMaxLength(64);
        }
    }


}
