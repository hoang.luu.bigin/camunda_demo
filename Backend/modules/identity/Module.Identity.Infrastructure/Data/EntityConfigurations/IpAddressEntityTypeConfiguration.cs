﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Identity.Domain.Models;

namespace Module.Identity.Infrastructure.Data.EntityConfigurations
{
    internal class IpAddressEntityTypeConfiguration : IEntityTypeConfiguration<IpAddress>
    {
        public void Configure(EntityTypeBuilder<IpAddress> builder)
        {
            builder.HasKey(x => x.Ip);
            builder.Property(x => x.Ip).HasMaxLength(16);
            builder.Property(x => x.City).HasMaxLength(512);
            builder.Property(x => x.Country).HasMaxLength(512);
            builder.Property(x => x.Region).HasMaxLength(512);
        }
    }
}
