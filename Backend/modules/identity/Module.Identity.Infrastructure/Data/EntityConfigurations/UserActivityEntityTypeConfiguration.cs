﻿using CloudKit.Infrastructure.Data.EfCore.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Identity.Domain.Models;

namespace Module.Identity.Infrastructure.Data.EntityConfigurations
{
    internal class UserActivityEntityTypeConfiguration : EntityConfigurationBase<UserActivity>
    {
        public override void Configure(EntityTypeBuilder<UserActivity> builder)
        {
            base.Configure(builder);
            builder.Property(x => x.Content).HasMaxLength(1024);
            builder.Property(x => x.Device).HasMaxLength(128);
            builder.Property(x => x.Email).HasMaxLength(128);
            builder.Property(x => x.Location).HasMaxLength(512);
            builder.Property(x => x.Ip).HasMaxLength(16);

        }
    }
}
