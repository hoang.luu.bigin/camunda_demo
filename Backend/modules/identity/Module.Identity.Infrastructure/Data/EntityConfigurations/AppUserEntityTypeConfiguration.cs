﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Identity.Domain.Models;

namespace Module.Identity.Infrastructure.Data.EntityConfigurations
{
    internal class AppUserEntityTypeConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.Property(x => x.FullName).HasMaxLength(256);
            builder.Property(x => x.Avatar).HasMaxLength(256);
            builder.Property(x => x.PreferencedLang).HasMaxLength(8);
            builder.Property(x => x.PreferencedTimeZone).HasMaxLength(16);

            // Each User can have many entries in the UserRole join table
            builder.HasMany(e => e.UserRoles)
                .WithOne()
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.HasIndex(p => p.NormalizedUserName).IsUnique(false);
            builder.HasIndex(p => new { p.NormalizedUserName, p.SystemId }).IsUnique();
            
        }
    }
}
