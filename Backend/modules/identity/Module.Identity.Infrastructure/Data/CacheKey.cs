﻿namespace Module.Identity.Infrastructure.Data
{
    public static class CacheKey
    {
        public const string CachePrefix = "";

        public const string Identity_IpAddress = CachePrefix + "Identity_IpAd_";
        public const string Identity_RestrictedIpAddress = CachePrefix + "Identity_ReIpAd_";

    }
}
