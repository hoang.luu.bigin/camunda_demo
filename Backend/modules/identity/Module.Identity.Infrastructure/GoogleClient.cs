﻿using Newtonsoft.Json;
using RestSharp;
using System.Threading.Tasks;

namespace Module.Identity.Infrastructure
{
    public class GoogleClient
    {
        private readonly RestClient _restClient;

        public GoogleClient()
        {
            _restClient = new RestClient("https://www.googleapis.com/oauth2/v1");
        }

        public async Task<GoogleUser> GetUserInfo(string accessToken)
        {
            var request = new RestRequest("userinfo", Method.GET);
            request.AddQueryParameter("access_token", accessToken);

            var response = await _restClient.ExecuteGetTaskAsync<GoogleUser>(request);

            if (!response.IsSuccessful)
                return null;

            return response.Data;
        }
    }

    public class GoogleUser
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("verified_email")]
        public bool VerifiedEmail { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("given_name")]
        public string GivenName { get; set; }

        [JsonProperty("family_name")]
        public string FamilyName { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }
    }
}
