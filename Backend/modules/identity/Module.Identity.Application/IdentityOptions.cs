﻿namespace Module.Identity.Application
{
    public class IdentityOptions
    {
        public string? EntityType { get; set; }
        public string ResetPasswordTitle { get; set; }
        public string LinkResetPasswordPattern { get; set; }
        public string ResetPasswordTemplatePath { get; set; }
        public string ResetPasswordLogo { get; set; }
        public string ResetPasswordLogoB { get; set; }
        public string ResetPasswordBackground { get; set; }
        public string ResetPasswordAddress { get; set; }
        public string ResetPasswordEmailAddress { get; set; }
        public IdentityActivateMethod ActivateMethod { get; set; }
        public string LinkConfirmEmailPattern { get; set; }
        public string ConfirmEmailTemplatePath { get; set; }
        public string ConfirmEmailTitle { get;  set; }
        public string RandomPasswordEmailTemplatePath { get; set; }
        public string RandomPasswordEmailTitle { get; set; }
        public string LoginLink { get; set; }
        public bool AllowUpdateProfile { get; set; } = true;
    }

    public enum IdentityActivateMethod: byte
    {
        None = 0,
        ConfirmEmail = 1,
        RandomPassword = 2
    }
}
