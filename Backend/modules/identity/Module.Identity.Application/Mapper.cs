﻿using Mapster;
using Module.Identity.Application.Models;
using Module.Identity.Domain.Models;

namespace Module.Identity.Application
{
    public static class Mapper
    {
        public static void Register()
        {
            TypeAdapterConfig<AppUser, AdminUserRM>.NewConfig()
                .AfterMapping(x => 
                {
                    x.UpdatedAt = x.UpdatedAt?.ToUniversalTime();
                });
        }
    }
}
