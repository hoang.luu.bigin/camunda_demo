﻿namespace Module.Identity.Application.Shared
{
    public class EntityType
    {
        public const string RootAdmin = "Admin";
        public const string NormalAdmin = "NormalAdmin";
        public const string ClientAdmin = "ClientAdmin";
        public const string ClientUser = "ClientUser";
    }

    public class SystemIdType
    {
        public const byte RootAdmin = 1;
        public const byte ClientAdmin = 2;
        public const byte ClientUser = 3; // extension user
    }

    public static class PolicyConst
    {
        public const string RootAdminOnly = "AdminOnly";
    }
}
