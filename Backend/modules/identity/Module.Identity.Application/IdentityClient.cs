﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Threading.Tasks;

namespace Module.Identity.Application
{
    /// <summary>
    /// Identity Client
    /// </summary>
    public class IdentityClient
    {
        private readonly RestClient _restClient;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration"></param>
        public IdentityClient(
            IConfiguration configuration
        )
        {
            _restClient = new RestClient(configuration["IdentityServer:Authority"]);
            _configuration = configuration;
        }

        /// <summary>
        /// Connect Token
        /// </summary>
        /// <param name="grantType"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scopes"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<TokenResponse> ConnectToken(string grantType, string clientId, string clientSecret, string scopes, string username, string password)
        {
            var request = new RestRequest("connect/token", Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            var formBody = "";
            if (!string.IsNullOrWhiteSpace(grantType))
                formBody += $"&grant_type={grantType}";

            if (!string.IsNullOrWhiteSpace(clientId))
                formBody += $"&client_id={clientId}";

            if (!string.IsNullOrWhiteSpace(clientSecret))
                formBody += $"&client_secret={clientSecret}";

            if (!string.IsNullOrWhiteSpace(scopes))
                formBody += $"&scope={scopes}";

            if (!string.IsNullOrWhiteSpace(username))
                formBody += $"&username={username}";

            if (!string.IsNullOrWhiteSpace(password))
                formBody += $"&password={password}";

            formBody = formBody.TrimStart('&');

            request.AddParameter("application/x-www-form-urlencoded", formBody, ParameterType.RequestBody);

            var response = await _restClient.ExecuteAsync<TokenResponse>(request);

            if (response.IsSuccessful)
                return response.Data;

            return null;
        }

        public async Task<TokenResponse> ExternalConnectToken(string clientId, string clientSecret, string scopes, string externalProvider, string externalToken)
        {
            var request = new RestRequest("connect/token", Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            var formBody = "";
            formBody += $"&grant_type=external";

            if (!string.IsNullOrWhiteSpace(clientId))
                formBody += $"&client_id={clientId}";

            if (!string.IsNullOrWhiteSpace(clientSecret))
                formBody += $"&client_secret={clientSecret}";

            if (!string.IsNullOrWhiteSpace(scopes))
                formBody += $"&scope={scopes}";

            if (!string.IsNullOrWhiteSpace(externalProvider))
                formBody += $"&external_provider={externalProvider}";

            if (!string.IsNullOrWhiteSpace(externalToken))
                formBody += $"&external_token={externalToken}";

            formBody = formBody.TrimStart('&');

            request.AddParameter("application/x-www-form-urlencoded", formBody, ParameterType.RequestBody);

            var response = await _restClient.ExecuteAsync<TokenResponse>(request);

            if (response.IsSuccessful)
                return response.Data;

            return null;
        }
    }



    /// <summary>
    /// Token Reponse
    /// </summary>
    public class TokenResponse
    {
        /// <summary>
        /// AccessToken
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// Expires In
        /// </summary>
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
        /// <summary>
        /// Token Type
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        /// <summary>
        /// Refresh Token
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
