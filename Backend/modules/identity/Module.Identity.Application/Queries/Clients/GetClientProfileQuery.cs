﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Identity.Application.Models;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Clients
{
    public class GetClientProfileQuery : AuthRequestBase<Profile>
    {
        public string Id { get; set; }
        public string? ClientId { get; set; }
    }

    public class GetClientProfileValidator : AbstractValidator<GetClientProfileQuery>
    {
        public GetClientProfileValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
        }
    }

    public class GetClientProfileQueryHandler : IRequestHandler<GetClientProfileQuery, Profile>
    {
        private readonly AppIdentityDbContext _appIdentityDbContext;

        public GetClientProfileQueryHandler(AppIdentityDbContext appIdentityDbContext)
        {
            _appIdentityDbContext = appIdentityDbContext;
        }

        public async Task<Profile> Handle(GetClientProfileQuery request, CancellationToken cancellationToken)
        {
            AppUser? user = null;

            if (request.GetSystemId() == SystemIdType.RootAdmin)
            {
                if (string.IsNullOrEmpty(request.ClientId))
                    throw new Exception("ClientId is required.");

                user = await _appIdentityDbContext.Users
                    .IgnoreQueryFilters()
                    .FirstOrDefaultAsync(x =>
                        x.Id == request.Id
                        && x.SystemId == SystemIdType.ClientAdmin
                        && x.EntityId == request.ClientId
                    );
            }
            else if (request.GetSystemId() == SystemIdType.ClientAdmin)
            {
                user = await _appIdentityDbContext.Users
                    .IgnoreQueryFilters()
                    .FirstOrDefaultAsync(x =>
                        x.Id == request.Id
                        && x.SystemId == SystemIdType.ClientUser
                        && x.EntityId == request.GetEntityId()
                    );
            }
            else
                throw new Exception("SystemId is invalid. [1, 2] is required.");

            if (user == null)
                throw new Exception("This user not found!");

            return user.Adapt<Profile>();
        }
    }
}
