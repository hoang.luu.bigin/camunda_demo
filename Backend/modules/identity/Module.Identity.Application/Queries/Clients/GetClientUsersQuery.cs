﻿using _Shared.Application;
using CloudKit.Common.Types;
using CloudKit.Infrastructure.Data.EfCore.Core;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Identity.Application.Models;
using Module.Identity.Application.Shared;
using Module.Identity.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Clients
{
    public class GetClientUsersQuery : PagingAuthRequestBase<EntityList<AdminUserRM>>
    {
        public string? SearchString { get; set; }
        public bool? Status { get; set; }
    }

    public class GetClientUsersQueryValidator : AbstractValidator<GetClientUsersQuery>
    {
    }

    public class GetClientUsersQueryHandler : IRequestHandler<GetClientUsersQuery, EntityList<AdminUserRM>>
    {
        private readonly AppIdentityDbContext _appIdentityDbContext;
        public GetClientUsersQueryHandler(AppIdentityDbContext appIdentityDbContext)
        {
            _appIdentityDbContext = appIdentityDbContext;
        }

        public async Task<EntityList<AdminUserRM>> Handle(GetClientUsersQuery request, CancellationToken cancellationToken)
        {
            if (request.GetSystemId() != SystemIdType.ClientAdmin)
                throw new Exception("ClientAdmin is required.");

            var result = new EntityList<AdminUserRM>();

            var searchString = request.SearchString?.ToLower() ?? "";

            var query = _appIdentityDbContext.Users
                .IgnoreQueryFilters()
                .AsNoTracking()
                .Where(x =>
                    x.SystemId == SystemIdType.ClientUser
                    && x.EntityId == request.GetEntityId()
                    && (x.FullName == null
                        || x.FullName.ToLower().Contains(searchString.ToLower())
                        || x.PhoneNumber.Contains(searchString)
                        || x.Email.ToLower().Contains(searchString))
                    && (request.Status == null || x.IsActive == request.Status)
                );

            result.Total = await query.CountAsync();

            if (!string.IsNullOrWhiteSpace(request.OrderBy))
                query = query.OrderWithDatatable(request.OrderBy);

            result.Entities = (await query
                    .Skip(request.Start)
                    .Take(request.Length)
                    .ToListAsync()
                    ).Adapt<List<AdminUserRM>>();

            return result;
        }
    }
}
