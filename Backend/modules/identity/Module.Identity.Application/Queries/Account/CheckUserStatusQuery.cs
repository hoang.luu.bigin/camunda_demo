﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Account
{
    public class CheckUserStatusQuery : IRequest<byte>
    {
        public string? Provider { get; set; }
        public string Key { get; set; }
        public string Email { get; set; }
    }

    public class CheckUserStatusQueryValidator : AbstractValidator<CheckUserStatusQuery>
    {

    }

    public class CheckUserStatusQueryHandler : IRequestHandler<CheckUserStatusQuery, byte>
    {
        private readonly UserManager<AppUser> _userManager;
        public CheckUserStatusQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<byte> Handle(CheckUserStatusQuery request, CancellationToken cancellationToken)
        {
            switch(request.Provider)
            {
                case "facebook":
                case  "google":
                    {
                        var user = await _userManager.FindByLoginAsync(request.Provider, request.Key);
                        if (user != null)
                            return 1;
                        else
                        {
                            user = await _userManager.FindByEmailAsync(request.Email);
                            return user == null ? (byte)2 : (byte)3;
                        }
                    }
                default:
                    {
                        var user = await _userManager.FindByNameAsync(request.Key);
                        return user != null ? (byte)1 : (byte)2;
                    }
            }
        }
    }
}
