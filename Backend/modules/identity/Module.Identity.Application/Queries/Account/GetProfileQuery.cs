﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Models;
using Module.Identity.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Account
{
    public class GetProfileQuery : AuthRequestBase<Profile>
    {
    }

    public class GetProfileValidator : AbstractValidator<GetProfileQuery>
    {
    }

    public class GetProfileQueryHandler : IRequestHandler<GetProfileQuery, Profile>
    {
        private readonly UserManager<AppUser> _userManager;
        public GetProfileQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<Profile> Handle(GetProfileQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.GetUserId().ToString());
            return user.Adapt<Profile>();
        }
    }
}
