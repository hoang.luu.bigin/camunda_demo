﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Models;
using Module.Identity.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Account
{
    public class GetUserLoginsQuery : AuthRequestBase<IEnumerable<AppUserLoginInfo>>
    {
    }

    public class GetUserLoginsQueryValidator : AbstractValidator<GetUserLoginsQuery>
    {
    }

    public class GetUserLoginsQueryHandler : IRequestHandler<GetUserLoginsQuery, IEnumerable<AppUserLoginInfo>>
    {
        private readonly UserManager<AppUser> _userManager;
        public GetUserLoginsQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<IEnumerable<AppUserLoginInfo>> Handle(GetUserLoginsQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.GetUserId().ToString());
            var claims = await _userManager.GetClaimsAsync(user);
            var tempUserLogins = await _userManager.GetLoginsAsync(user);
            IList<AppUserLoginInfo> userLogins = new List<AppUserLoginInfo>();
            foreach(var item in tempUserLogins)
            {
                var userLogin = item.Adapt<AppUserLoginInfo>();
                userLogin.Name = claims.FirstOrDefault(x => x.Type == item.LoginProvider + ":name")?.Value;
                userLogins.Add(userLogin);
            }
            return userLogins;
        }
    }
}
