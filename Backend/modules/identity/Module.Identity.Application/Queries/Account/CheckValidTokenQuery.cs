﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Account
{
    public class CheckValidTokenQuery : IRequest<bool>
    {
        public string Token { get; set; }
        public string Purpose { get; set; }
        public string? PurposeKey { get; set; }
        public string UserId { get;  set; }
    }

    public class CheckValidTokenQueryValidator : AbstractValidator<CheckValidTokenQuery>
    {

    }

    public class CheckValidTokenQueryHandler : IRequestHandler<CheckValidTokenQuery, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public CheckValidTokenQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(CheckValidTokenQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            switch(request.Purpose)
            {
                case UserManager<AppUser>.ResetPasswordTokenPurpose:
                case UserManager<AppUser>.ConfirmEmailTokenPurpose:
                   return await _userManager.VerifyUserTokenAsync(user, 
                       request.Purpose == UserManager<AppUser>.ResetPasswordTokenPurpose 
                       ? _userManager.Options.Tokens.PasswordResetTokenProvider 
                       : _userManager.Options.Tokens.EmailConfirmationTokenProvider
                       , request.Purpose, request.Token);
            }
            return false;
        }
    }
}
