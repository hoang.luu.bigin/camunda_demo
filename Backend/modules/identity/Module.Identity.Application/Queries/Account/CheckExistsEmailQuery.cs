﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Account
{
    public class CheckExistsEmailQuery : IRequest<bool>
    {
        /// <example>ex@bigin.vn</example>
        public string Email { get; set; }
    }

    public class CheckExistsEmailQueryValidator : AbstractValidator<CheckExistsEmailQuery>
    {
    }

    public class CheckExistsEmailQueryHandler : IRequestHandler<CheckExistsEmailQuery, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public CheckExistsEmailQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(CheckExistsEmailQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            return user != null;
        }
    }
}
