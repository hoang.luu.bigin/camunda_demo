﻿using _Shared.Application;
using CloudKit.Common.Types;
using CloudKit.Infrastructure.Data.EfCore.Core;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Identity.Application.Models;
using Module.Identity.Application.Shared;
using Module.Identity.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Admin
{
    public class GetAdminsQuery : PagingAuthRequestBase<EntityList<AdminUserRM>>
    {
        public string? SearchString { get; set; }
        public bool? Status { get; set; }
    }

    public class GetAdminsQueryValidator : AbstractValidator<GetAdminsQuery>
    {

    }

    public class GetAdminsQueryHandler : IRequestHandler<GetAdminsQuery, EntityList<AdminUserRM>>
    {
        private readonly AppIdentityDbContext _appIdentityDbContext;

        public GetAdminsQueryHandler(AppIdentityDbContext appIdentityDbContext)
        {
            _appIdentityDbContext = appIdentityDbContext;
        }

        public async Task<EntityList<AdminUserRM>> Handle(GetAdminsQuery request, CancellationToken cancellationToken)
        {
            if (request.GetEntityType() != EntityType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            var result = new EntityList<AdminUserRM>();

            var searchString = request.SearchString?.ToLower() ?? "";

            var query = _appIdentityDbContext.Users
                .IgnoreQueryFilters()
                .AsNoTracking()
                .Where(x =>
                    x.SystemId == SystemIdType.RootAdmin
                    && x.EntityType == EntityType.NormalAdmin
                    && (x.FullName == null
                        || x.FullName.ToLower().Contains(searchString)
                        || x.PhoneNumber.Contains(searchString)
                        || x.Email.ToLower().Contains(searchString))
                    && (request.Status == null || x.IsActive == request.Status)
                );

            result.Total = await query.CountAsync();

            if (!string.IsNullOrWhiteSpace(request.OrderBy))
                query = query.OrderWithDatatable(request.OrderBy);

            result.Entities = (await query
                    .Skip(request.Start)
                    .Take(request.Length)
                    .ToListAsync()
                    ).Adapt<List<AdminUserRM>>();

            return result;
        }
    }
}
