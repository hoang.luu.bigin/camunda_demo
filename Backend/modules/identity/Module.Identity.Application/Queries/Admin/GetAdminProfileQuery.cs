﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Models;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries.Admin
{
    public class GetAdminProfileQuery : AuthRequestBase<Profile>
    {
        public Guid Id { get; set; }
    }

    public class GetAdminProfileValidator : AbstractValidator<GetAdminProfileQuery>
    {
        public GetAdminProfileValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
        }
    }

    public class GetAdminProfileQueryHandler : IRequestHandler<GetAdminProfileQuery, Profile>
    {
        private readonly UserManager<AppUser> _userManager;
        public GetAdminProfileQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<Profile> Handle(GetAdminProfileQuery request, CancellationToken cancellationToken)
        {
            if (request.GetEntityType() != EntityType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            var user = await _userManager.FindByIdAsync(request.Id.ToString());
            return user.Adapt<Profile>();
        }
    }
}
