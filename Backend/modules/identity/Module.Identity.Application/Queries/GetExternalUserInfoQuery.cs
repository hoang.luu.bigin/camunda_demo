﻿using Facebook;
using FluentValidation;
using MediatR;
using Module.Identity.Application.Models;
using Module.Identity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Queries
{
    public class GetExternalUserInfoQuery : IRequest<ExternalUserInfo>
    {
        public string Token { get; set; }
        public string Provider { get; set; }
    }

    public class GetExternalUserInfoQueryValidator : AbstractValidator<GetExternalUserInfoQuery>
    {
    }

    public class GetExternalUserInfoQueryHandler : IRequestHandler<GetExternalUserInfoQuery, ExternalUserInfo>
    {
        private readonly GoogleClient _googleClient;

        public GetExternalUserInfoQueryHandler(GoogleClient googleClient)
        {
            _googleClient = googleClient;
        }
        public async Task<ExternalUserInfo> Handle(GetExternalUserInfoQuery request, CancellationToken cancellationToken)
        {
            ExternalUserInfo result = null;
            switch (request.Provider)
            {
                case "facebook":
                    var client = new FacebookClient();
                    dynamic facebookUser = await client.GetTaskAsync("/me",
                        new
                        {
                            fields = "id,name,email",
                            access_token = request.Token
                        });
                    result = new ExternalUserInfo()
                    {
                        Id = facebookUser.id,
                        Email = facebookUser.email,
                        FullName = facebookUser.name
                    };
                    break;
                case "google":

                    var googleUser = await _googleClient.GetUserInfo(request.Token);
                    result = new ExternalUserInfo()
                    {
                        Id = googleUser?.Id,
                        Email = googleUser?.Email,
                        FullName = googleUser?.Name
                    };
                    break;
            }
            result.Provider = request.Provider;
            return result;
        }
    }
}
