﻿using IdentityServer4.Models;
using IdentityServer4.Services;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Module.Identity.Application
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMediator _mediator;

        public ProfileService(UserManager<AppUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            context.IssuedClaims.AddRange(context.Subject.Claims);
            var user = await _userManager.GetUserAsync(context.Subject);
            if(user != null)
            {
                context.IssuedClaims.Add(new Claim("system_id", user.SystemId.ToString()));
                context.IssuedClaims.Add(new Claim("entity_type", user.EntityType));
                context.IssuedClaims.Add(new Claim("entity_code", user.EntityCode));
                if(user.EntityId != null)
                    context.IssuedClaims.Add(new Claim("entity_id", user.EntityId.ToString()));
                if (user.FirstLoginAt == null)
                {
                    user.FirstLoginAt = DateTime.UtcNow;
                    await _userManager.UpdateAsync(user);
                    context.IssuedClaims.Add(new Claim("f", "1"));
                }
                if (string.IsNullOrWhiteSpace(user.PasswordHash))
                    context.IssuedClaims.Add(new Claim("hp", "0"));
            }
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            context.IsActive = user != null && (user.EmailConfirmed || user.PhoneNumberConfirmed)
                                && user.IsActive && !user.IsDeleted;
        }
    }

}
