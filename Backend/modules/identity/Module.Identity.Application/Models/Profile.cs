﻿using System;

namespace Module.Identity.Application.Models
{
    /// <summary>
    /// User profile
    /// </summary>
    public class Profile
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Avatar
        /// </summary>
        public string? Avatar { get; set; }
        /// <summary>
        /// Full Name
        /// </summary>
        public string? FullName { get; set; }
        /// <summary>
        /// Date of birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }
        /// <summary>
        /// Preferenced language
        /// </summary>
        public string? PreferencedLang { get; set; }
        /// <summary>
        /// Entity type
        /// </summary>
        public string? EntityType { get; set; }

    }
}
