﻿namespace Module.Identity.Application.Models
{
    public class ExternalUserInfo
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Provider{ get; set; }
    }
}
