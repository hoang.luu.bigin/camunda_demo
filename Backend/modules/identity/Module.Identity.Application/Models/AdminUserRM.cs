﻿using _Shared.Application;
using System;
using System.Collections.Generic;
using System.Text;

namespace Module.Identity.Application.Models
{
    public class AdminUserRM: RMBase
    {
        public Guid Id { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

    }
}
