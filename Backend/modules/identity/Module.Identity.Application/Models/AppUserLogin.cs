﻿using Microsoft.AspNetCore.Identity;

namespace Module.Identity.Application.Models
{
    public class AppUserLoginInfo : UserLoginInfo
    {
        public AppUserLoginInfo(string loginProvider, string providerKey, string displayName) : base(loginProvider, providerKey, displayName)
        {

        }
        public AppUserLoginInfo() : base(null, null, null)
        {
        }
        public string? Name { get; set; }
    } 
}
