﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Module.Identity.Application
{
    public class CustomIdentityServerMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomIdentityServerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/connect/token") && context.Request.ContentType.StartsWith("application/json"))
            {
                context.Request.ContentType = "application/x-www-form-urlencoded";
                string content = string.Empty;
                using (StreamReader reader
                                 = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
                {
                    content = await reader.ReadToEndAsync();
                }
                JObject jObject = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(content);
                FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(jObject.ToObject<Dictionary<string, string>>());
                context.Request.Body = await formUrlEncodedContent.ReadAsStreamAsync();
            }
            await _next(context);
        }
    }
}
