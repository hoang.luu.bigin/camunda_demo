﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Permission
{
    public class ResetPermissionCommand : IRequest<bool>
    {
        public IList<AppPermission> Permissions { get; set; }
    }

    public class ResetPermissionCommandValidator : AbstractValidator<ResetPermissionCommand>
    {
        public ResetPermissionCommandValidator()
        {
        }
    }

    public class ResetPermissionCommandHandler : IRequestHandler<ResetPermissionCommand, bool>
    {
        private readonly AppIdentityDbContext _dbContext;

        public ResetPermissionCommandHandler(AppIdentityDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> Handle(ResetPermissionCommand command, CancellationToken cancellationToken)
        {
            _dbContext.AppPermisstions.RemoveRange(await _dbContext.AppPermisstions.ToListAsync());

            _dbContext.AppPermisstions.AddRange(command.Permissions);

            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}
