﻿using _Shared.Application;
using CloudKit.Infrastructure.Mailer.Models;
using CloudKit.Infrastructure.Notification;
using CloudKit.UI.AspNetCore.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Module.Identity.Application.Commands
{
    public class RegistrationCommand : IRequest<bool>
    {
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string EntityType { get; set; }
        public string EntityCode { get; set; }
        public string EntityId { get; set; }
        public byte SystemId { get; set; }
    }

    public class RegistrationCommandValidator : AbstractValidator<RegistrationCommand>
    {
        public RegistrationCommandValidator(AppIdentityDbContext dbContext)
        {
            RuleFor(x => x.UserName)
                .NotEmpty()
                .MustAsync(async (command, username, cancellation) =>
                {
                    return await dbContext.Users
                        .IgnoreQueryFilters()
                        .FirstOrDefaultAsync(x =>
                            x.UserName == command.UserName
                            && x.SystemId == command.SystemId
                            && x.EntityId == command.EntityId
                        )
                        == null;
                })
                .WithMessage("User is exists");

            RuleFor(x => x.FullName)
               .MaximumLength(50);
        }
    }

    public class RegistrationCommandHandler : IRequestHandler<RegistrationCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly INotificationService _notifyService;
        private readonly IHostEnvironment _webHostEnvironment;
        private readonly IIdentityOptionsProvider _identityOptionsProvider;
        private readonly RandomPasswordGenerator _randomPasswordGenerator;

        public RegistrationCommandHandler(
            IServiceProvider serviceProvider,

            INotificationService notifyService,
            IHostEnvironment webHostEnvironment,
            IIdentityOptionsProvider identityOptionsProvider,
            RandomPasswordGenerator randomPasswordGenerator
            )
        {
            _serviceProvider = serviceProvider;

            _notifyService = notifyService;
            _webHostEnvironment = webHostEnvironment;
            _identityOptionsProvider = identityOptionsProvider;
            _randomPasswordGenerator = randomPasswordGenerator;
        }

        public async Task<bool> Handle(RegistrationCommand command, CancellationToken cancellationToken)
        {
            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = command.SystemId;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();

            var identityOptions = _identityOptionsProvider.Resolve(command.SystemId, command.EntityType);
            var user = new AppUser()
            {
                UserName = command.UserName,
                PreferencedLang = CultureInfo.CurrentCulture.Name,
                FullName = command.FullName,
                Avatar = command.Avatar,
                Email = command.Email,
                PhoneNumber = command.PhoneNumber,
                EmailConfirmed = identityOptions.ActivateMethod != IdentityActivateMethod.ConfirmEmail,
                EntityType = command.EntityType,
                EntityCode = command.EntityCode,
                EntityId = command.EntityId,
                SystemId = command.SystemId
            };
            if (identityOptions.ActivateMethod == IdentityActivateMethod.RandomPassword)
            {
                command.PassWord = _randomPasswordGenerator.Generate();
                user.ForceChangePassword = true;
            }
            var identityResult = await _userManager.CreateAsync(user, command.PassWord);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);

            switch (identityOptions.ActivateMethod)

            {
                case IdentityActivateMethod.ConfirmEmail:
                    {
                        var code = Uri.EscapeDataString(await _userManager.GenerateEmailConfirmationTokenAsync(user));
                        var confirmationLink = string.Format(identityOptions.LinkConfirmEmailPattern, user.Id, code, command.Email);

                        await _notifyService.SendTemplateEmailsAsync(new List<EmailTemplateRequest>() {
                        new EmailTemplateRequest()
                        {
                            To = command.Email,
                            Subject = identityOptions.ConfirmEmailTitle,
                            TemplatePath = _webHostEnvironment.MapPath(identityOptions.ConfirmEmailTemplatePath),
                            TemplateData = new {
                                command.FullName,
                                Link = confirmationLink,
                                command.Email
                            }
                        } });
                        break;
                    }
                case IdentityActivateMethod.RandomPassword:
                    {
                        await _notifyService.SendTemplateEmailsAsync(new List<EmailTemplateRequest>() {
                        new EmailTemplateRequest()
                        {
                            To = command.Email,
                            Subject = identityOptions.RandomPasswordEmailTitle,
                            TemplatePath = _webHostEnvironment.MapPath(identityOptions.RandomPasswordEmailTemplatePath),
                            TemplateData = new {
                                command.FullName,
                                command.UserName,
                                pass_word = HttpUtility.HtmlEncode(command.PassWord),
                                identityOptions.LoginLink,
                                Logo = identityOptions.ResetPasswordLogo,
                                Background = identityOptions.ResetPasswordBackground,
                                Address = identityOptions.ResetPasswordAddress,
                                Mailto = identityOptions.ResetPasswordEmailAddress,
                                Logob = identityOptions.ResetPasswordLogoB
                            }
                        } });
                        break;
                    }
            }

            return identityResult.Succeeded /*&& customer != null*/;
        }
    }

}
