﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Clients
{
    public class UpdatePrivateAccountCommand : AuthRequestBase<bool>
    {
        public string OldEmail { get; set; }
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
        public string ClientId { get; set; }
    }

    public class UpdatePrivateAccountCommandValidator : AbstractValidator<UpdatePrivateAccountCommand>
    {
        public UpdatePrivateAccountCommandValidator()
        {
            RuleFor(x => x.OldEmail).NotNull().NotEmpty();
            RuleFor(x => x.Email).NotNull().NotEmpty();
            RuleFor(x => x.ClientId).NotNull().NotEmpty();
        }
    }

    public class UpdatePrivateAccountCommandHandler : IRequestHandler<UpdatePrivateAccountCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdatePrivateAccountCommandHandler(
            IServiceProvider serviceProvider
            )
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool> Handle(UpdatePrivateAccountCommand command, CancellationToken cancellationToken)
        {
            if (command.GetSystemId() != SystemIdType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = SystemIdType.ClientAdmin;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();
            var _appIdentityDbContext = scope.ServiceProvider.GetService<AppIdentityDbContext>();

            var user = await _appIdentityDbContext.Users
                .FirstOrDefaultAsync(x =>
                    x.Email == command.OldEmail
                    && x.EntityId == command.ClientId
                );

            if (user == null)
                throw new Exception("This user not found!");

            user.PhoneNumber = command.PhoneNumber;
            user.Avatar = command.Avatar;
            user.FullName = command.FullName;
            user.Email = command.Email;
            user.UserName = command.Email;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}
