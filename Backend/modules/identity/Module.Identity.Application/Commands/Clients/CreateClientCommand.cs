﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Module.Identity.Application.Shared;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Clients
{
    public class CreateClientCommand : AuthRequestBase<bool>
    {
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
        public string? ClientId { get; set; }
    }

    public class CreateClientCommandValidator : AbstractValidator<CreateClientCommand>
    {
        public CreateClientCommandValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }

    public class CreateClientCommandHandler : IRequestHandler<CreateClientCommand, bool>
    {
        private readonly IMediator _mediator;

        public CreateClientCommandHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<bool> Handle(CreateClientCommand command, CancellationToken cancellationToken)
        {
            var registrationCommand = command.Adapt<RegistrationCommand>();

            registrationCommand.UserName = registrationCommand.Email;
            
            if (command.GetSystemId() == SystemIdType.RootAdmin)
            {
                if (string.IsNullOrEmpty(command.ClientId))
                    throw new System.Exception("ClientId is required.");

                // create client admin
                registrationCommand.SystemId = SystemIdType.ClientAdmin;
                registrationCommand.EntityType = EntityType.ClientAdmin;
                registrationCommand.EntityCode = command.ClientId;
                registrationCommand.EntityId = command.ClientId;
            }
            else if (command.GetSystemId() == SystemIdType.ClientAdmin)
            {
                // create client user
                registrationCommand.SystemId = SystemIdType.ClientUser;
                registrationCommand.EntityType = EntityType.ClientUser;
                registrationCommand.EntityCode = command.GetEntityCode();
                registrationCommand.EntityId = command.GetEntityId();
            }
            else
                throw new System.Exception("SystemId is invalid. [1, 2] is required.");

            return await _mediator.Send(registrationCommand);
        }
    }
}
