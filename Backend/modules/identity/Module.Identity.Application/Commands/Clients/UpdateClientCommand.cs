﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Clients
{
    public class UpdateClientCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
        public string? ClientId { get; set; }
    }

    public class UpdateClientCommandValidator : AbstractValidator<UpdateClientCommand>
    {
        public UpdateClientCommandValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }

    public class UpdateClientCommandHandler : IRequestHandler<UpdateClientCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdateClientCommandHandler(
            IServiceProvider serviceProvider
            )
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool> Handle(UpdateClientCommand command, CancellationToken cancellationToken)
        {
            byte systemId = 0;
            var clientId = "";

            if (command.GetSystemId() == SystemIdType.RootAdmin)
            {
                if (string.IsNullOrEmpty(command.ClientId))
                    throw new Exception("ClientId is required.");

                systemId = SystemIdType.ClientAdmin;
                clientId = command.ClientId;
            }
            else if (command.GetSystemId() == SystemIdType.ClientAdmin)
            {
                systemId = SystemIdType.ClientUser;
                clientId = command.GetEntityId();
            }
            else
                throw new Exception("SystemId is invalid. [1, 2] is required.");

            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = systemId;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();
            var _appIdentityDbContext = scope.ServiceProvider.GetService<AppIdentityDbContext>();

            var user = await _appIdentityDbContext.Users
                .FirstOrDefaultAsync(x =>
                    x.Id == command.Id
                    && x.EntityId == clientId
                );

            if (user == null)
                throw new Exception("This user not found!");

            user.Email = command.Email;
            user.UserName = command.Email;

            user.FullName = command.FullName;
            user.PhoneNumber = command.PhoneNumber;
            user.Avatar = command.Avatar;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}
