﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Clients
{
    public class UpdatePrivateAccountStatusCommand : AuthRequestBase<bool>
    {
        public string Email { get; set; }
        public bool Status { get; set; }
        public string ClientId { get; set; }
    }

    public class UpdatePrivateAccountStatusCommandValidator : AbstractValidator<UpdatePrivateAccountStatusCommand>
    {
        public UpdatePrivateAccountStatusCommandValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty();
            RuleFor(x => x.ClientId).NotNull().NotEmpty();
        }
    }

    public class UpdatePrivateAccountStatusCommandHandler : IRequestHandler<UpdatePrivateAccountStatusCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdatePrivateAccountStatusCommandHandler(
            IServiceProvider serviceProvider
            )
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool> Handle(UpdatePrivateAccountStatusCommand command, CancellationToken cancellationToken)
        {
            if (command.GetSystemId() != SystemIdType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = SystemIdType.ClientAdmin;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();
            var _appIdentityDbContext = scope.ServiceProvider.GetService<AppIdentityDbContext>();

            var user = await _appIdentityDbContext.Users
                .FirstOrDefaultAsync(x =>
                    x.Email == command.Email
                    && x.EntityId == command.ClientId
                );

            if (user == null)
                throw new Exception("This user not found!");

            user.IsActive = command.Status;
            if (string.IsNullOrWhiteSpace(user.PreferencedLang))
                user.PreferencedLang = CultureInfo.CurrentCulture.Name;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}
