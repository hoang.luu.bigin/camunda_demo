﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Clients
{
    public class DeactivateOrActivateClientCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public bool Status { get; set; }
        public string? ClientId { get; set; }
    }

    public class DeactivateOrActivateClientCommandValidator : AbstractValidator<DeactivateOrActivateClientCommand>
    {
        public DeactivateOrActivateClientCommandValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
        }
    }

    public class DeactivateOrActivateClientCommandCommandHandler : IRequestHandler<DeactivateOrActivateClientCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;

        public DeactivateOrActivateClientCommandCommandHandler(
            IServiceProvider serviceProvider
            )
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool> Handle(DeactivateOrActivateClientCommand request, CancellationToken cancellationToken)
        {
            byte systemId = 0;
            var clientId = "";

            if (request.GetSystemId() == SystemIdType.RootAdmin)
            {
                if (string.IsNullOrEmpty(request.ClientId))
                    throw new Exception("ClientId is required.");

                systemId = SystemIdType.ClientAdmin;
                clientId = request.ClientId;
            }
            else if (request.GetSystemId() == SystemIdType.ClientAdmin)
            {
                systemId = SystemIdType.ClientUser;
                clientId = request.GetEntityId();
            }
            else
                throw new Exception("SystemId is invalid. [1, 2] is required.");

            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = systemId;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();
            var _appIdentityDbContext = scope.ServiceProvider.GetService<AppIdentityDbContext>();
           
            var user = await _appIdentityDbContext.Users
                .FirstOrDefaultAsync(x =>
                    x.Id == request.Id
                    && x.EntityId == clientId
                );

            if (user == null)
                throw new Exception("This user not found!");

            user.IsActive = request.Status;
            if (string.IsNullOrWhiteSpace(user.PreferencedLang))
                user.PreferencedLang = CultureInfo.CurrentCulture.Name;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}

