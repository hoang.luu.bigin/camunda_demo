﻿using _Shared.Application;
using CloudKit.Infrastructure.Mailer.Models;
using CloudKit.Infrastructure.Notification;
using CloudKit.UI.AspNetCore.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Module.Identity.Application.Commands.Clients
{
    public class ResetClientUserPasswordCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public string ClientId { get; set; }
    }

    public class ResetClientUserPasswordCommandValidator : AbstractValidator<ResetClientUserPasswordCommand>
    {
        public ResetClientUserPasswordCommandValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
            RuleFor(x => x.ClientId).NotNull().NotEmpty();
        }
    }

    public class ResetClientUserPasswordCommandHandler : IRequestHandler<ResetClientUserPasswordCommand, bool>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly AppIdentityDbContext _appIdentityDbContext;
        private readonly RandomPasswordGenerator _randomPasswordGenerator;
        private readonly IIdentityOptionsProvider _identityOptionsProvider;
        private readonly INotificationService _notifyService;
        private readonly IHostEnvironment _webHostEnvironment;

        public ResetClientUserPasswordCommandHandler(
            IServiceProvider serviceProvider,
            AppIdentityDbContext appIdentityDbContext,
            RandomPasswordGenerator randomPasswordGenerator,
            IIdentityOptionsProvider identityOptionsProvider,
            INotificationService notifyService,
            IHostEnvironment webHostEnvironment
            )
        {
            _serviceProvider = serviceProvider;
            _appIdentityDbContext = appIdentityDbContext;
            _randomPasswordGenerator = randomPasswordGenerator;
            _identityOptionsProvider = identityOptionsProvider;
            _notifyService = notifyService;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<bool> Handle(ResetClientUserPasswordCommand command, CancellationToken cancellationToken)
        {
            if (command.GetSystemId() != SystemIdType.ClientAdmin)
                throw new Exception("ClientAdmin is required.");

            // excuse in new scope with new system id
            using var scope = _serviceProvider.CreateScope();

            var _systemIdProvider = scope.ServiceProvider.GetService<ISystemIdProvider>();
            _systemIdProvider.SystemId = SystemIdType.ClientUser;

            var _userManager = scope.ServiceProvider.GetService<UserManager<AppUser>>();
            var _appIdentityDbContext = scope.ServiceProvider.GetService<AppIdentityDbContext>();

            var user = await _appIdentityDbContext.Users
                .FirstOrDefaultAsync(x =>
                    x.Id == command.Id
                    && x.EntityId == command.GetEntityId()
                );

            if (user == null)
                throw new Exception("This user not found!");

            string resetCode = await _userManager.GeneratePasswordResetTokenAsync(user);
            var passWord = _randomPasswordGenerator.Generate();

            var identityResult = await _userManager.ResetPasswordAsync(user, resetCode, passWord);
            if (!identityResult.Succeeded)
                throw new Exception(identityResult.Errors.First().Description);

            user.ForceChangePassword = false;
            user.EmailConfirmed = true;

            identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
                throw new Exception(identityResult.Errors.First().Description);

            var identityOptions = _identityOptionsProvider.Resolve(SystemIdType.ClientUser, EntityType.ClientUser);
            await _notifyService.SendTemplateEmailsAsync(new List<EmailTemplateRequest>()
            {
                new EmailTemplateRequest()
                {
                    To = user.Email,
                    Subject = identityOptions.RandomPasswordEmailTitle,
                    TemplatePath = _webHostEnvironment.MapPath(identityOptions.RandomPasswordEmailTemplatePath),
                    TemplateData = new {
                        user.FullName,
                        user.UserName,
                        pass_word = HttpUtility.HtmlEncode(passWord),
                        Link = identityOptions.LoginLink,
                        Logo = identityOptions.ResetPasswordLogo,
                        Background = identityOptions.ResetPasswordBackground,
                        Address = identityOptions.ResetPasswordAddress,
                        Mailto = identityOptions.ResetPasswordEmailAddress,
                        Logob = identityOptions.ResetPasswordLogoB
                    }
                }
            });

            return identityResult.Succeeded;
        }
    }
}
