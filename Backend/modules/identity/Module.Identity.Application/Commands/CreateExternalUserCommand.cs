﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Queries;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands
{
    public class CreateExternalUserCommand : IRequest<AppUser?>
    {
        public string ExternalProvider { get; set; }
        public string ExternalToken { get; set; }
        public string? FullName { get; set; }
        public string? Avatar { get; set; }
        public string? Email { get; set; }
        public string EntityType { get; set; }
        public string EntityCode { get; set; }
        public string EntityId { get; set; }
        public byte SystemId { get; set; }
        public bool ForceConfirmEmail { get; set; }
    }

    public class CreateExternalUserCommandValidator : AbstractValidator<CreateExternalUserCommand>
    {
        public CreateExternalUserCommandValidator()
        {
            RuleFor(x => x.ExternalProvider).NotEmpty();
            RuleFor(x => x.ExternalToken).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.FullName)
               .MaximumLength(50);
        }
    }

    public class CreateExternalUserCommandHandler : IRequestHandler<CreateExternalUserCommand, AppUser?>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMediator _mediator;
        public CreateExternalUserCommandHandler(UserManager<AppUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }
        public async Task<AppUser?> Handle(CreateExternalUserCommand command, CancellationToken cancellationToken)
        {
            var externalUserInfo = await _mediator.Send(new GetExternalUserInfoQuery()
            {
                Token = command.ExternalToken,
                Provider = command.ExternalProvider
            });
            if(externalUserInfo.Email != command.Email && !string.IsNullOrWhiteSpace(externalUserInfo.Email)) throw new Exception("Token does not match the email address");
            var user = await _userManager.FindByLoginAsync(externalUserInfo.Provider, externalUserInfo.Id);
            if (user == null)
            {
                user = new AppUser()
                {
                    UserName = string.IsNullOrEmpty(externalUserInfo.Email) ?  Guid.NewGuid().ToString() : externalUserInfo.Email,
                    PreferencedLang = CultureInfo.CurrentCulture.Name,
                    FullName = command.FullName,
                    Avatar = command.Avatar,
                    Email = command.Email,
                    EmailConfirmed = !command.ForceConfirmEmail && !string.IsNullOrWhiteSpace(externalUserInfo.Email),
                    EntityType = command.EntityType,
                    EntityCode = command.EntityCode,
                    EntityId = command.EntityId,
                    SystemId = command.SystemId
                };
                var identityResult = await _userManager.CreateAsync(user);
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                identityResult = await _userManager.AddClaimsAsync(user, new List<Claim>()
                            {
                                new Claim(command.ExternalProvider + ":name", externalUserInfo.FullName),
                                new Claim(command.ExternalProvider + ":email", externalUserInfo.Email)
                            });
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(command.ExternalProvider, externalUserInfo.Id, command.ExternalProvider));
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            }
            return user /*&& customer != null*/;
        }
    }

}
