﻿using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Module.Identity.Application.Shared;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Admin
{
    public class CreateNormalAdminCommand : AuthRequestBase<bool>
    {
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
    }

    public class CreateNormalAdminCommandValidator : AbstractValidator<CreateNormalAdminCommand>
    {
        public CreateNormalAdminCommandValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }

    public class CreateNormalAdminCommandHandler : IRequestHandler<CreateNormalAdminCommand, bool>
    {
        private readonly IMediator _mediator;

        public CreateNormalAdminCommandHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<bool> Handle(CreateNormalAdminCommand command, CancellationToken cancellationToken)
        {
            if (command.GetEntityType() != EntityType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            // create normal admin
            var registrationCommand = command.Adapt<RegistrationCommand>();

            registrationCommand.UserName = registrationCommand.Email;

            registrationCommand.SystemId = SystemIdType.RootAdmin;
            registrationCommand.EntityType = EntityType.NormalAdmin;
            registrationCommand.EntityCode = command.Email;
            registrationCommand.EntityId = command.Email;

            await _mediator.Send(registrationCommand);

            return true;
        }
    }
}
