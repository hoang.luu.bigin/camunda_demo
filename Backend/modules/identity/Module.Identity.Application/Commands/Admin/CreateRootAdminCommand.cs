﻿using FluentValidation;
using Mapster;
using MediatR;
using Module.Identity.Application.Shared;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Admin
{
    public class CreateRootAdminCommand : IRequest<bool>
    {
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
    }

    public class CreateRootAdminCommandValidator : AbstractValidator<CreateRootAdminCommand>
    {
        public CreateRootAdminCommandValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }

    public class CreateRootAdminCommandHandler : IRequestHandler<CreateRootAdminCommand, bool>
    {
        private readonly IMediator _mediator;

        public CreateRootAdminCommandHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<bool> Handle(CreateRootAdminCommand command, CancellationToken cancellationToken)
        {
            var registrationCommand = command.Adapt<RegistrationCommand>();

            registrationCommand.UserName = registrationCommand.Email;

            registrationCommand.SystemId = SystemIdType.RootAdmin;
            registrationCommand.EntityType = EntityType.RootAdmin;

            registrationCommand.EntityCode = command.Email;
            registrationCommand.EntityId = command.Email;

            await _mediator.Send(registrationCommand);
            return true;
        }
    }
}
