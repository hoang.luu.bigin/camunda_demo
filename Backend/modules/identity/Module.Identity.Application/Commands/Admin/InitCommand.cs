﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Admin
{
    public class InitCommand : IRequest<bool>
    {
    }

    public class InitCommandValidator : AbstractValidator<InitCommand>
    {
    }

    public class InitCommandHandler : IRequestHandler<InitCommand, bool>
    {
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IEnumerable<IPermissionRegistration> _permissionRegistrations;

        public InitCommandHandler(
            RoleManager<AppRole> roleManager,
            IEnumerable<IPermissionRegistration> permissionRegistrations
            )
        {
            _roleManager = roleManager;
            _permissionRegistrations = permissionRegistrations;
        }

        public async Task<bool> Handle(InitCommand command, CancellationToken cancellationToken)
        {
            PermissionDescriptionGroupCollection permissionGroupCollection = new PermissionDescriptionGroupCollection();
            foreach (var permissionRegistration in _permissionRegistrations)
            {
                permissionRegistration.Register(permissionGroupCollection);
            }
            var permissions = permissionGroupCollection.GetAllPermissions();
            if (!await _roleManager.RoleExistsAsync("TopAdmin"))
            {
                var topAdmin = new AppRole("TopAdmin")
                {
                    Description = "TopAdmin has full control, permision on system"
                };
                await _roleManager.CreateAsync(topAdmin);
                foreach (var permission in permissions)
                {
                    await _roleManager.AddClaimAsync(topAdmin, new Claim("Permission", permission.Code));
                }
            }

            if (!await _roleManager.RoleExistsAsync("OverallAdmin"))
            {
                var overrallAdmin = new AppRole("OverallAdmin")
                {
                    Description = "OverallAdmin has full control, permision on system"
                };
                await _roleManager.CreateAsync(overrallAdmin);
                foreach (var permission in permissions)
                {
                    await _roleManager.AddClaimAsync(overrallAdmin, new Claim("Permission", permission.Code));
                }
            }
            return true;
        }
    }
}
