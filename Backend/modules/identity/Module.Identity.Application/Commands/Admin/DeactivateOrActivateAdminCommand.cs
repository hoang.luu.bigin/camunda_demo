﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Admin
{
    public class DeactivateOrActivateAdminCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public bool Status { get; set; }
    }

    public class DeactivateOrActivateAdminCommandValidator : AbstractValidator<DeactivateOrActivateAdminCommand>
    {
        public DeactivateOrActivateAdminCommandValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
        }
    }

    public class DeactivateOrActivateAdminCommandCommandHandler : IRequestHandler<DeactivateOrActivateAdminCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;

        public DeactivateOrActivateAdminCommandCommandHandler(
            UserManager<AppUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<bool> Handle(DeactivateOrActivateAdminCommand request, CancellationToken cancellationToken)
        {
            if (request.GetEntityType() != EntityType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            var user = await _userManager.FindByIdAsync(request.Id);
            if (user == null)
                throw new Exception("This user not found!");

            user.IsActive = request.Status;
            if (string.IsNullOrWhiteSpace(user.PreferencedLang))
                user.PreferencedLang = CultureInfo.CurrentCulture.Name;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}

