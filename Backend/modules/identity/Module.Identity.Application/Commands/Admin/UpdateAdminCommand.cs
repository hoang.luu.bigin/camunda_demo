﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Shared;
using Module.Identity.Domain.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Admin
{
    public class UpdateAdminCommand : AuthRequestBase<bool>
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Avatar { get; set; }
    }

    public class UpdateAdminCommandValidator : AbstractValidator<UpdateAdminCommand>
    {
        public UpdateAdminCommandValidator()
        {
            RuleFor(x => x.Id).NotNull().NotEmpty();
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }

    public class UpdateAdminCommandHandler : IRequestHandler<UpdateAdminCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;

        public UpdateAdminCommandHandler(
            UserManager<AppUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<bool> Handle(UpdateAdminCommand command, CancellationToken cancellationToken)
        {
            if (command.GetEntityType() != EntityType.RootAdmin)
                throw new Exception("RootAdmin is required.");

            var user = await _userManager.FindByIdAsync(command.Id);
            if (user == null)
                throw new Exception("This user not found!");

            user.Email = command.Email;
            user.UserName = command.Email;

            user.FullName = command.FullName;
            user.PhoneNumber = command.PhoneNumber;
            user.Avatar= command.Avatar;

            user.EntityId = command.Email;
            user.EntityCode = command.Email;

            user.UpdatedAt = DateTime.UtcNow;

            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }
    }
}
