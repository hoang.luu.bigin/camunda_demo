﻿using _Shared.Application;
using CloudKit.Infrastructure.Mailer.Models;
using CloudKit.Infrastructure.Notification;
using CloudKit.UI.AspNetCore.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class SendLinkResetPasswordCommand : IRequest<bool>
    {
        public string Email { get; set; }
    }

    public class SendLinkResetPasswordCommandValidator : AbstractValidator<SendLinkResetPasswordCommand>
    {
        public SendLinkResetPasswordCommandValidator()
        {
        }
    }

    public class SendLinkResetPasswordCommandHandler : IRequestHandler<SendLinkResetPasswordCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly INotificationService _notifyService;
        private readonly IHostEnvironment _webHostEnvironment;
        private readonly IIdentityOptionsProvider _identityOptionsProvider;
        private readonly ISystemIdProvider _systemIdProvider;

        public SendLinkResetPasswordCommandHandler(
            UserManager<AppUser> userManager
            , INotificationService notifyService
            , IHostEnvironment webHostEnvironment
            , IIdentityOptionsProvider identityOptionsProvider
            , ISystemIdProvider systemIdProvider)
        {
            _userManager = userManager;
            _notifyService = notifyService;
            _webHostEnvironment = webHostEnvironment;
            _identityOptionsProvider = identityOptionsProvider;
            _systemIdProvider = systemIdProvider;
        }
        public async Task<bool> Handle(SendLinkResetPasswordCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(command.Email);

            var identityOptions = _identityOptionsProvider.Resolve(_systemIdProvider.SystemId, user.EntityType);
            string resetCode = Uri.EscapeDataString(await _userManager.GeneratePasswordResetTokenAsync(user));

            var resetPasswordLink = string.Format(identityOptions.LinkResetPasswordPattern, user.Id, resetCode, user.Email);

            await _notifyService.SendTemplateEmailsAsync(new List<EmailTemplateRequest>() {
                new EmailTemplateRequest()
                {
                    To = command.Email,
                    Subject = identityOptions.ResetPasswordTitle,
                    TemplatePath = _webHostEnvironment.MapPath(identityOptions.ResetPasswordTemplatePath),
                    TemplateData = new {
                        user.FullName,
                        Link = resetPasswordLink,
                        user.Email,
                        Logo = identityOptions.ResetPasswordLogo,
                        Background = identityOptions.ResetPasswordBackground,
                        Address = identityOptions.ResetPasswordAddress,
                        Mailto = identityOptions.ResetPasswordEmailAddress,
                        Logob = identityOptions.ResetPasswordLogoB
                    }
                } });
            return true;
        }

    }
}
