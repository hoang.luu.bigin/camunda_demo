﻿using _Shared.Application;
using CloudKit.Infrastructure.Mailer.Models;
using CloudKit.Infrastructure.Notification;
using CloudKit.UI.AspNetCore.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class SendLinkConfirmEmailCommand : IRequest<bool>
    {
        public string Email { get; set; }
    }

    public class SendLinkConfirmAccountCommandValidator : AbstractValidator<SendLinkConfirmEmailCommand>
    {
        public SendLinkConfirmAccountCommandValidator()
        {
        }
    }

    public class SendLinkConfirmAccountCommandHandler : IRequestHandler<SendLinkConfirmEmailCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly INotificationService _notifyService;
        private readonly IHostEnvironment _webHostEnvironment;
        private readonly IIdentityOptionsProvider _identityOptionsProvider;
        private readonly ISystemIdProvider _systemIdProvider;

        public SendLinkConfirmAccountCommandHandler(
            UserManager<AppUser> userManager
            , INotificationService notifyService
            , IHostEnvironment webHostEnvironment
            , IIdentityOptionsProvider identityOptionsProvider
            , ISystemIdProvider systemIdProvider)
        {
            _userManager = userManager;
            _notifyService = notifyService;
            _webHostEnvironment = webHostEnvironment;
            _identityOptionsProvider = identityOptionsProvider;
            _systemIdProvider = systemIdProvider;
        }
        public async Task<bool> Handle(SendLinkConfirmEmailCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(command.Email);
            if (user == null) throw new Exception("User does not exists");
            if (!user.EmailConfirmed)
            {
                var identityOptions = _identityOptionsProvider.Resolve(_systemIdProvider.SystemId, user.EntityType);
                var code = Uri.EscapeDataString(await _userManager.GenerateEmailConfirmationTokenAsync(user));
                var confirmationLink = string.Format(identityOptions.LinkConfirmEmailPattern, user.Id, code, command.Email);
                await _notifyService.SendTemplateEmailsAsync(new List<EmailTemplateRequest>() {
                        new EmailTemplateRequest()
                        {
                            To = command.Email,
                            Subject = identityOptions.ConfirmEmailTitle,
                            TemplatePath = _webHostEnvironment.MapPath(identityOptions.ConfirmEmailTemplatePath),
                            TemplateData = new {
                                user.FullName,
                                Link = confirmationLink,
                                user.Email
                            }
                        } });
            }
            return !user.EmailConfirmed;
        }

    }
}
