﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class UpdateProfileUserCommand : AuthRequestBase<bool>
    {
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? PreferencedLang { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string? Avatar { get; set; }
    }

    public class UpdateProfileUserCommandValidator : AbstractValidator<UpdateProfileUserCommand>
    {
        public UpdateProfileUserCommandValidator()
        {

        }
    }

    public class UpdateProfileUserCommandHandler : IRequestHandler<UpdateProfileUserCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;

        public UpdateProfileUserCommandHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(UpdateProfileUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.GetUserId().ToString());        
            user.FullName = request.FullName;
            user.PhoneNumber = request.PhoneNumber;
            user.PreferencedLang = request.PreferencedLang;
            user.Avatar = request.Avatar;
            if (string.IsNullOrWhiteSpace(user.PreferencedLang))
                user.PreferencedLang = CultureInfo.CurrentCulture.Name;

            var result = await _userManager.UpdateAsync(user);

            return result.Succeeded;
        }
    }
}
