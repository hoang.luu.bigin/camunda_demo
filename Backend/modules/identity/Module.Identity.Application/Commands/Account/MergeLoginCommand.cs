﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Models;
using Module.Identity.Application.Queries;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class MergeLoginCommand : IRequest<bool>
    {
        public string ExternalToken { get; set; }
        public string ExternalProvider { get; set; }
    }

    public class MergeLoginCommandValidator : AbstractValidator<MergeLoginCommand>
    {
        public MergeLoginCommandValidator()
        {
            RuleFor(x => x.ExternalProvider).NotNull().NotEmpty();
            RuleFor(x => x.ExternalToken).NotNull().NotEmpty();
        }
    }

    public class MergeLoginCommandHandler : IRequestHandler<MergeLoginCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMediator _mediator;

        public MergeLoginCommandHandler( UserManager<AppUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }
        public async Task<bool> Handle(MergeLoginCommand command, CancellationToken cancellationToken)
        {
            ExternalUserInfo externalUserInfo = await _mediator.Send(new GetExternalUserInfoQuery()
            {
                Token = command.ExternalToken,
                Provider = command.ExternalProvider
            });

            //Check exist user
            if (!string.IsNullOrWhiteSpace(externalUserInfo.Id))
            {
                if (await _userManager.FindByLoginAsync(command.ExternalProvider, externalUserInfo.Id) == null)
                {
                    AppUser user = await _userManager.FindByEmailAsync(externalUserInfo.Email);
                    if(user == null) throw new Exception("User is not exists");
                    var identityResult = await _userManager.AddClaimsAsync(user, new List<Claim>()
                            {
                                new Claim(command.ExternalProvider + ":name", externalUserInfo.FullName),
                                 new Claim(command.ExternalProvider + ":email", externalUserInfo.Email)
                            });
                    if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);

                    identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(command.ExternalProvider, externalUserInfo.Id, command.ExternalProvider));
                    if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                }
            }

            return true;
        }

    }
}
