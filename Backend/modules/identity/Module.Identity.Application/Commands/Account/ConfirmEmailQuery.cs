﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class ConfirmEmailQuery : IRequest<bool>
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }

    public class ConfirmationEmailQueryValidator : AbstractValidator<ConfirmEmailQuery>
    {
    }

    public class ConfirmationEmailQueryHandler : IRequestHandler<ConfirmEmailQuery, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public ConfirmationEmailQueryHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(ConfirmEmailQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var identityResult = await _userManager.ConfirmEmailAsync(user, request.Token);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            identityResult = await _userManager.UpdateSecurityStampAsync(user);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            return identityResult.Succeeded;
        }
    }
}
