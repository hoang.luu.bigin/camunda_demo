﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class ChangePasswordCommand : AuthRequestBase<bool>
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }

    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator()
        {

        }
    }

    public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public ChangePasswordCommandHandler(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<bool> Handle(ChangePasswordCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(command.GetUserId().ToString());
            var canSignIn = await _signInManager.CheckPasswordSignInAsync(user, command.OldPassword, false);
            if (canSignIn.Succeeded)
            {
                var identityResult = await _userManager.ChangePasswordAsync(user, command.OldPassword, command.Password);
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                user.ForceChangePassword = false;
                identityResult = await _userManager.UpdateAsync(user);
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);

            }
            return canSignIn.Succeeded;
        }
    }
}
