﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class ResetPasswordCommand : IRequest<bool>
    {
        /// <summary>
        /// Phone number
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

    }

    public class ReserPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        public ReserPasswordCommandValidator()
        {

        }
    }

    public class ReserPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public ReserPasswordCommandHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var identityResult = await _userManager.ResetPasswordAsync(user, request.Token, request.Password);
            if (!identityResult.Succeeded) 
                throw new Exception(identityResult.Errors.First().Description);

            user.ForceChangePassword = false;
            user.EmailConfirmed = true;

            identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded) 
                throw new Exception(identityResult.Errors.First().Description);

            return identityResult.Succeeded;
        }
    }
}
