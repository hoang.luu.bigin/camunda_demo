﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class ResetPasswordWithoutTokenCommand : IRequest<bool>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class ResetPasswordWithoutTokenCommandValidator : AbstractValidator<ResetPasswordWithoutTokenCommand>
    {
        public ResetPasswordWithoutTokenCommandValidator()
        {

        }
    }

    public class ResetPasswordWithoutTokenCommandHandler : IRequestHandler<ResetPasswordWithoutTokenCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public ResetPasswordWithoutTokenCommandHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(ResetPasswordWithoutTokenCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByNameAsync(request.UserName);
            string token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var identityResult = await _userManager.ResetPasswordAsync(user, token, request.Password);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            user.ForceChangePassword = false;
            identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            return identityResult.Succeeded;
        }
    }
}
