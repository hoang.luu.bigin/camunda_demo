﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class UnLinkExternalLoginCommand : AuthRequestBase<bool>
    {
        public string ExternalProvider { get; set; }
        public string ExternalProviderKey { get; set; }
    }

    public class UnLinkExternalLoginCommandValidator : AbstractValidator<UnLinkExternalLoginCommand>
    {
        public UnLinkExternalLoginCommandValidator()
        {
        }
    }

    public class UnLinkExternalLoginCommandHandler : IRequestHandler<UnLinkExternalLoginCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        public UnLinkExternalLoginCommandHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(UnLinkExternalLoginCommand command, CancellationToken cancellationToken)
        {
            AppUser user = await _userManager.FindByIdAsync(command.GetUserId().ToString());
            var identityResult = await _userManager.RemoveLoginAsync(user, command.ExternalProvider, command.ExternalProviderKey);
            if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            return await Task.FromResult(true);
        }

    }
}
