﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Models;
using Module.Identity.Application.Queries;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class LinkExternalLoginCommand : AuthRequestBase<AppUserLoginInfo?>
    {
        public string ExternalToken { get; set; }
        public string ExternalProvider { get; set; }
    }

    public class LinkExternalLoginCommandValidator : AbstractValidator<LinkExternalLoginCommand>
    {
        public LinkExternalLoginCommandValidator()
        {
            RuleFor(x => x.ExternalProvider).NotNull().NotEmpty();
            RuleFor(x => x.ExternalToken).NotNull().NotEmpty();
        }
    }

    public class LinkExternalLoginCommandHandler : IRequestHandler<LinkExternalLoginCommand, AppUserLoginInfo?>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMediator _mediator;
        public LinkExternalLoginCommandHandler( UserManager<AppUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }
        public async Task<AppUserLoginInfo?> Handle(LinkExternalLoginCommand command, CancellationToken cancellationToken)
        {
            ExternalUserInfo externalUserInfo = await _mediator.Send(new GetExternalUserInfoQuery()
            {
                Token = command.ExternalToken,
                Provider = command.ExternalProvider
            });
            //Check exist user
            if (!string.IsNullOrWhiteSpace(externalUserInfo.Id))
            {
                if (await _userManager.FindByLoginAsync(command.ExternalProvider, externalUserInfo.Id) == null)
                {
                    AppUser user = await _userManager.FindByIdAsync(command.GetUserId().ToString());

                    var identityResult = await _userManager.AddClaimsAsync(user, new List<Claim>()
                            {
                                new Claim(command.ExternalProvider + ":name", externalUserInfo.FullName),
                                new Claim(command.ExternalProvider + ":email", externalUserInfo.Email)
                            });
                    if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);

                    identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(command.ExternalProvider, externalUserInfo.Id, command.ExternalProvider));
                    if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                    return new AppUserLoginInfo()
                    {
                        LoginProvider = command.ExternalProvider,
                        ProviderDisplayName = command.ExternalProvider,
                        ProviderKey = externalUserInfo.Id,
                        Name = externalUserInfo.FullName
                    };
                }
            }
            return null;
        }
    }
}
