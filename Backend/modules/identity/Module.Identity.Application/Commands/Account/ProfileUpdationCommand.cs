﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class ProfileUpdationCommand : AuthRequestBase<bool>
    {
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? PreferencedLang { get; set; }
        public string? Avatar { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }

    public class ProfileUpdationCommandValidator : AbstractValidator<ProfileUpdationCommand>
    {
        public ProfileUpdationCommandValidator()
        {

        }
    }

    public class ProfileUpdationCommandHandler : IRequestHandler<ProfileUpdationCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IIdentityOptionsProvider _identityOptionsProvider;

        public ProfileUpdationCommandHandler(UserManager<AppUser> userManager
            , IIdentityOptionsProvider identityOptionsProvider)
        {
            _userManager = userManager;
            _identityOptionsProvider = identityOptionsProvider;
        }
        public async Task<bool> Handle(ProfileUpdationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.GetUserId().ToString());
            var identityOptions = _identityOptionsProvider.Resolve(user.SystemId, user.EntityType);
            if (!identityOptions.AllowUpdateProfile) throw new Exception($"Profile cannot be updated for users whose entityType is {user.EntityType}");
            
            user.FullName = request.FullName;
            user.PhoneNumber = request.PhoneNumber;
            user.PreferencedLang = request.PreferencedLang;
            user.Avatar = request.Avatar;
            if (string.IsNullOrWhiteSpace(user.PreferencedLang))
                user.PreferencedLang = CultureInfo.CurrentCulture.Name;

            var result = await _userManager.UpdateAsync(user);

            return result.Succeeded;
        }
    }
}
