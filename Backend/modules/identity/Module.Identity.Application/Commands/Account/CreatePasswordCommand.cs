﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Identity.Application.Commands.Account
{
    public class CreatePasswordCommand : AuthRequestBase<bool>
    {
        public string Password { get; set; }

    }

    public class CreatePasswordCommandValidator : AbstractValidator<CreatePasswordCommand>
    {
        public CreatePasswordCommandValidator()
        {

        }
    }

    public class CreatePasswordCommandHandler : IRequestHandler<CreatePasswordCommand, bool>
    {
        private readonly UserManager<AppUser> _userManager;

        public CreatePasswordCommandHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<bool> Handle(CreatePasswordCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(command.GetUserId().ToString());
            if (string.IsNullOrWhiteSpace(user.PasswordHash))
            {
                var identityResult = await _userManager.AddPasswordAsync(user, command.Password);
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
                user.ForceChangePassword = false;
                identityResult = await _userManager.UpdateAsync(user);
                if (!identityResult.Succeeded) throw new Exception(identityResult.Errors.First().Description);
            }
            return true;
        }
    }
}
