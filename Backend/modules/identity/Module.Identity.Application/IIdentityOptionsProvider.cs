﻿namespace Module.Identity.Application
{
    public  interface IIdentityOptionsProvider
    {
        IdentityOptions Resolve(byte systemId, string entityType);
    }
}
