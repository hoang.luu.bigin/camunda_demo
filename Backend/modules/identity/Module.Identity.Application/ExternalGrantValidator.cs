﻿using CloudKit.Infrastructure.OTPService;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Module.Identity.Application.Commands.Account;
using Module.Identity.Application.Queries;
using Module.Identity.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Module.Identity.Application
{
    public class ExternalGrantValidator : IExtensionGrantValidator
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IOtpService _otpService;
        private readonly IMediator _mediator;

        public ExternalGrantValidator(
            IOtpService otpService,
            IMediator mediator,
            UserManager<AppUser> userManager
            )
        {
            _userManager = userManager;
            _otpService = otpService;
            _mediator = mediator;
        }

        public string GrantType => "external";

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            var externalToken = context.Request.Raw.Get("external_token");
            var externalProvider = context.Request.Raw.Get("external_provider");
            
            if (string.IsNullOrWhiteSpace(externalProvider) || string.IsNullOrWhiteSpace(externalToken))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                return;
            }
            AppUser user = null;
            bool isValid = false;
            switch (externalProvider)
            {
                case "facebook":
                case "google":

                    var externalUserInfo = await _mediator.Send(new GetExternalUserInfoQuery()
                    {
                        Token = externalToken,
                        Provider = externalProvider
                    });
                    if (!string.IsNullOrWhiteSpace(externalUserInfo.Id))
                    {
                        user = await _userManager.FindByLoginAsync(externalUserInfo.Provider, externalUserInfo.Id);
                        isValid = user != null;
                    }
                    break;
                    
                case "phoneOtp":
                    string[] otpParams = externalToken.Split('|');
                    string externalId = otpParams[0];
                    var otpRequestId = Guid.Parse(otpParams[1]);
                    var otpToken = otpParams[2];
                    if (await _otpService.ValidateOtpAsync(otpRequestId, externalId, otpToken))
                    {
                        user = await _userManager.FindByLoginAsync(externalProvider, externalId);
                        isValid = user != null;
                    }
                    break;
                case "confirmEmail":
                    string[] confirmEmailParams = externalToken.Split('|');
                    if (await _mediator.Send(new ConfirmEmailQuery() { 
                        UserId = confirmEmailParams[0],
                        Token = confirmEmailParams[1]
                    }))
                    {
                        user = await _userManager.FindByIdAsync(confirmEmailParams[0]);
                        isValid = user != null;
                    }
                    break;
                case "resetPassword":
                    string[] resetPasswordParams = externalToken.Split('|');
                    if (await _mediator.Send(new ResetPasswordCommand()
                    {
                        UserId = resetPasswordParams[0],
                        Token = resetPasswordParams[1],
                        Password = resetPasswordParams[2]
                    }))
                    {
                        user = await _userManager.FindByIdAsync(resetPasswordParams[0]);
                        isValid = user != null;
                    }
                    break;
                default:
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                    return;
            }

            if (isValid)
            {

                var filtered = new List<Claim>();
                if (user.FullName != null)
                    filtered.Add(new Claim(JwtClaimTypes.Name, user.FullName));
                if(user.PhoneNumber != null)
                    filtered.Add(new Claim(JwtClaimTypes.PhoneNumber, user.PhoneNumber));
                if (user.Email != null)
                    filtered.Add(new Claim(JwtClaimTypes.Email, user.Email));
                
                context.Result = new GrantValidationResult(user.Id, GrantType, filtered);
            }
            else
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
            return;
        }
    }
    
}
