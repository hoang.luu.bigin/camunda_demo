﻿using Microsoft.AspNetCore.Identity;
using Module.Identity.Domain.Models;
using System;
using System.Text;

namespace Module.Identity.Application
{
    public class RandomPasswordGenerator
    {
        private readonly UserManager<AppUser> _userManager;
        public RandomPasswordGenerator(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public string Generate()
        {
            var options = _userManager.Options.Password;

            int length = options.RequiredLength >= 4 ? options.RequiredLength : 4;

            var s = new char[length];
            for (var i = 0; i < s.Length; i++)
                s[i] = '0';

            if (options.RequireNonAlphanumeric)
                s[0] = '1';
            if (options.RequireDigit)
                s[1] = '2';
            if (options.RequireLowercase)
                s[2] = '3';
            if (options.RequireUppercase)
                s[3] = '4';

            Random random = new Random();

            for (var i = 0; i < s.Length; i++)
            {
                var i1 = random.Next(0, s.Length);
                var i2 = random.Next(0, s.Length);

                var temp = s[i1];
                s[i1] = s[i2];
                s[i2] = temp;
            }

            StringBuilder password = new StringBuilder();
            char c;

            for (var i = 0; i < s.Length; i++)
            {
                c = s[i] switch
                {
                    '0' => (char)random.Next(33, 126),
                    '1' => (char)random.Next(33, 48),
                    '2' => (char)random.Next(48, 58),
                    '3' => (char)random.Next(97, 123),
                    '4' => (char)random.Next(65, 91),
                    _ => (char)0,
                };

                if (c != 0)
                    password.Append(c);
            }

            return password.ToString();
        }
    }
}
