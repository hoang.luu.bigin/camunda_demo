﻿using _Shared.Application;
using System;
using System.Collections.Generic;

namespace Module.Identity.Application
{
    public class IdentityPermissionRegistration : IPermissionRegistration
    {
        public PermissionDescriptionGroupCollection Register(PermissionDescriptionGroupCollection permissionCollection)
        {
            permissionCollection.Add(
                new PermissionDescriptionGroup()
                {
                    Code = "Identity",
                    Priority = 1.0f,
                    Permissions = new List<PermissionDescription>()
                    {
                        new PermissionDescription()
                        {
                            Code = "ManageAdmin"
                        },
                        new PermissionDescription()
                        {
                            Code = "ViewAdmin"
                        }
                    }
                }
            );
            return permissionCollection;
        }
    }
}
