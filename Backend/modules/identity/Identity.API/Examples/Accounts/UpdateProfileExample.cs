﻿using Module.Identity.Application.Commands.Account;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;

namespace Identity.API.Examples.Accounts
{
    public class UpdateProfileExample : IMultipleExamplesProvider<ProfileUpdationCommand>
    {
        public IEnumerable<SwaggerExample<ProfileUpdationCommand>> GetExamples()
        {
            return new List<SwaggerExample<ProfileUpdationCommand>>() { 
                new SwaggerExample<ProfileUpdationCommand>()
                {
                    Name = "Ex1",
                    Value = new ProfileUpdationCommand()
                    {
                        DateOfBirth = DateTime.Now,
                        FullName = "Bigin VN",
                        PhoneNumber = "0352821821",
                        PreferencedLang = "Vi-vn"
                    }
                },
                 new SwaggerExample<ProfileUpdationCommand>()
                {
                    Name = "Ex2",
                    Value = new ProfileUpdationCommand()
                    {
                        DateOfBirth = DateTime.Now,
                        FullName = "Bigin VN 2",
                        PhoneNumber = "0352821822",
                        PreferencedLang = "En-en"
                    }
                }
            };
        }
    }

}
