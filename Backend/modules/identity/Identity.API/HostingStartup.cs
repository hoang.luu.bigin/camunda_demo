﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Module.Identity.Application;
using Module.Identity.Application.Commands;
using Module.Identity.Application.Shared;
using Module.Identity.Infrastructure;

[assembly: HostingStartup(typeof(Identity.API.HostingStartup))]
namespace Identity.API
{
    public class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {

                services.Scan(scan =>
                    scan
                        .FromAssembliesOf(typeof(RegistrationCommand))
                        .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                            .AsImplementedInterfaces()
                            .WithTransientLifetime());

                services.AddValidatorsFromAssemblyContaining(typeof(RegistrationCommand), ServiceLifetime.Transient);
                services.AddTransient<RandomPasswordGenerator>();
                services.AddTransient<IdentityClient>();
                services.AddTransient<GoogleClient>();

                services.AddAuthorization(options =>
                {
                    options.AddPolicy(PolicyConst.RootAdminOnly, policy =>
                        policy.RequireClaim("system_id", new string[] { SystemIdType.RootAdmin.ToString() })
                    );
                });
            });

            Mapper.Register();

            builder.ConfigureAppConfiguration((context, config) =>
            {
            });
        }
    }
}
