﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Hosting;
using Module.Identity.Application;
using Module.Identity.Application.Commands.Account;
using Module.Identity.Application.Models;
using Module.Identity.Application.Queries.Account;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.API.Controllers
{
    [ApiController]
    [Route("v1/Identity/[controller]")]
    [Produces("application/json")]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IHostEnvironment _hostEnvironment;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="hostEnvironment"></param>
        public AccountController(IMediator mediator, IHostEnvironment hostEnvironment)
        {
            _mediator = mediator;
            _hostEnvironment = hostEnvironment;
        }

        [HttpGet("CheckExistsEmail")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> CheckExistsEmailAsync([FromQuery] CheckExistsEmailQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("CheckUserStatus")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<byte> CheckUserStatusAsync([FromQuery] CheckUserStatusQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("CheckValidToken")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> CheckValidTokenAsync([FromQuery] CheckValidTokenQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("ConfirmEmail")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> ConfirmEmailAsync([FromQuery] ConfirmEmailQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Send Link Reset password
        /// </summary>
        /// <remarks>Send Link Reset password</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("SendLinkResetPassword")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Send Link Reset password")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> SendLinkResetPasswordAsync([FromBody] SendLinkResetPasswordCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// Send Link Confirm Email
        /// </summary>
        /// <remarks>Send Link Confirm Email</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("SendLinkConfirmEmail")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Send Link Confirm Email")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> SendLinkConfirmEmailAsync([FromBody] SendLinkConfirmEmailCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <remarks>Change password</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("ResetPassword")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Reset Password")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> ResetPasswordAsync([FromBody] ResetPasswordCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("[action]")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Reset password without token")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "ApiKey")]
        public async Task<IActionResult> ResetPasswordWithoutTokenAsync([FromBody] ResetPasswordWithoutTokenCommand command)
        {
            if (_hostEnvironment.IsProduction())
                return NotFound();
            return new JsonResult(await _mediator.Send(command));
        }

        [HttpPost("GenPass")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public string GenPass([FromServices] RandomPasswordGenerator randomPasswordGenerator)
        {
            return randomPasswordGenerator.Generate();
        }

        /// <summary>
        /// Create password
        /// </summary>
        /// <remarks>Create password</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("CreatePassword")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Create Password")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> CreatePasswordAsync([FromBody] CreatePasswordCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <remarks>Change password</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("ChangePassword")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Change Password")]
        [SwaggerResponse(statusCode: 400, type: typeof(ModelStateDictionary), description: "Error Description")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> ChangePasswordAsync([FromBody] ChangePasswordCommand command)
        {
            return await _mediator.Send(command);
        }
        
        [HttpPost("LinkExternalLogin")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [SwaggerResponse(statusCode: 200, type: typeof(AppUserLoginInfo), description: "Successful!")]
        [SwaggerResponse(statusCode: 409, type: typeof(string), description: "The account is registered!")]
        public async Task<IActionResult> LinkExternalLoginAsync([FromBody] LinkExternalLoginCommand command)
        {
            var userLoginInfo = await _mediator.Send(command);
            if (userLoginInfo == null)
                return new ConflictResult();
            return new JsonResult(userLoginInfo);
        }

        [HttpDelete("UnLinkExternalLogin")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> UnLinkExternalLoginAsync([FromBody] UnLinkExternalLoginCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("MergeLogin")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        public async Task<bool> MergeLoginAsync([FromBody] MergeLoginCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("GetUserLogins")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IEnumerable<AppUserLoginInfo>> GetUserLoginsAsync([FromQuery] GetUserLoginsQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Get user's profile
        /// </summary>
        /// <remarks>Get user's profile</remarks>
        /// <response code="200">User profile</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet("Profile")]
        [SwaggerResponse(statusCode: 200, type: typeof(Profile), description: "User profile")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<Profile> GetProfile()
        {
            return await _mediator.Send(new GetProfileQuery());
        }

        /// <summary>
        /// Update profile
        /// </summary>
        /// <remarks>Update user's profile</remarks>
        /// <param name="command">Profile updation</param>
        /// <response code="200">Ok</response>
        /// <response code="401">Unauthorized</response>
        [HttpPut("Profile")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Profile updated")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerOperation(
            Tags = new[] { "Identity" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> UpdateProfileAsync([FromBody] ProfileUpdationCommand command)
        {
            return await _mediator.Send(command);
        }

    }
}
