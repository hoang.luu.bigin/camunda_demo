﻿using CloudKit.Common.Types;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Module.Identity.Application.Commands.Clients;
using Module.Identity.Application.Models;
using Module.Identity.Application.Queries.Clients;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace Identity.API.Controllers
{
    [ApiController]
    [Route("v1/Identity/[controller]")]
    [Produces("application/json")]

    public class ClientController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ClientController(
            IMediator mediator
        )
        {
            _mediator = mediator;
        }

        [HttpGet]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<EntityList<AdminUserRM>> GetClientUsersAsync([FromQuery] GetClientUsersQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<Profile> ViewAdmin(string id)
        {
            return await _mediator.Send(new GetClientProfileQuery { Id = id });
        }

        [HttpPost]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> CreateClientAsync([FromBody] CreateClientCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> UpdateClientAsync([FromBody] UpdateClientCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("ChangeStatus")]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> DeactivateOrActivateClientAsync([FromBody] DeactivateOrActivateClientCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPost("ResetClientUserPassword")]
        [SwaggerOperation(
            Tags = new[] { "Identity.Client" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> ResetUserPasswordAsync([FromBody] ResetClientUserPasswordCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

