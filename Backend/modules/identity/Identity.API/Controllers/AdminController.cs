﻿using CloudKit.Common.Types;
using CloudKit.UI.AspNetCore.Extensions;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Module.Identity.Application.Commands.Admin;
using Module.Identity.Application.Models;
using Module.Identity.Application.Queries.Admin;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.API.Controllers
{
    [ApiController]
    [Route("v1/Identity/[controller]")]
    [Produces("application/json")]
    public class AdminController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AdminController(
            IMediator mediator
        )
        {
            _mediator = mediator;
        }

        [HttpGet]
        [SwaggerOperation(
            Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<EntityList<AdminUserRM>> GetAdminsAsync([FromQuery] GetAdminsQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<Profile> ViewAdmin(Guid id)
        {
            return await _mediator.Send(new GetAdminProfileQuery { Id = id });
        }

        [HttpPost]
        [SwaggerOperation(
           Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "ApiKey, Bearer")]
        public async Task<bool> CreateAdminAsync([FromBody] CreateNormalAdminCommand command)
        {
            var identity = HttpContext.User.Identity;

            if (identity.AuthenticationType == "ApiKey")
            {
                // create root admin
                return await _mediator.Send(command.Adapt<CreateRootAdminCommand>());
            }
            else if (identity.AuthenticationType == "AuthenticationTypes.Federation") // "Bearer"
            {
                // create normal admin
                return await _mediator.Send(command);
            }
            else
                throw new Exception("ApiKey or Bearer authentication scheme is required.");
        }

        [HttpPut]
        [SwaggerOperation(
            Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> UpdateAdminAsync([FromBody] UpdateAdminCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("ChangeStatus")]
        [SwaggerOperation(
            Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<bool> DeactivateOrActivateAdmin([FromBody] DeactivateOrActivateAdminCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("CheckIp")]
        [SwaggerOperation(
           Tags = new[] { "Identity.Admin" }
        )]
        [Authorize(AuthenticationSchemes = "ApiKey")]
        public IActionResult CheckIp()
        {
            return Ok(new
            {
                IP = HttpContext.GetRequestIpAddress(),
                UserAgent = HttpContext.GetUserAgent(),
                DeviceName = HttpContext.GetDeviceName(),
                Headers = HttpContext.Request.Headers.Where(x => x.Key.ToLower() != "api-key").ToList()
            });
        }
    }
}

