﻿using Module.Configuration.Domain.Models;
 using System.Collections.Generic;
using System.Threading.Tasks;
 
namespace Module.Configuration.Domain.Constracts
{
    public interface ISettingRepo : IRepositoryBase<Setting>
    {
        Task<List<Setting>> GetSettingsByGroupAsync(string group);
        Task<Setting> GetSettingByIdAsync(string id);
        void AddOrUpdateSetting(string id, string value,
            string name, string group, string dataType,
            string culture = "", bool isPublic = false);
     }
}
