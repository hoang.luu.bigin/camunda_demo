﻿using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface ITagHtmlRepo : IRepositoryBase<TagHtml>
    {
       Task<List<TagHtml>> GetTagHtmlsAsync(string culture = "");
        IEnumerable<TagHtml> GetTagsBySearch(string keyword, int start, int length, string orderBy, bool isActiveOnly, string culture, out int total);
        Task<bool> UpdateStatusAsync(int tagHtmlId, bool status);
        Task<TagHtml> GetTagHtmlByIdAsync(int id);

        Task<List<TagHtml>> GetActiveTagHtmlsAsync(string culture = "");
    }
}
