﻿using Module.Configuration.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface IRepositoryBase<T> where T : EntityBase
    {
        IEnumerable<T> FindAll();
        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void MarkAsDeleted(T entity);
        void Disable(T entity);
        void Save();

        Task<int> SaveAsync();
    }
}
