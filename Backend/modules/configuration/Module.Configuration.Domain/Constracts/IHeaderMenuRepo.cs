﻿using Module.Configuration.Domain.DTO;
using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface IHeaderMenuRepo
    {
        Task<List<HeaderMenu>> GetHeaderMenuGroupsAsync(string culture);

        Task<int> AddOrUpdateAsync(HeaderMenu footer);
        Task<bool> DeleteHeaderAsync(int id);
        Task<bool> SortHeaderAsync(List<HeaderSortInput> inputs);
        Task RemoveHeaderMenusFromCacheAsync();

    }
}
