﻿using Module.Configuration.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface IVirtualFileRepository : IRepositoryBase<VirtualFile>
    {
        Task<VirtualFile> GetFileByPath(string path);
        Task<List<VirtualFile>> GetListTokenAsync();
        Task<int> SaveFileUploadAsync(VirtualFile file);
    }
}
