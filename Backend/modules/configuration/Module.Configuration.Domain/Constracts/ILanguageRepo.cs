﻿using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface ILanguageRepo : IRepositoryBase<Language>
    {
        Task<List<Language>> GetLanguagesAsync(bool isActive, string culture);
        Task<Language> GetLanguageByIdAsync(string id, bool isActive, string culture);
        Task<Language> GetDefaultLanguageAsync(bool isActive, string culture);

        Task<bool> UpdateStatusAsync(string languageId, bool status);
        Task<List<Language>> GetLanguagesByScopeAsync(bool isActive, LanguageScope scope, string culture);
    }
}
