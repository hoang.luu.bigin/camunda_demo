﻿using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface IConfigurationService
    {
        Task<List<Language>> GetLanguagesAsync(bool isActive, string culture);
        Task<Language> GetLanguageByIdAsync(string id, bool isActive, string culture);
        Task<Language> GetDefaultLanguageAsync(bool isActive, string culture);

        Task<List<Setting>> GetSettingsByGroupAsync(string group);
        Task<Setting> GetSettingByIdAsync(string id);
    }
}
