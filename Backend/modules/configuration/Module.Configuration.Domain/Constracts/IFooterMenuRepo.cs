﻿using Module.Configuration.Domain.DTO;
using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Domain.Constracts
{
    public interface IFooterMenuRepo
    {
        Task<List<FooterMenu>> GetFooterMenuGroupsAsync(string culture);

        Task<int> AddOrUpdateAsync(FooterMenu footer);
        Task<bool> DeleteFooterAsync(int id);
        Task<bool> SortFooterAsync(List<FooterSortInput> inputs);
    }
}
