﻿namespace Module.Configuration.Domain.DTO
{
    public class ContentInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
