﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module.Configuration.Domain.DTO
{
    public class RoleInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
