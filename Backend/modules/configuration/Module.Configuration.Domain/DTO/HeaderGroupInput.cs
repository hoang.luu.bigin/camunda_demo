﻿using Module.Configuration.Domain.Models;
using System.Collections.Generic;

namespace Module.Configuration.Domain.DTO
{
    public class HeaderGroupInput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }

        public int? ParentId { get; set; }
        public HeaderMenuType Type { get; set; }

        public int ContentId { get; set; }
        public string ContentName { get; set; }

        public List<HeaderGroupInput> Childs { get; set; }
        public List<ContentCategoryInfo> Categories { get; set; }

        public List<HeaderGroupLocalize> Localize { get; set; }
        public List<Language> Languages { get; set; }
    }

    public class HeaderGroupLocalize
    {
        public string Lang { get; set; }
        public string Name { get; set; }
    }

    public class HeaderSortInput
    {
        public int Id { get; set; }
        public int Priority { get; set; }
    }
}
