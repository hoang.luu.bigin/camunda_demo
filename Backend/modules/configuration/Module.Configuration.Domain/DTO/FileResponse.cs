﻿namespace Module.Configuration.Domain.DTO
{
    public class FileResponse
    {
        public string fileName { get; set; }
        public string filePath { get; set; }
        public string fileUrl { get; set; }
        public string fileExtension { get; set; }
        public string thumbnailName { get; set; }
        public string thumbnailPath { get; set; }
        public string thumbnailUrl { get; set; }
    }
}
