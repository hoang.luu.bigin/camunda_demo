﻿namespace Module.Configuration.Domain.DTO
{
    public class ContentCategoryInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
    }
}
