﻿namespace Module.Configuration.Domain.DTO
{
    public class ConfigurationOption
    {
        public string SeoHost { get; set; }
        public string AppName { get; set; }
    }
}
