﻿using Module.Configuration.Domain.Models;
using System.Collections.Generic;

namespace Module.Configuration.Domain.DTO
{
    public class FooterGroupInput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }

        public int? ParentId { get; set; }
        public int Type { get; set; }

        public int ContentId { get; set; }
        public string ContentName { get; set; }

        public List<FooterGroupInput> Childs { get; set; }
        public List<ContentCategoryInfo> Categories { get; set; }

        public List<FooterGroupLocalize> Localize { get; set; }
        public List<Language> Languages { get; set; }
    }

    public class FooterGroupLocalize
    {
        public string Lang { get; set; }
        public string Name { get; set; }
    }

    public class FooterSortInput
    {
        public int Id { get; set; }
        public int Priority { get; set; }
    }
}
