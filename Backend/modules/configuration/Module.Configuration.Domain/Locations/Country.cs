﻿using CloudKit.Domain;

namespace Module.Configuration.Domain.Locations
{
    public class Country : EntityBase<string>
    {
        public Country() : base(default) { }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
    }
}
