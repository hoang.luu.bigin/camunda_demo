﻿using CloudKit.Domain;

namespace Module.Configuration.Domain.Locations
{
    //City/Ward
    public class LocationLevel3 : EntityBase<string>
    {
        public LocationLevel3() : base(default)
        { 
        }
        public string CountryCode { get; set; }
        public string Level1Code { get; set; }
        public string Level2Code { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
    }
}
