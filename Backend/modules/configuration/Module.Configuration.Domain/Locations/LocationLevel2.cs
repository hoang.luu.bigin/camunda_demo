﻿using CloudKit.Domain;
using System;

namespace Module.Configuration.Domain.Locations
{
    //County/District
    public class LocationLevel2 : EntityBase<string>, IEquatable<LocationLevel2>
    {
        public LocationLevel2() : base(default) { }
        public string CountryCode { get; set; }
        public string Level1Code { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }

        public bool Equals(LocationLevel2 other)
        {
            return Code == other.Code && Level1Code == other.Level1Code;
        }
    }
}
