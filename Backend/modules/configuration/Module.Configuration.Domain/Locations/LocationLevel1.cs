﻿using CloudKit.Domain;
using System;

namespace Module.Configuration.Domain.Locations
{
    // State/Province
    public class LocationLevel1 : EntityBase<string>, IEquatable<LocationLevel1>
    {
        public LocationLevel1() : base(default) 
        { }
        public string CountryCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
      

        public bool Equals(LocationLevel1 other)
        {
            return Code == other.Code;
        }
    }
}
