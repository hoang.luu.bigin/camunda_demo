﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class Language : EntityBase
    {
        [Key]
        [MaxLength(50)]
        public string Id { get; set; }

        public string Code { get; set; }

        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Description { get; set; }

        [MaxLength(500)]
        public string Icon { get; set; }
        public int Priority { get; set; }
        public string NativeName { get; set; }
        public LanguageScope Scope { get; set; }

        public virtual List<LanguageLocalization> Localizations { get; set; }
    }

    [Serializable()]
    public class LanguageLocalization : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string LanguageId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Lang { get; set; }

        [MaxLength(500)]
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
    }

    public enum LanguageScope
    {
        All = 0,
        Admin = 1,
        User = 2,
        None = 3
    }
}
