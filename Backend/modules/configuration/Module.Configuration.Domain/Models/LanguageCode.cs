﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class LanguageCode : EntityBase
    {
        [Key]
        [MaxLength(50)]
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
