﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class VirtualFile : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string OriginalPath { get; set; }

        [Required]
        public string VirtualPath { get; set; }

        public FileType Group { get; set; }

        public string Tag { get; set; }
    }
    public enum FileType
    {
        SiteMap = 1,
        Robot = 2,
        Token = 3
    }
}
