﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class Setting : EntityBase
    {
        [Key]
        [MaxLength(200)]
        public string Id { get; set; }

        public string Value { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Group { get; set; }

        [MaxLength(200)]
        public string DataType { get; set; }
        public bool IsPublic { get; set; } = false;
    }
}
