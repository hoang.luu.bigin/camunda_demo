﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class TagHtml : EntityBase
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public TagPosition Position { get; set; }
    }
    public enum TagPosition
    {
        Inside = 0,
        Before = 1,
        After = 2
    }
}
