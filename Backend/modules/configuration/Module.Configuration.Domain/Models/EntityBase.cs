﻿using System;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class EntityBase
    {
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public EntityBase()
        {
            CreatedDate = DateTime.UtcNow;
            UpdatedDate = DateTime.UtcNow;
            IsActive = true;
            IsDeleted = false;
        }
    }
}
