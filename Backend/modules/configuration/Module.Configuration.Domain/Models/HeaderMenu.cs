﻿using Module.Configuration.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class HeaderMenu : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Url { get; set; }
        public int Priority { get; set; }

        public int? ParentId { get; set; }
        public HeaderMenuType Type { get; set; }
        public int ContentId { get; set; }      // newsId, pressId, staticPageId        

        public virtual List<HeaderMenuLocalization> Localizations { get; set; }
    }

    [Serializable()]
    public class HeaderMenuLocalization : EntityBase
    {
        [Required]
        public int HeaderMenuId { get; set; }
        [Required]
        public string Lang { get; set; }

        [MaxLength(500)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Url { get; set; }

        [ForeignKey("HeaderMenuId")]
        [JsonIgnore]
        public virtual HeaderMenu HeaderMenu { get; set; }
    }
}
