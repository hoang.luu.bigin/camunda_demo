﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Module.Configuration.Domain.Models
{
    [Serializable()]
    public class FooterMenu : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Url { get; set; }

        public int Priority { get; set; }

        public int? ParentId { get; set; }
        public int Type { get; set; }
        public int ContentId { get; set; }      // newsId, pressId, staticPageId        

        public virtual List<FooterMenuLocalization> Localizations { get; set; }
    }

    [Serializable()]
    public class FooterMenuLocalization : EntityBase
    {
        [Required]
        public int FooterMenuId { get; set; }
        [Required]
        public string Lang { get; set; }

        [MaxLength(500)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Url { get; set; }

        [ForeignKey("FooterMenuId")]
        [JsonIgnore]
        public virtual FooterMenu FooterMenu { get; set; }
    }
}
