﻿using CloudKit.Infrastructure.Cache;
using CloudKit.Infrastructure.Data.MongoDb;
using Configuration.Application.Queries.Languages;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Module.Configuration.Application.Commands.Settings;
using Module.Configuration.Domain.Constracts;
using Module.Configuration.Infrastructure.Data;
using Module.Configuration.Infrastructure.Data.Repositories;

[assembly: HostingStartup(typeof(Configuration.API.HostingStartup))]
namespace Configuration.API
{
    public class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.AddDbContext<ConfigurationDbContext>(options =>
                    options.UseMySql(context.Configuration.GetConnectionString("ConfigurationConnection"))
                );

                services.AddScoped<ICacheKeyFormatter, CacheKeyFormatter>();
                services.AddTransient<ISettingRepo, SettingRepo>();
                services.AddSingleton<MongoDbConfigurationDbContext>();
                services.Configure<MongoDbConfigurationDbSettings>(context.Configuration.GetSection("ConfigurationDatabaseSettings"));

                services.Scan(scan =>
                    scan
                        .FromAssembliesOf(typeof(GetLanguageByCodeQuery))
                        .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                            .AsImplementedInterfaces()
                            .WithTransientLifetime());

                services.AddValidatorsFromAssemblyContaining(typeof(GetLanguageByCodeQuery), ServiceLifetime.Transient);

            });

            builder.ConfigureAppConfiguration((context, config) =>
            {
            });
        }
    }
}
