﻿using Configuration.Application.Queries.Locations;
using Google.Maps.Places;
using Google.Maps.Places.Details;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Module.Configuration.Application.Queries.Locations;
using Module.Configuration.Domain.Locations;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configuration.API.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    [Produces("application/json")]
    public class LocationController : ControllerBase
    {
        private readonly IMediator _mediator;
        public LocationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("GetAddressByCode")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<GetAddressByCodeResponse> GetAddressByCodeAsync([FromQuery] GetAddressByCodeQuery query) => _mediator.Send(query);


        [HttpGet("GetCountries")]
        [SwaggerOperation(
           Tags = new[] { "Configuration" }
        )]
        public Task<IEnumerable<Country>> GetCountriesAsync([FromQuery] GetCountryListQuery query) => _mediator.Send(query);

        [HttpGet("[action]")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
            , Summary = "Get Location Level1 List(state/province)"
        )]
        public Task<IEnumerable<LocationLevel1>> GetLocationLevel1sAsync([FromQuery] GetLevel1ListQuery query) => _mediator.Send(query);

        [HttpGet("[action]")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
            , Summary = "Get Location Level2 List(county/district)"
        )]
        public Task<IEnumerable<LocationLevel2>> GetLocationLevel2sAsync([FromQuery] GetLevel2ListQuery query) => _mediator.Send(query);

        [HttpGet("[action]")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
            , Summary = "Get Location Level3 List(city/ward)"
        )]
        public Task<IEnumerable<LocationLevel3>> GetLocationLevel3sAsync([FromQuery] GetLevel3ListQuery query) => _mediator.Send(query);


        [HttpGet("GetAutoComplete")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<AutocompleteResponse> GetAutoCompleteAsync([FromQuery] GetAutoCompleteQuery query) => _mediator.Send(query);

        [HttpGet("GetGoogleAddressDetail")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<PlaceDetailsResponse> GetGoogleAddressDetailAsync([FromQuery] GetGoogleAddressDetailQuery query) => _mediator.Send(query);

    }
}
