﻿using Configuration.Application.Queries.Languages;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Module.Configuration.Application.Models;
using Module.Configuration.Domain.Models;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configuration.API.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    [Produces("application/json")]
    public class LanguageController : ControllerBase
    {
        private readonly IMediator _mediator;
        public LanguageController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet("{langCode}")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<Language> GetAsync(string langCode) => _mediator.Send(new GetLanguageByCodeQuery() { LangCode = langCode });

        [HttpGet("GetSupportLanguages")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<IEnumerable<Language>> GetLanguagesAsync([FromQuery]GetLanguageListQuery query) => _mediator.Send(query);

        [HttpGet("GetLanguages")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<List<LanguageCodeRM>> GetLanguageCodesAsync() => _mediator.Send(new GetLanguageCodesQuery());

    }
}
