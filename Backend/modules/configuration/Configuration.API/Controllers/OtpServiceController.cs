﻿using CloudKit.Infrastructure.OTPService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Configuration.API.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    [Produces("application/json")]
    public class OtpServiceController : ControllerBase
    {
        private readonly IOtpService _otpService;
        private readonly IOtpPersistentHandler _otpPersistentHandler;
        private readonly IHostEnvironment _hostEnvironment;

        public OtpServiceController(IOtpService otpService, IOtpPersistentHandler otpPersistentHandler, IHostEnvironment env)
        {
            _otpService = otpService;
            _otpPersistentHandler = otpPersistentHandler;
            _hostEnvironment = env;
        }

        [HttpGet("RequestOtp")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 429)]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public async Task<IActionResult> RequestOtpAsync([FromQuery] string key) {
            var requestId = await _otpService.RequestOtpAsync(key);
                if (requestId == Guid.Empty)
                return StatusCode(429, "Number of otp requests exceeded the limit");
            return new JsonResult(requestId);
        }

        /// <summary>
        /// Get Otp token base on RequestId + PhoneNumber .
        /// This API for non-production enviroment
        /// </summary>
        
        [HttpGet("GetOtp")]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public async Task<IActionResult> GetOtpAsync([FromQuery]Guid requestId, [FromQuery]string key)
        {
            if (_hostEnvironment.IsProduction())
                return NotFound();
            return new JsonResult(await _otpPersistentHandler.GetTokenAsync(requestId, key));
        }
    }
}
