﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Module.Configuration.Application.Commands.Settings;
using Module.Configuration.Application.Models;
using Module.Configuration.Application.Queries.Settings;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Configuration.API.Controllers
{
    [ApiController]
    [Route("v1/Configuration/[controller]")]
    [Produces("application/json")]
    public class SettingsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public SettingsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet()]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<IEnumerable<SettingRM>> GetAsync([FromQuery]GetAllPublicSettings request) => _mediator.Send(request);

        [HttpPost, HttpPut]
        [SwaggerOperation(
            Tags = new[] { "Configuration" }
        )]
        public Task<bool> UpdateAsync([FromBody] List<SettingInput> settings)
        {
            return _mediator.Send(new AddUpdateConfigurationCommand { Settings = settings });
        }
    }
}
