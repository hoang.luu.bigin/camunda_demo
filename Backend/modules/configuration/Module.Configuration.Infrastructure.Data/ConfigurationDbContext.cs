﻿using Module.Configuration.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.EntityConfigurations;
using System.Collections.Generic;
using CloudKit.Infrastructure.Data.EfCore.Core;
using Module.Configuration.Infrastructure.Data.Extensions;

namespace Module.Configuration.Infrastructure.Data
{
    public class ConfigurationDbContext : AppDbContext
    {
        protected override string PrefixTable => "Configuration";
        public ConfigurationDbContext(DbContextOptions<ConfigurationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CountryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProvinceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DistrictEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WardEntityTypeConfiguration());
            modelBuilder.SeedLanguageData();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LanguageLocalization>()
                .HasKey(x => new { x.LanguageId, x.Lang });

            modelBuilder.Entity<FooterMenuLocalization>()
                .HasKey(x => new { x.FooterMenuId, x.Lang });

            modelBuilder.Entity<HeaderMenuLocalization>()
               .HasKey(x => new { x.HeaderMenuId, x.Lang });

            modelBuilder.Entity<Language>().HasData(new List<Language>()
                {
                    new Language()
                    {
                        Id = "vi-VN",
                        Code = "vi",
                        Icon = "/media/flat/vn.svg",
                        NativeName = "Tiếng Việt",
                        Scope = LanguageScope.All,
                        Priority = 1,
                    },
                    new Language()
                    {
                        Id = "en-US",
                        Code = "en",
                        Icon = "/media/flat/020-flag.svg",
                        NativeName = "English (United States)",
                        Scope = LanguageScope.All,
                        Priority = 2
                    }
                });

            modelBuilder.Entity<LanguageLocalization>().HasData(new List<LanguageLocalization>()
                {
                    new LanguageLocalization()
                    {
                        LanguageId = "vi-VN",
                        Lang = "vi-VN",
                        Name = "Tiếng Việt",
                        Description = "",
                    },
                    new LanguageLocalization()
                    {
                        LanguageId = "vi-VN",
                        Lang = "en-US",
                        Name = "Vietnamese",
                        Description = "",
                    },
                    new LanguageLocalization()
                    {
                        LanguageId = "en-US",
                        Lang = "vi-VN",
                        Name = "Tiếng Anh",
                        Description = "",
                    },
                    new LanguageLocalization()
                    {
                        LanguageId = "en-US",
                        Lang = "en-US",
                        Name = "English",
                        Description = "",
                    }
                });

            modelBuilder.Entity<Setting>().HasData(new List<Setting>() { 
                new Setting()
                { 
                    Id = "third-party-google-recaptcha-key",
                    Name = "Google's Recaptcha Key",
                    Group = "ThirdParty",
                    Value = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
                    DataType = "string"
                },
                new Setting()
                {
                    Name = "Google's Recaptcha Secret",
                    Id = "third-party-google-recaptcha-secret",
                    Group = "ThirdParty",
                    Value = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe",
                    DataType = "string"
                }
            });

        }

        public DbSet<FooterMenu> FooterMenu { get; set; }
        public DbSet<FooterMenuLocalization> FooterMenuLocalization { get; set; }
        public DbSet<HeaderMenu> HeaderMenu { get; set; }
        public DbSet<HeaderMenuLocalization> HeaderMenuLocalization { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LanguageLocalization> LanguageLocalizations { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<TagHtml> TagHtmls { get; set; }
        public DbSet<VirtualFile> VirtualFiles { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<LocationLevel1> Provinces { get; set; }
        public virtual DbSet<LocationLevel2> Districts { get; set; }
        public virtual DbSet<LocationLevel3> Wards { get; set; }
        public DbSet<LanguageCode> LanguageCodes { get; set; }
    }

}
