﻿using CloudKit.Infrastructure.Data.MongoDb;
using CsvHelper;
using CsvHelper.Configuration;
using Module.Configuration.Domain.Locations;
using MongoDB.Driver;
using MongoDBMigrations;
using System.Linq;
using System.Globalization;
using System.IO;

namespace Module.Catalog.Infrastructure.Data.Migrations
{
    public class _1_0_0_InsertLocationData : IMigration
    {
        public Version Version => new Version(4, 0, 0);
        public string Name => "Insert Location Data";
        const string PREFIX = "Configuration";

        public void Down(IMongoDatabase database)
        {
            var countryCollection = database.GetCollectionEx<Country>(PREFIX);
            countryCollection.DeleteMany(x => true);
            var locationLevel1Collection = database.GetCollectionEx<LocationLevel1>(PREFIX);
            locationLevel1Collection.DeleteMany(x => true);
            var locationLevel2Collection = database.GetCollectionEx<LocationLevel2>(PREFIX);
            locationLevel2Collection.DeleteMany(x => true);
            var locationLevel3Collection = database.GetCollectionEx<LocationLevel3>(PREFIX);
            locationLevel3Collection.DeleteMany(x => true);
        }

        public void Up(IMongoDatabase database)
        {
            Down(database);

            var countryCollection = database.GetCollectionEx<Country>(PREFIX);
            countryCollection.DeleteMany(x => true);
            countryCollection.InsertOne(new Country() { 
                Name = "United States",
                FullName = "United States",
                Code = "US"
            });

            //Read Csv file
            string dataFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "/MongoMigrations/uscities.csv";
            TextReader reader = new StreamReader(dataFilePath);
            var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<Location>().ToList();

            //Level 1
            var locationLevel1Collection = database.GetCollectionEx<LocationLevel1>(PREFIX);
            var level1Data = records.GroupBy(elem => elem.state_id).Select(x => new LocationLevel1()
            {
                Code = x.Key,
                Name = x.First().state_name,
                FullName = x.First().state_name,
                CountryCode = "US"
            }).ToList();
            locationLevel1Collection.InsertMany(level1Data);

            //Level 2
            var locationLevel2Collection = database.GetCollectionEx<LocationLevel2>(PREFIX);
            var level2Data = records.GroupBy(elem => elem.county_fips).Select(x => new LocationLevel2()
            {
                Level1Code = x.First().state_id,
                Code = x.Key,
                Name = x.First().county_name,
                FullName = x.First().county_name,
                CountryCode = "US"
            }).ToList();
            locationLevel2Collection.InsertMany(level2Data);  
            
            //Level 3
            var locationLevel3Collection = database.GetCollectionEx<LocationLevel3>(PREFIX);
            locationLevel3Collection.InsertMany(records.Select(x => new LocationLevel3()
            {
                Level1Code = x.state_id,
                Level2Code = x.county_fips,
                Code = x.id,
                Name = x.city,
                FullName = x.city,
                CountryCode = "US"
            }).ToList());
        }
    }
    public sealed class Location
    {
        public string id { get; set; }
        public string city { get; set; }
        public string state_id { get; set; }
        public string state_name { get; set; }
        public string county_fips { get; set; }
        public string county_name { get; set; }
    }
    
}

