﻿using CloudKit.Infrastructure.Data.MongoDb;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Module.Configuration.Domain.Locations;
using MongoDB.Driver;

namespace Module.Configuration.Infrastructure.Data
{
    public class MongoDbConfigurationDbSettings : CloudKit.Infrastructure.Data.MongoDb.MongoDatabaseSettings
    {
    }

    public class MongoDbConfigurationDbContext : MongoDbBaseContext
    {
        protected override string PrefixTable => "Configuration";
        public MongoDbConfigurationDbContext(
            IOptionsMonitor<MongoDbConfigurationDbSettings> settings, 
            ILoggerFactory loggerFactory
            ) : base(settings, loggerFactory)
        {
        }

        public IMongoCollection<Country> Countries { get { return GetCollection<Country>(); } }
        public IMongoCollection<LocationLevel1> LocationLevel1s { get { return GetCollection<LocationLevel1>(); } }
        public IMongoCollection<LocationLevel2> LocationLevel2s { get { return GetCollection<LocationLevel2>(); } }
        public IMongoCollection<LocationLevel3> LocationLevel3s { get { return GetCollection<LocationLevel3>(); } }

    }
}
