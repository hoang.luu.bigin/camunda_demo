﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class VirtualFileRepository : RepositoryBase<VirtualFile>, IVirtualFileRepository
    {
        private new readonly ConfigurationDbContext _context;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;


        public VirtualFileRepository(
            ConfigurationDbContext dbContext,
            IDistributedCache cache,
            ICacheKeyFormatter cacheKeyFormatter
        )
            : base(dbContext)
        {
            _context = dbContext;
            _cache = cache;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        private Task<List<VirtualFile>> GetVirtualFilesFromCacheAsync()
        {
            return _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_VirtualFiles), () =>
            {
                return _context.VirtualFiles
                    .Where(x => x.IsDeleted == false)
                    .ToListAsync();
        }, false);
        }

        public override async Task<int> SaveAsync()
        {
            await Task.WhenAll(base.SaveAsync(), _cache.RemoveAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_VirtualFiles)));

            return 1;
        }

        public async Task<VirtualFile> GetFileByPath(string path)
        {
            var vFiles = await GetVirtualFilesFromCacheAsync();
            return vFiles.FirstOrDefault(x => x.VirtualPath.Equals(path, StringComparison.OrdinalIgnoreCase));
        }

        public async Task<List<VirtualFile>> GetListTokenAsync()
        {
            var vFiles = await GetVirtualFilesFromCacheAsync();
            return vFiles.Where(x => x.Group == FileType.Token).ToList();
        }

        public async Task<int> SaveFileUploadAsync(VirtualFile file)
        {
            var vFiles = await GetVirtualFilesFromCacheAsync();
            var exist = vFiles.FirstOrDefault(x => x.Id == file.Id);
            if(exist == null)
            {
                _context.VirtualFiles.Add(file);
            }
            else
            {
                exist.OriginalPath = file.OriginalPath;
                exist.VirtualPath = file.VirtualPath;
                exist.Tag = file.Tag;
                exist.Group = file.Group;
                _context.VirtualFiles.Update(exist);
            }
            await SaveAsync();
            return file.Id;
        }
    }
}
