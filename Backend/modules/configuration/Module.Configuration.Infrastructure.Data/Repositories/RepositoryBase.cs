﻿using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        protected DbContext _context;

        public RepositoryBase(DbContext context)
        {
            _context = context;
        }

        public virtual IEnumerable<T> FindAll()
        {
            return _context.Set<T>();
        }

        public virtual IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }

        public virtual void Create(T entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.UpdatedDate = DateTime.UtcNow;
            entity.IsActive = true;
            entity.IsDeleted = false;
            _context.Set<T>().Add(entity);
        }

        public virtual void Update(T entity)
        {
            entity.UpdatedDate = DateTime.UtcNow;
            _context.Set<T>().Update(entity);
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void MarkAsDeleted(T entity)
        {
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTime.UtcNow;
            _context.Set<T>().Update(entity);
        }

        public virtual void Disable(T entity)
        {
            entity.IsActive = false;
            entity.UpdatedDate = DateTime.UtcNow;
            _context.Set<T>().Update(entity);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        public virtual Task<int> SaveAsync()
        {
           return _context.SaveChangesAsync();
        }
    }
}
