﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class LanguageRepo : RepositoryBase<Language>, ILanguageRepo
    {
        private new readonly ConfigurationDbContext _context;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;
        private List<Language> _allLanguages = null;

        private async Task<List<Language>> GetAllLanguagesAsync(bool refresh = false)
        {
            if(_allLanguages == null)
                _allLanguages = await _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_Languages), () =>
                {
                    return _context.Languages
                        .Include(x => x.Localizations)
                        .Where(x => !x.IsDeleted)
                    .ToListAsync();
                }, refresh);

            return _allLanguages;
        }

        public LanguageRepo(
            ConfigurationDbContext context,
            IDistributedCache cache,
            ICacheKeyFormatter cacheKeyFormatter
        ) : base(context)
        {
            _cache = cache;
            _context = context;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        public async Task<List<Language>> GetLanguagesAsync(bool isActive, string culture)
        {
            var allLanguages = await GetAllLanguagesAsync(true);

            var languages = allLanguages.Where(x =>
                !isActive || x.IsActive
            )
            .OrderBy(x => x.Priority)
            .ToList();

            languages = languages.Select(x =>
                LocalizationMapper.MapLanguageLocalization(x, culture)
            )
            .ToList();

            return languages;
        }

        public async Task<Language> GetDefaultLanguageAsync(bool isActive, string culture)
        {
            var allLanguages = await GetAllLanguagesAsync();

            var language = allLanguages
                .Where(x =>
                    !isActive || x.IsActive
                )
                .OrderBy(x => x.Priority)
                .FirstOrDefault();

            return LocalizationMapper.MapLanguageLocalization(language, culture);
        }

        public async Task<Language> GetLanguageByIdAsync(string id, bool isActive, string culture)
        {
            var allLanguages = await GetAllLanguagesAsync();

            var language = allLanguages
                .FirstOrDefault(x =>
                    x.Id.Equals(id, StringComparison.OrdinalIgnoreCase)
                    && (!isActive || x.IsActive)
                );

            return LocalizationMapper.MapLanguageLocalization(language, culture);
        }

        public async Task<bool> UpdateStatusAsync(string languageId, bool status)
        {
            var language = _context.Languages.SingleOrDefault(x => x.Id == languageId);
            if (language == null)
                return false;

            language.IsActive = status;
            _context.Languages.Update(language);
            await _context.SaveChangesAsync();

            //Refresh cache
            await GetAllLanguagesAsync(true);

            return true;
        }

        public async Task<List<Language>> GetLanguagesByScopeAsync(bool isActive, LanguageScope scope, string culture)
        {
            var allLanguages = await GetAllLanguagesAsync();

            var languages = allLanguages.Where(x =>
                !isActive || x.IsActive && x.Scope == scope
            )
            .OrderBy(x => x.Priority)
            .ToList();

            languages = languages.Select(x =>
                LocalizationMapper.MapLanguageLocalization(x, culture)
            )
            .ToList();
            return languages;
        }
    }
}
