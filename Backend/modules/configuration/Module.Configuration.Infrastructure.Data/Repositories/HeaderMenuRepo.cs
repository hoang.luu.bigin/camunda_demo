﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.DTO;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class HeaderMenuRepo : IHeaderMenuRepo
    {
        private readonly ConfigurationDbContext _context;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;
        private List<HeaderMenu> _headerMenus;

        public HeaderMenuRepo(ConfigurationDbContext context, IDistributedCache cache, ICacheKeyFormatter cacheKeyFormatter)
        {
            _context = context;
            _cache = cache;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        // cache first
        public async Task<List<HeaderMenu>> GetHeaderMenusFromCacheAsync(bool refresh = false)
        {
            if(_headerMenus == null)
                _headerMenus = await _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_HeaderMenus), () =>
                {
                    return _context.HeaderMenu
                        .Include(x => x.Localizations)
                        .ToListAsync();
                });
            return _headerMenus;
        }

        public async Task RemoveHeaderMenusFromCacheAsync()
        {
            await _cache.RemoveAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_HeaderMenus));
        }

        // get next
        public async Task<List<HeaderMenu>> GetHeaderMenuGroupsAsync(string culture)
        {
            var headers = await GetHeaderMenusFromCacheAsync(true);

            headers = headers
                    .Where(x => x.IsActive && !x.IsDeleted)
                    .ToList();

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    var local = header.Localizations.SingleOrDefault(x => x.Lang.Equals(culture) && !string.IsNullOrEmpty(x.Name));
                    if (local == null)
                        local = header.Localizations.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name));

                    if (local != null)
                    {
                        header.Name = local.Name;
                        header.Url = local.Url;
                    }
                }

                headers = headers.OrderBy(x => x.Priority).ToList();
            }
            await RemoveHeaderMenusFromCacheAsync();
            return headers;
        }

        public async Task<int> AddOrUpdateAsync(HeaderMenu header)
        {
            if (header.Id == 0)
            {
                var entity = _context.HeaderMenu.Add(header);
                await _context.SaveChangesAsync();

                await RemoveHeaderMenusFromCacheAsync();

                return entity.Entity.Id;
            }
            else
            {
                var localizes = _context.HeaderMenuLocalization.Where(x => x.HeaderMenuId == header.Id);
                _context.HeaderMenuLocalization.RemoveRange(localizes);

                _context.HeaderMenu.Update(header);
                await _context.SaveChangesAsync();

                await RemoveHeaderMenusFromCacheAsync();

                return header.Id;
            }
        }

        public async Task<bool> DeleteHeaderAsync(int id)
        {
            var header = _context.HeaderMenu.SingleOrDefault(x => x.Id == id);
            if (header == null)
                return false;

            header.IsDeleted = true;

            var childs = _context.HeaderMenu.Where(x => x.ParentId.HasValue && x.ParentId.Value == header.Id).ToList();
            if (childs != null)
            {
                foreach (var child in childs)
                    child.IsDeleted = true;

                _context.HeaderMenu.UpdateRange(childs);
            }

            _context.HeaderMenu.Update(header);
            await _context.SaveChangesAsync();

            await RemoveHeaderMenusFromCacheAsync();

            return true;
        }

        public async Task<bool> SortHeaderAsync(List<HeaderSortInput> inputs)
        {
            if (inputs == null)
                return false;

            var idList = inputs.Select(x => x.Id).ToList();

            var headers = _context.HeaderMenu.Where(x => x.IsActive && !x.IsDeleted && idList.Contains(x.Id)).ToList();
            if (headers == null)
                return false;

            foreach (var header in headers)
            {
                var input = inputs.SingleOrDefault(x => x.Id == header.Id);
                if (input != null)
                    header.Priority = input.Priority;
            }

            _context.UpdateRange(headers);
            await _context.SaveChangesAsync();

            await RemoveHeaderMenusFromCacheAsync();

            return true;
        }

       
    }
}
