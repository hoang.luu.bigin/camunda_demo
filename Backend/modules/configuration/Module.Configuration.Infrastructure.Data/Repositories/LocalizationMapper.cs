﻿using Module.Configuration.Domain.Models;
using System;
using System.Linq;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public static class LocalizationMapper
    {
        public static Language MapLanguageLocalization(Language language, string culture)
        {
            if (language == null || language.Localizations == null || !language.Localizations.Any())
                return language;

            var localization = language.Localizations
                .OrderBy(x => x.Lang)
                .FirstOrDefault(x => x.IsActive && !x.IsDeleted);

            if (!string.IsNullOrWhiteSpace(culture))
            {
                var exactLocalization = language.Localizations
                    .FirstOrDefault(x =>
                        x.Lang.Equals(culture, StringComparison.OrdinalIgnoreCase)
                        && x.IsActive && !x.IsDeleted
                    );
                if (exactLocalization != null)
                    localization = exactLocalization;
            }

            if (localization != null)
            {
                language.Name = localization.Name;
                language.Description = localization.Description;
            }

            return language;
        }

    }
}
