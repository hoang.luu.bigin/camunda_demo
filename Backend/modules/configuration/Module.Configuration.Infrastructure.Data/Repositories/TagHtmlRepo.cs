﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudKit.Infrastructure.Data.EfCore.Core;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class TagHtmlRepo : RepositoryBase<TagHtml>, ITagHtmlRepo
    {
        private new readonly ConfigurationDbContext _context;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;
        private List<TagHtml> _htmlTags;

        public TagHtmlRepo(ConfigurationDbContext context, IDistributedCache cache, ICacheKeyFormatter cacheKeyFormatter)
            : base(context)
        {
            _cache = cache;
            _context = context;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        private async Task<List<TagHtml>> GetTagHtmlsFromCacheAsync()
        {
            if (_htmlTags == null)
               _htmlTags = await _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_HtmlTags), () =>
                {
                    return _context.TagHtmls
                    .Where(x=> x.IsDeleted == false)
                    .ToListAsync();
                });
            return _htmlTags;
        }

        public async Task<TagHtml> GetTagHtmlByIdAsync(int id)
        {
            await GetTagHtmlsFromCacheAsync();
            return _htmlTags.FirstOrDefault(x => x.Id == id);
        }

        public async Task<List<TagHtml>> GetTagHtmlsAsync(string culture = "")
        {
            await GetTagHtmlsFromCacheAsync();

            return _htmlTags;
        }

        public async Task<List<TagHtml>> GetActiveTagHtmlsAsync(string culture = "")
        {
            await GetTagHtmlsFromCacheAsync();

            return _htmlTags.Where(x => x.IsActive).ToList();
        }

        public IEnumerable<TagHtml> GetTagsBySearch(string keyword, int start, int length, string orderBy, bool isActiveOnly, string culture, out int total)
        {
            var tags = _context.TagHtmls
                .Where(x =>
                    (string.IsNullOrWhiteSpace(keyword)
                      || x.Name.Contains(keyword)
                      || x.Description.Contains(keyword))
                    && (!isActiveOnly || x.IsActive)
                    && !x.IsDeleted
                );

            total = tags.Count();
            if (total <= 0)
                return new List<TagHtml>();

            if (!string.IsNullOrWhiteSpace(orderBy))
                tags = tags.OrderWithDatatable(orderBy);

            if (start >= 0 && length >= 0)
                tags = tags.Skip(start).Take(length);

            return tags.ToList();
        }

        public async Task<bool> UpdateStatusAsync(int TagHtmlId, bool status)
        {
            var TagHtml = _context.TagHtmls.SingleOrDefault(x => x.Id == TagHtmlId);
            if (TagHtml == null)
                return false;

            TagHtml.IsActive = status;
            _context.TagHtmls.Update(TagHtml);
            await _context.SaveChangesAsync();

            await _cache.RemoveAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_HtmlTags));
            return true;
        }
    }
}
