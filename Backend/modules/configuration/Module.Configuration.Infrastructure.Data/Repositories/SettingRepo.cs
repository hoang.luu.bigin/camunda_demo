﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class SettingRepo : RepositoryBase<Setting>, ISettingRepo
    {
        private readonly ConfigurationDbContext _dbContext;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;
        private List<Setting> _loadSettings = null;

        private async Task<List<Setting>> GetSettingsFromCacheAsync(bool refresh = false)
        {
            if(_loadSettings == null)
                _loadSettings =  await _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_Settings), () =>
                {
                    return _dbContext.Settings
                    .Where(x => x.IsActive == true && x.IsDeleted == false)
                    .ToListAsync();
                }, refresh);
            return _loadSettings;
        }

        public SettingRepo(
            ConfigurationDbContext context, 
            IDistributedCache cache,
            ICacheKeyFormatter cacheKeyFormatter
        ) : base(context)
        {
            _dbContext = context;
            _cache = cache;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        public async Task<List<Setting>> GetSettingsByGroupAsync(string group)
        {
            if (string.IsNullOrEmpty(group)) return new List<Setting>();
            List<Setting> settings = await GetSettingsFromCacheAsync(true);
            return settings.Where(x => x.Group.Equals(group,StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public async Task<Setting> GetSettingByIdAsync(string id)
        {
            List<Setting> settings = await GetSettingsFromCacheAsync();

            return settings.FirstOrDefault(x => x.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
        }

        public void AddOrUpdateSetting(string id, string value,
            string name, string group, string dataType,
            string culture = "", bool isPublic = false)
        {
            if (!string.IsNullOrWhiteSpace(culture))
            {
                id = $"{id}.{culture}";
                name = $"{name}({culture})";
            }

            var setting = _dbContext.Settings
                .FirstOrDefault(x => x.Id == id);

            if (setting != null)
            {
                setting.Value = value;
                setting.Name = name;
                setting.Group = group;
                setting.DataType = dataType;
                setting.IsPublic = isPublic;
                Update(setting);
            }
            else
            {
                setting = new Setting()
                {
                    Id = id,
                    Value = value,
                    Name = name,
                    Group = group,
                    DataType = dataType,
                    IsPublic = isPublic
            };

                Create(setting);
            }

            _cache.Remove(_cacheKeyFormatter.Format(CacheKey.Configuration_Settings));
        }
    }
}
