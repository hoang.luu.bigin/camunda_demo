﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.DTO;
using Module.Configuration.Domain.Models;
using CloudKit.Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Configuration.Infrastructure.Data.Repositories
{
    public class FooterMenuRepo : IFooterMenuRepo
    {
        private readonly ConfigurationDbContext _context;
        private readonly IDistributedCache _cache;
        private readonly ICacheKeyFormatter _cacheKeyFormatter;

        public FooterMenuRepo(ConfigurationDbContext context, IDistributedCache cache, ICacheKeyFormatter cacheKeyFormatter)
        {
            _context = context;
            _cache = cache;
            _cacheKeyFormatter = cacheKeyFormatter;
        }

        // cache first
        public  Task<List<FooterMenu>> GetFooterMenusFromCacheAsync(bool refresh = false)
        {
            return  _cache.GetOrCreateAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_FooterMenus), () =>
            {
                return _context.FooterMenu
                    .Include(x => x.Localizations)
                    .Where(x => x.IsDeleted == false)
                    .ToListAsync();
            }, refresh);
        }

        private async Task RemoveFooterMenusFromCacheAsync()
        {
            await _cache.RemoveAsync(_cacheKeyFormatter.Format(CacheKey.Configuration_FooterMenus));
        }

        // get next
        public async Task<List<FooterMenu>> GetFooterMenuGroupsAsync(string culture)
        {
            var footers = await GetFooterMenusFromCacheAsync();

            footers = footers
                    .Where(x => x.IsActive)
                    .ToList();

            if (footers != null)
            {
                foreach (var footer in footers)
                {
                    var local = footer.Localizations.SingleOrDefault(x => x.Lang.Equals(culture) && !string.IsNullOrEmpty(x.Name));
                    if (local == null)
                        local = footer.Localizations.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name));

                    if (local != null)
                    {
                        footer.Name = local.Name;
                        footer.Url = local.Url;
                    }
                }

                footers = footers.OrderBy(x => x.Priority).ToList();
            }

            return footers;
        }

        public async Task<int> AddOrUpdateAsync(FooterMenu footer)
        {
            if (footer.Id == 0)
            {
                var entity = _context.FooterMenu.Add(footer);
                await _context.SaveChangesAsync();

                await RemoveFooterMenusFromCacheAsync();

                return entity.Entity.Id;
            }
            else
            {
                var localizes = _context.FooterMenuLocalization.Where(x => x.FooterMenuId == footer.Id);
                _context.FooterMenuLocalization.RemoveRange(localizes);

                _context.FooterMenu.Update(footer);
                await _context.SaveChangesAsync();

                await RemoveFooterMenusFromCacheAsync();

                return footer.Id;
            }
        }

        public async Task<bool> DeleteFooterAsync(int id)
        {
            var footer = _context.FooterMenu.SingleOrDefault(x => x.Id == id);
            if (footer == null)
                return false;

            footer.IsDeleted = true;

            var childs = _context.FooterMenu.Where(x => x.ParentId.HasValue && x.ParentId.Value == footer.Id).ToList();
            if (childs != null)
            {
                foreach (var child in childs)
                    child.IsDeleted = true;

                _context.FooterMenu.UpdateRange(childs);
            }

            _context.FooterMenu.Update(footer);
            await _context.SaveChangesAsync();

            await RemoveFooterMenusFromCacheAsync();

            return true;
        }

        public async Task<bool> SortFooterAsync(List<FooterSortInput> inputs)
        {
            if (inputs == null)
                return false;

            var idList = inputs.Select(x => x.Id).ToList();

            var footers = _context.FooterMenu.Where(x => x.IsActive && !x.IsDeleted && idList.Contains(x.Id)).ToList();
            if (footers == null)
                return false;

            foreach (var footer in footers)
            {
                var input = inputs.SingleOrDefault(x => x.Id == footer.Id);
                if (input != null)
                    footer.Priority = input.Priority;
            }

            _context.UpdateRange(footers);
            await _context.SaveChangesAsync();

            await RemoveFooterMenusFromCacheAsync();

            return true;
        }
    }

}
