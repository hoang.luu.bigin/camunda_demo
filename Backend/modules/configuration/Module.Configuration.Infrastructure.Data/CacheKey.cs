﻿namespace Module.Configuration.Infrastructure.Data
{
    public static class CacheKey
    {
        public const string CachePrefix = "";
        public const string Configuration_Languages = CachePrefix + "Conf_Languages";
        public const string Configuration_Countries = CachePrefix + "Conf_Countries";
        public const string Configuration_Provinces = CachePrefix + "Conf_Provinces";
        public const string Configuration_Districts = CachePrefix + "Conf_Districts";
        public const string Configuration_Wards = CachePrefix + "Conf_Wards";
        public const string Configuration_FooterMenus = CachePrefix + "Conf_FooterMenus";
        public const string Configuration_Settings = CachePrefix + "Conf_Settings";
        public const string Configuration_HtmlTags = CachePrefix + "Conf_HtmlTags";
        public const string Configuration_VirtualFiles = CachePrefix + "Conf_VFiles";
        public const string Configuration_HeaderMenus = CachePrefix + "Conf_HeaderMenus";



    }
}
