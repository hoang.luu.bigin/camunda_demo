﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Configuration.Infrastructure.Data.Migrations
{
    public partial class UpdateData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9524), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9525) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9521), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9522) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9452), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9462) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(7080), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(7105) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "Icon", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 974, DateTimeKind.Utc).AddTicks(618), "/media/flat/020-flag.svg", new DateTime(2020, 4, 27, 7, 47, 47, 974, DateTimeKind.Utc).AddTicks(648) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "Icon", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 973, DateTimeKind.Utc).AddTicks(4097), "/media/flat/vn.svg", new DateTime(2020, 4, 27, 7, 47, 47, 973, DateTimeKind.Utc).AddTicks(4994) });

            migrationBuilder.InsertData(
                table: "configuration_settings",
                columns: new[] { "Id", "CreatedDate", "DataType", "Group", "IsActive", "IsDeleted", "Name", "UpdatedDate", "Value" },
                values: new object[,]
                {
                    { "third-party-google-recaptcha-key", new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(1437), null, "ThirdParty", true, false, "Google's Recaptcha Key", new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(1444), "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI" },
                    { "third-party-google-recaptcha-secret", new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(3824), null, "ThirdParty", true, false, "Google's Recaptcha Secret", new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(3832), "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key");

            migrationBuilder.DeleteData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret");

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9058), new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9059) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9052), new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9055) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(8981), new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(8990) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(5781), new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(5813) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "Icon", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 903, DateTimeKind.Utc).AddTicks(7907), "flag-icon-gb", new DateTime(2020, 4, 27, 7, 8, 24, 903, DateTimeKind.Utc).AddTicks(7932) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "Icon", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 8, 24, 902, DateTimeKind.Utc).AddTicks(8340), "flag-icon-vn", new DateTime(2020, 4, 27, 7, 8, 24, 902, DateTimeKind.Utc).AddTicks(9868) });
        }
    }
}
