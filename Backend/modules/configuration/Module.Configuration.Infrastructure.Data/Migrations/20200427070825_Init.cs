﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Configuration.Infrastructure.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "configuration_districts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    ProvinceCode = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_districts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_footer_menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    ContentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_footer_menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_header_menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    ContentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_header_menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_languages",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Icon = table.Column<string>(maxLength: 500, nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    NativeName = table.Column<string>(nullable: true),
                    Scope = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_localizations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EntityId = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: false),
                    LocaleKeyGroup = table.Column<string>(nullable: false),
                    LocaleKey = table.Column<string>(nullable: false),
                    LocaleValue = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_localizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_provinces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_provinces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_settings",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 200, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Group = table.Column<string>(maxLength: 200, nullable: true),
                    DataType = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_tag_htmls",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Position = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_tag_htmls", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_virtual_files",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OriginalPath = table.Column<string>(nullable: false),
                    VirtualPath = table.Column<string>(nullable: false),
                    Group = table.Column<int>(nullable: false),
                    Tag = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_virtual_files", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_wards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    DistrictCode = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_wards", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "configuration_footer_menu_localization",
                columns: table => new
                {
                    FooterMenuId = table.Column<int>(nullable: false),
                    Lang = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 500, nullable: true),
                    Url = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_footer_menu_localization", x => new { x.FooterMenuId, x.Lang });
                    table.ForeignKey(
                        name: "FK_configuration_footer_menu_localization_configuration_footer_~",
                        column: x => x.FooterMenuId,
                        principalTable: "configuration_footer_menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "configuration_header_menu_localization",
                columns: table => new
                {
                    HeaderMenuId = table.Column<int>(nullable: false),
                    Lang = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 500, nullable: true),
                    Url = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_header_menu_localization", x => new { x.HeaderMenuId, x.Lang });
                    table.ForeignKey(
                        name: "FK_configuration_header_menu_localization_configuration_header_~",
                        column: x => x.HeaderMenuId,
                        principalTable: "configuration_header_menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "configuration_language_localizations",
                columns: table => new
                {
                    LanguageId = table.Column<string>(maxLength: 50, nullable: false),
                    Lang = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 500, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_language_localizations", x => new { x.LanguageId, x.Lang });
                    table.ForeignKey(
                        name: "FK_configuration_language_localizations_configuration_languages~",
                        column: x => x.LanguageId,
                        principalTable: "configuration_languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "configuration_languages",
                columns: new[] { "Id", "Code", "CreatedDate", "Icon", "IsActive", "IsDeleted", "NativeName", "Priority", "Scope", "UpdatedDate" },
                values: new object[] { "vi-VN", "vi", new DateTime(2020, 4, 27, 7, 8, 24, 902, DateTimeKind.Utc).AddTicks(8340), "flag-icon-vn", true, false, "Tiếng Việt", 0, 0, new DateTime(2020, 4, 27, 7, 8, 24, 902, DateTimeKind.Utc).AddTicks(9868) });

            migrationBuilder.InsertData(
                table: "configuration_languages",
                columns: new[] { "Id", "Code", "CreatedDate", "Icon", "IsActive", "IsDeleted", "NativeName", "Priority", "Scope", "UpdatedDate" },
                values: new object[] { "en-US", "en", new DateTime(2020, 4, 27, 7, 8, 24, 903, DateTimeKind.Utc).AddTicks(7907), "flag-icon-gb", true, false, "English (United States)", 0, 0, new DateTime(2020, 4, 27, 7, 8, 24, 903, DateTimeKind.Utc).AddTicks(7932) });

            migrationBuilder.InsertData(
                table: "configuration_language_localizations",
                columns: new[] { "LanguageId", "Lang", "CreatedDate", "Description", "IsActive", "IsDeleted", "Name", "UpdatedDate" },
                values: new object[,]
                {
                    { "vi-VN", "vi-VN", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(5781), null, true, false, "Tiếng Việt", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(5813) },
                    { "vi-VN", "en-US", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(8981), null, true, false, "Vietnamese", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(8990) },
                    { "en-US", "vi-VN", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9052), null, true, false, "Tiếng Anh", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9055) },
                    { "en-US", "en-US", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9058), null, true, false, "English", new DateTime(2020, 4, 27, 7, 8, 24, 906, DateTimeKind.Utc).AddTicks(9059) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "configuration_districts");

            migrationBuilder.DropTable(
                name: "configuration_footer_menu_localization");

            migrationBuilder.DropTable(
                name: "configuration_header_menu_localization");

            migrationBuilder.DropTable(
                name: "configuration_language_localizations");

            migrationBuilder.DropTable(
                name: "configuration_localizations");

            migrationBuilder.DropTable(
                name: "configuration_provinces");

            migrationBuilder.DropTable(
                name: "configuration_settings");

            migrationBuilder.DropTable(
                name: "configuration_tag_htmls");

            migrationBuilder.DropTable(
                name: "configuration_virtual_files");

            migrationBuilder.DropTable(
                name: "configuration_wards");

            migrationBuilder.DropTable(
                name: "configuration_footer_menu");

            migrationBuilder.DropTable(
                name: "configuration_header_menu");

            migrationBuilder.DropTable(
                name: "configuration_languages");
        }
    }
}
