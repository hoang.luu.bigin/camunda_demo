﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Configuration.Infrastructure.Data.Migrations
{
    public partial class ConfigurationDbContext_AddLanguageCodeTableAndSeedDataLanguage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "configuration_language_codes",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_language_codes", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "configuration_language_codes",
                columns: new[] { "Id", "CreatedDate", "IsActive", "IsDeleted", "Name", "UpdatedDate" },
                values: new object[,]
                {
                    { "af", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(3580), true, false, "Afrikaans", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(4420) },
                    { "mr-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Marathi (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "ms", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Malay", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "ms-BN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Malay (Brunei Darussalam)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "ms-MY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Malay (Malaysia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "mt", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Maltese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "mt-MT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Maltese (Malta)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "nb", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250), true, false, "Norwegian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7250) },
                    { "nb-NO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Norwegian (Norway)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "nl", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Dutch", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "nl-BE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Dutch (Belgium)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "nl-NL", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Dutch (Netherlands)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "mr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "Marathi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "nn-NO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Norwegian (Nynorsk) (Norway)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "ns-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Northern Sotho (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "pa", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Punjabi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "pa-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Punjabi (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "pl", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Polish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "pl-PL", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Polish (Poland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "ps", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Pashto", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "ps-AR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Pashto (Afghanistan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "pt", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270), true, false, "Portuguese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7270) },
                    { "pt-BR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Portuguese (Brazil)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "pt-PT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Portuguese (Portugal)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "qu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Quechua", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "ns", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260), true, false, "Northern Sotho", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7260) },
                    { "qu-BO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Quechua (Bolivia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "mn-MN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "Mongolian (Mongolia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "mk-MK", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "FYRO Macedonian (Former Yugoslav Republic of Macedonia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "is-IS", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Icelandic (Iceland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "it-CH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Italian (Switzerland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "it-IT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Italian (Italy)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "ja", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Japanese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "ja-JP", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Japanese (Japan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "ka", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Georgian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "ka-GE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Georgian (Georgia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "kk", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210), true, false, "Kazakh", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7210) },
                    { "kk-KZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Kazakh (Kazakhstan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "kn", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Kannada", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "kn-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Kannada (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "mn", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "Mongolian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "ko", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Korean", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "kok", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Konkani", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "kok-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Konkani (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "ky", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Kyrgyz", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "ky-KG", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Kyrgyz (Kyrgyzstan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "lt", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Lithuanian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "lt-LT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Lithuanian (Lithuania)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "lv", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Latvian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "lv-LV", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Latvian (Latvia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230) },
                    { "mi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7230), true, false, "Maori", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "mi-NZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "Maori (New Zealand)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "mk", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240), true, false, "FYRO Macedonian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7240) },
                    { "ko-KR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220), true, false, "Korean (Korea)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7220) },
                    { "is", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Icelandic", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "qu-EC", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Quechua (Ecuador)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "ro", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Romanian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "tn", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Tswana", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "tn-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Tswana (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "tr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Turkish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "tr-TR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Turkish (Turkey)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "tt", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Tatar", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "tt-RU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400), true, false, "Tatar (Russia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400) },
                    { "ts", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400), true, false, "Tsonga", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400) },
                    { "uk", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400), true, false, "Ukrainian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7400) },
                    { "uk-UA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Ukrainian (Ukraine)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "ur", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Urdu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "ur-PK", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Urdu (Islamic Republic of Pakistan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "tl-PH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330), true, false, "Tagalog (Philippines)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7330) },
                    { "uz", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Uzbek (Latin)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "vi-VN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Vietnamese (Viet Nam)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "xh", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Xhosa", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "xh-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Xhosa (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh-CN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese (S)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh-HK", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese (Hong Kong)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh-MO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese (Macau)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh-SG", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese (Singapore)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420) },
                    { "zh-TW", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7420), true, false, "Chinese (T)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7430) },
                    { "zu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7430), true, false, "Zulu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7430) },
                    { "zu-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7430), true, false, "Zulu (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7430) },
                    { "vi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410), true, false, "Vietnamese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7410) },
                    { "qu-PE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280), true, false, "Quechua (Peru)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7280) },
                    { "tl", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Tagalog", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "th", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Thai", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "ro-RO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Romanian (Romania)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "ru", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Russian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "ru-RU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Russian (Russia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "sa", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Sanskrit", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "sa-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Sanskrit (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "se", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290), true, false, "Sami (Northern)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7290) },
                    { "sk", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Slovak", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "sk-SK", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Slovak (Slovakia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "sl", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Slovenian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "sl-SI", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Slovenian (Slovenia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "sq", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Albanian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "th-TH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Thai (Thailand)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "sq-AL", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Albanian (Albania)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "sv", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Swedish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "sv-FI", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Swedish (Finland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "sv-SE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Swedish (Sweden)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "sw", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Swahili", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "sw-KE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Swahili (Kenya)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "syr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Syriac", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "syr-SY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310), true, false, "Syriac (Syria)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7310) },
                    { "ta", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Tamil", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "ta-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Tamil (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "te", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Telugu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "te-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320), true, false, "Telugu (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7320) },
                    { "sr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300), true, false, "Serbian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7300) },
                    { "id-ID", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Indonesian (Indonesia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "it", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Italian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "hy-AM", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Armenian (Armenia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "cy", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Welsh", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "cy-GB", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Welsh (United Kingdom)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "da", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "Danish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "da-DK", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "Danish (Denmark)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "de", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "German", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "de-AT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "German (Austria)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "de-CH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "German (Switzerland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "de-DE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000), true, false, "German (Germany)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7000) },
                    { "de-LI", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "German (Liechtenstein)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "de-LU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "German (Luxembourg)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "dv", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "Divehi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "cs-CZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Czech (Czech Republic)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "dv-MV", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "Divehi (Maldives)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "el-GR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "Greek (Greece)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "en", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "English", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "en-AU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Australia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-BZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Belize)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-CA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Canada)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-CB", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Caribbean)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-GB", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (United Kingdom)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-IE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Ireland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-JM", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020), true, false, "English (Jamaica)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7020) },
                    { "en-NZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (New Zealand)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "id", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200), true, false, "Indonesian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7200) },
                    { "el", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010), true, false, "Greek", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7010) },
                    { "en-TT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (Trinidad and Tobago)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "cs", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Czech", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "ca", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Catalan", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "af-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6890), true, false, "Afrikaans (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6900) },
                    { "ar", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6950), true, false, "Arabic", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6950) },
                    { "ar-AE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6950), true, false, "Arabic (U.A.E.)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6950) },
                    { "ar-BH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Bahrain)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-DZ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Algeria)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-EG", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Egypt)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-IQ", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Iraq)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-JO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Jordan)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-KW", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960), true, false, "Arabic (Kuwait)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6960) },
                    { "ar-LB", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Lebanon)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ar-LY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Libya)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ca-ES", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Catalan (Spain)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "ar-MA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Morocco)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ar-QA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Qatar)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ar-SA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Saudi Arabia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ar-SY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Syria)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "ar-TN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Arabic (Tunisia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "ar-YE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Arabic (Yemen)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "az", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Azeri (Latin)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "be", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Belarusian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "be-BY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Belarusian (Belarus)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "bg", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Bulgarian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "bg-BG", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980), true, false, "Bulgarian (Bulgaria)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6980) },
                    { "bs-BA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990), true, false, "Bosnian (Bosnia and Herzegovina)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6990) },
                    { "ar-OM", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970), true, false, "Arabic (Oman)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(6970) },
                    { "en-US", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (United States)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "en-PH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (Republic of the Philippines)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "en-ZW", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (Zimbabwe)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "fi-FI", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080), true, false, "Finnish (Finland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080) },
                    { "en-ZA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030), true, false, "English (South Africa)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7030) },
                    { "fo-FO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080), true, false, "Faroese (Faroe Islands)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080) },
                    { "fr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080), true, false, "French", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080) },
                    { "fr-BE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080), true, false, "French (Belgium)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080) },
                    { "fr-CA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "French (Canada)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170) },
                    { "fr-CH", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "French (Switzerland)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170) },
                    { "fr-FR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "French (France)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170) },
                    { "fr-LU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "French (Luxembourg)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170) },
                    { "fr-MC", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "French (Principality of Monaco)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170) },
                    { "gl", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7170), true, false, "Galician", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "fi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Finnish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "gl-ES", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Galician (Spain)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "gu-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Gujarati (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "he", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Hebrew", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "he-IL", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Hebrew (Israel)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "hi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Hindi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "hi-IN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Hindi (India)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hr", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Croatian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hr-BA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Croatian (Bosnia and Herzegovina)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hr-HR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Croatian (Croatia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Hungarian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hu-HU", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Hungarian (Hungary)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "hy", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190), true, false, "Armenian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7190) },
                    { "gu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180), true, false, "Gujarati", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7180) },
                    { "fa-IR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Farsi (Iran)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "fo", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080), true, false, "Faroese", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7080) },
                    { "eu-ES", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Basque (Spain)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "eo", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Esperanto", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-AR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish (Argentina)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-BO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish (Bolivia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-CL", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish (Chile)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-CO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish (Colombia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-CR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040), true, false, "Spanish (Costa Rica)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7040) },
                    { "es-DO", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Dominican Republic)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "fa", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Farsi", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "es-ES", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Castilian)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "es-GT", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Guatemala)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "es-HN", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Honduras)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "es-EC", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Ecuador)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "es-NI", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Nicaragua)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "eu", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Basque", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "es-MX", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050), true, false, "Spanish (Mexico)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7050) },
                    { "et", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Estonian", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "es-VE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Venezuela)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "es-UY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Uruguay)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "et-EE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070), true, false, "Estonian (Estonia)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7070) },
                    { "es-PY", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Paraguay)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "es-PR", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Puerto Rico)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "es-PE", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Peru)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "es-PA", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (Panama)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) },
                    { "es-SV", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060), true, false, "Spanish (El Salvador)", new DateTime(2020, 11, 24, 10, 4, 10, 24, DateTimeKind.Utc).AddTicks(7060) }
                });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1570), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1570) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1560), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1560) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1530), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(1530) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(110), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(110) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 39, DateTimeKind.Utc).AddTicks(8270), new DateTime(2020, 11, 24, 10, 4, 10, 39, DateTimeKind.Utc).AddTicks(8270) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 39, DateTimeKind.Utc).AddTicks(5350), new DateTime(2020, 11, 24, 10, 4, 10, 39, DateTimeKind.Utc).AddTicks(5350) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(3870), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(3870) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(6170), new DateTime(2020, 11, 24, 10, 4, 10, 40, DateTimeKind.Utc).AddTicks(6170) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "configuration_language_codes");

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2009), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2010) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2006), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2007) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(1969), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(1972) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(899), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(925) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(6366), new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(6379) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(2720), new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(3375) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(3087), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(3092) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(4403), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(4407) });
        }
    }
}
