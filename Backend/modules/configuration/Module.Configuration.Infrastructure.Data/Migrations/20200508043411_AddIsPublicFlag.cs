﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Configuration.Infrastructure.Data.Migrations
{
    public partial class AddIsPublicFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPublic",
                table: "configuration_settings",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6262), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6264) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6257), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6259) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6187), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6195) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(3201), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(3228) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "Priority", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(9377), 2, new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(9414) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "Priority", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 710, DateTimeKind.Utc).AddTicks(9276), 1, new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(332) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "DataType", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(8985), "string", new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(8998) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "DataType", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 715, DateTimeKind.Utc).AddTicks(3631), "string", new DateTime(2020, 5, 8, 4, 34, 9, 715, DateTimeKind.Utc).AddTicks(3644) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPublic",
                table: "configuration_settings");

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9524), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9525) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9521), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9522) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9452), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(9462) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(7080), new DateTime(2020, 4, 27, 7, 47, 47, 975, DateTimeKind.Utc).AddTicks(7105) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "Priority", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 974, DateTimeKind.Utc).AddTicks(618), 0, new DateTime(2020, 4, 27, 7, 47, 47, 974, DateTimeKind.Utc).AddTicks(648) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "Priority", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 973, DateTimeKind.Utc).AddTicks(4097), 0, new DateTime(2020, 4, 27, 7, 47, 47, 973, DateTimeKind.Utc).AddTicks(4994) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "DataType", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(1437), null, new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(1444) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "DataType", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(3824), null, new DateTime(2020, 4, 27, 7, 47, 47, 976, DateTimeKind.Utc).AddTicks(3832) });
        }
    }
}
