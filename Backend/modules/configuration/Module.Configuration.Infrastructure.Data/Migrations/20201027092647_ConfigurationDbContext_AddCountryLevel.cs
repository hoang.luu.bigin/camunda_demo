﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Module.Configuration.Infrastructure.Data.Migrations
{
    public partial class ConfigurationDbContext_AddCountryLevel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "configuration_localizations");

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                table: "configuration_provinces",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "configuration_countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_countries", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2009), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2010) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2006), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(2007) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(1969), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(1972) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(899), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(925) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(6366), new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(6379) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(2720), new DateTime(2020, 10, 27, 9, 26, 47, 67, DateTimeKind.Utc).AddTicks(3375) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(3087), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(3092) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(4403), new DateTime(2020, 10, 27, 9, 26, 47, 69, DateTimeKind.Utc).AddTicks(4407) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "configuration_countries");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                table: "configuration_provinces");

            migrationBuilder.CreateTable(
                name: "configuration_localizations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EntityId = table.Column<Guid>(type: "char(36)", nullable: false),
                    LanguageCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    LocaleKey = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    LocaleKeyGroup = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    LocaleValue = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_configuration_localizations", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6262), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6264) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "en-US", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6257), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6259) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "en-US" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6187), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(6195) });

            migrationBuilder.UpdateData(
                table: "configuration_language_localizations",
                keyColumns: new[] { "LanguageId", "Lang" },
                keyValues: new object[] { "vi-VN", "vi-VN" },
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(3201), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(3228) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "en-US",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(9377), new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(9414) });

            migrationBuilder.UpdateData(
                table: "configuration_languages",
                keyColumn: "Id",
                keyValue: "vi-VN",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 710, DateTimeKind.Utc).AddTicks(9276), new DateTime(2020, 5, 8, 4, 34, 9, 711, DateTimeKind.Utc).AddTicks(332) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-key",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(8985), new DateTime(2020, 5, 8, 4, 34, 9, 714, DateTimeKind.Utc).AddTicks(8998) });

            migrationBuilder.UpdateData(
                table: "configuration_settings",
                keyColumn: "Id",
                keyValue: "third-party-google-recaptcha-secret",
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2020, 5, 8, 4, 34, 9, 715, DateTimeKind.Utc).AddTicks(3631), new DateTime(2020, 5, 8, 4, 34, 9, 715, DateTimeKind.Utc).AddTicks(3644) });
        }
    }
}
