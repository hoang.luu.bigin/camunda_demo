﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Configuration.Domain.Locations;

namespace Module.Configuration.Infrastructure.EntityConfigurations
{
    internal sealed class WardEntityTypeConfiguration : IEntityTypeConfiguration<LocationLevel3>
    {
        public void Configure(EntityTypeBuilder<LocationLevel3> builder)
        {
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
        }
    }
}
