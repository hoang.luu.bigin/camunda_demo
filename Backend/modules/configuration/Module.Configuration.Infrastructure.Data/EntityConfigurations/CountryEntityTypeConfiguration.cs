﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Configuration.Domain.Locations;

namespace Module.Configuration.Infrastructure.EntityConfigurations
{
    internal sealed class CountryEntityTypeConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
        }
    }
}
