﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Configuration.Domain.Locations;

namespace Module.Configuration.Infrastructure.EntityConfigurations
{
    internal sealed class ProvinceEntityTypeConfiguration : IEntityTypeConfiguration<LocationLevel1>
    {
        public void Configure(EntityTypeBuilder<LocationLevel1> builder)
        {
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);

        }
    }
}
