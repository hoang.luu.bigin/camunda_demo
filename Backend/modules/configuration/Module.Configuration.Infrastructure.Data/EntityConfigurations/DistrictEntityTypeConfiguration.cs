﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Configuration.Domain.Locations;

namespace Module.Configuration.Infrastructure.EntityConfigurations
{
    internal sealed class DistrictEntityTypeConfiguration : IEntityTypeConfiguration<LocationLevel2>
    {
        public void Configure(EntityTypeBuilder<LocationLevel2> builder)
        {
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);

        }
    }
}
