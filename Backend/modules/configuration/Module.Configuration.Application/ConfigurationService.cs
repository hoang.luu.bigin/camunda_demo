﻿using Module.Configuration.Domain.Constracts;
using Module.Configuration.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module.Configuration.Application
{
    public class ConfigurationService : IConfigurationService
    {
    
        private readonly ILanguageRepo _languageRepo;
        private readonly ISettingRepo _settingRepo;

        public ConfigurationService(
        
            ILanguageRepo languageRepo,
            ISettingRepo settingRepo
        )
        {
            _languageRepo = languageRepo;
            _settingRepo = settingRepo;
        }

        public async Task<List<Language>> GetLanguagesAsync(bool isActive, string culture)
        {
            return await _languageRepo.GetLanguagesAsync(isActive, culture);
        }

        public async Task<Language> GetLanguageByIdAsync(string id, bool isActive, string culture)
        {
            return await _languageRepo.GetLanguageByIdAsync(id, isActive, culture);
        }

        public async Task<Language> GetDefaultLanguageAsync(bool isActive, string culture)
        {
            return await _languageRepo.GetDefaultLanguageAsync(isActive, culture);
        }

        public async Task<List<Setting>> GetSettingsByGroupAsync(string group)
        {
            return await _settingRepo.GetSettingsByGroupAsync(group);
        }

        public async Task<Setting> GetSettingByIdAsync(string id)
        {
            return await _settingRepo.GetSettingByIdAsync(id);
        }

      
    }
}
