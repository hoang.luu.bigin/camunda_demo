﻿using _Shared.Application;
using FluentValidation;
using Google.Maps.Places;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Application.Models;
using Module.Configuration.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Configuration.Application.Queries.Settings
{
    public class GetAllPublicSettings : QueryRequestBase, IRequest<IEnumerable<SettingRM>>
    {
    }

    public class GetAllPublicSettingsValidator : AbstractValidator<GetAllPublicSettings>
    {
    }

    public class GetAllPublicSettingsHandler : IRequestHandler<GetAllPublicSettings, IEnumerable<SettingRM>>
    {
        private readonly ConfigurationDbContext _dbContext;
        public GetAllPublicSettingsHandler(ConfigurationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<SettingRM>> Handle(GetAllPublicSettings request, CancellationToken cancellationToken)
        {
            var settings = (await _dbContext.Settings
                 .Where(x => x.IsPublic && (!x.Id.Contains(".") || EF.Functions.Like(x.Id,$"%.{request.Culture}")))
                 .ToListAsync())
                 .Adapt<List<SettingRM>>();
            return settings;
        }
    }
}
