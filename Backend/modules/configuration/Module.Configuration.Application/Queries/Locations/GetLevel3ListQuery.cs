﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.Data;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Locations
{
    public class GetLevel3ListQuery : IRequest<IEnumerable<LocationLevel3>>
    {
        public string? Level1Code { get; set; }
        public string? Level2Code { get; set; }
    }

    public class GetWardListQueryValidator : AbstractValidator<GetLevel3ListQuery>
    {
    }

    public class GetWardListQueryHandler : IRequestHandler<GetLevel3ListQuery, IEnumerable<LocationLevel3>>
    {
        private readonly MongoDbConfigurationDbContext _configurationDbContext;
        public GetWardListQueryHandler(MongoDbConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }
        public async Task<IEnumerable<LocationLevel3>> Handle(GetLevel3ListQuery request, CancellationToken cancellationToken)
        {
            return await _configurationDbContext.LocationLevel3s
                .Find(x =>
                    (string.IsNullOrEmpty(request.Level1Code) || x.Level1Code == request.Level1Code)
                    &&
                    (string.IsNullOrEmpty(request.Level2Code) || x.Level2Code == request.Level2Code)
                )
                .SortBy(x => x.Name)
                .ToListAsync();
        }
    }
}
