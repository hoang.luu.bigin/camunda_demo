﻿using _Shared.Application;
using FluentValidation;
using Google.Maps.Places;
using MediatR;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Configuration.Application.Queries.Locations
{
    public class GetAutoCompleteQuery : QueryRequestBase, IRequest<AutocompleteResponse>
    {
        public string Address { get; set; }
    }

    public class GetLocationQueryValidator : AbstractValidator<GetAutoCompleteQuery>
    {
        public GetLocationQueryValidator()
        {
            RuleFor(x => x.Address).MinimumLength(3);
        }
    }

    public class GetLocationQueryHandler : IRequestHandler<GetAutoCompleteQuery, AutocompleteResponse>
    {
        private readonly IConfiguration _configuration;
        public GetLocationQueryHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Task<AutocompleteResponse> Handle(GetAutoCompleteQuery request, CancellationToken cancellationToken)
        {
            AutocompleteRequest acRequest = new AutocompleteRequest()
            {
                Input = request.Address,
                Language = request.Culture,
                Types = new PlaceType[1] {PlaceType.Geocode},
                Components = "vn"
            };
            return new PlacesService(new Google.Maps.GoogleSigned(_configuration["GoogleCloudKey"])).GetAutocompleteResponseAsync(acRequest);
        }
    }
}
