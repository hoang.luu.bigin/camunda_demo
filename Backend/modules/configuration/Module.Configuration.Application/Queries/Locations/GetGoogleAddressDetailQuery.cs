﻿using _Shared.Application;
using FluentValidation;
using Google.Maps;
using Google.Maps.Places.Details;
using MediatR;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Configuration.Application.Queries.Locations
{
    public class GetGoogleAddressDetailQuery : QueryRequestBase, IRequest<PlaceDetailsResponse>
    {
        public string PlaceId { get; set; }
    }

    public class GetGoogleAddressDetailQueryValidator : AbstractValidator<GetGoogleAddressDetailQuery>
    {
        public GetGoogleAddressDetailQueryValidator()
        {
            RuleFor(x => x.PlaceId).MaximumLength(512);
        }
    }

    public class GetGoogleAddressDetailQueryHandler : IRequestHandler<GetGoogleAddressDetailQuery, PlaceDetailsResponse>
    {
        private readonly IConfiguration _configuration;
        public GetGoogleAddressDetailQueryHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Task<PlaceDetailsResponse> Handle(GetGoogleAddressDetailQuery request, CancellationToken cancellationToken)
        {
            var placeDetailRequest = new PlaceDetailsRequest()
            {
                PlaceID = request.PlaceId,
                Language = request.Culture
            };
            return new PlaceDetailsService(new GoogleSigned(_configuration["GoogleCloudKey"])).GetResponseAsync(placeDetailRequest);
        }
    }
}
