﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.Data;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Configuration.Application.Queries.Locations
{
    public class GetCountryListQuery : IRequest<IEnumerable<Country>>
    {

    }

    public class GetCountryListQueryValidator : AbstractValidator<GetCountryListQuery>
    {

    }

    public class GetCountryListQueryHandler : IRequestHandler<GetCountryListQuery, IEnumerable<Country>>
    {
        private readonly MongoDbConfigurationDbContext _configurationDbContext;
        public GetCountryListQueryHandler(MongoDbConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }
        public async Task<IEnumerable<Country>> Handle(GetCountryListQuery request, CancellationToken cancellationToken)
        {
            return await _configurationDbContext.Countries
               .Find(x => x.IsDeleted == false)
               .SortBy(x => x.Name)
               .ToListAsync();
        }
    }
}
