﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.Data;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Locations
{
    public class GetLevel1ListQuery : IRequest<IEnumerable<LocationLevel1>>
    {
        public string? CountryCode { get; set; }
    }
    public class GetProvinceListQueryValidator : AbstractValidator<GetLevel1ListQuery>
    {

    }

    public class GetProvinceListQueryHandler : IRequestHandler<GetLevel1ListQuery, IEnumerable<LocationLevel1>>
    {
        private readonly MongoDbConfigurationDbContext _configurationDbContext;
        public GetProvinceListQueryHandler(MongoDbConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }
        public async Task<IEnumerable<LocationLevel1>> Handle(GetLevel1ListQuery request, CancellationToken cancellationToken)
        {
            return await _configurationDbContext.LocationLevel1s
                .Find(
                     x => string.IsNullOrEmpty(request.CountryCode) || x.CountryCode == request.CountryCode
                )
                .SortBy(x => x.Name)
                .ToListAsync();
        }
    }
}
