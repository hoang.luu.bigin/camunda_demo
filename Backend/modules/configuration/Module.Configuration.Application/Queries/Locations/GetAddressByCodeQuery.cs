﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Locations
{
    public class GetAddressByCodeQuery :  IRequest<GetAddressByCodeResponse>
    {
        public string? CountryCode { get; set; }
        public string? Level1Code { get; set; }
        public string? Level2Code { get; set; }
        public string? Level3Code { get; set; }
    }


    public class GetAddressByCodeResponse
    {
        public Country? Country { get; set; }
        public LocationLevel1? Level1Response { get; set; }
        public LocationLevel2? Level2Response { get; set; }
        public LocationLevel3? Level3Response { get; set; }
    }

    public class GetAddressByCodeQueryValidator : AbstractValidator<GetAddressByCodeQuery>
    {
        public GetAddressByCodeQueryValidator()
        {
        }
    }

    public class GetAddressByCodeQueryHandler : IRequestHandler<GetAddressByCodeQuery, GetAddressByCodeResponse>
    {
        private readonly MongoDbConfigurationDbContext _configurationDbContext;
        public GetAddressByCodeQueryHandler(MongoDbConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }
        public async Task<GetAddressByCodeResponse> Handle(GetAddressByCodeQuery request, CancellationToken cancellationToken)
        {
            GetAddressByCodeResponse getAddressByCodeResponse = new GetAddressByCodeResponse();
            if (!string.IsNullOrEmpty(request.CountryCode))
                getAddressByCodeResponse.Country = await _configurationDbContext.Countries.FirstOrDefaultExAsync(x => x.Code == request.CountryCode);

            if (!string.IsNullOrEmpty(request.Level1Code))
                getAddressByCodeResponse.Level1Response = await _configurationDbContext.LocationLevel1s.FirstOrDefaultExAsync(x => x.Code == request.Level1Code);

            if (!string.IsNullOrEmpty(request.Level2Code))
                getAddressByCodeResponse.Level2Response = await _configurationDbContext.LocationLevel2s.FirstOrDefaultExAsync(x => x.Code == request.Level2Code);

            if (!string.IsNullOrEmpty(request.Level3Code))
                getAddressByCodeResponse.Level3Response = await _configurationDbContext.LocationLevel3s.FirstOrDefaultExAsync(x => x.Code == request.Level3Code);

            return getAddressByCodeResponse;
        }
    }
}
