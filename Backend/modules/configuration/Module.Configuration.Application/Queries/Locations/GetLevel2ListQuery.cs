﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Locations;
using Module.Configuration.Infrastructure.Data;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Locations
{
    public class GetLevel2ListQuery : IRequest<IEnumerable<LocationLevel2>>
    {
        public string? Level1Code { get; set; }
    }

    public class GetDistrictListQueryValidator : AbstractValidator<GetLevel2ListQuery>
    {
    }

    public class GetDistrictListQueryHandler : IRequestHandler<GetLevel2ListQuery, IEnumerable<LocationLevel2>>
    {
        private readonly MongoDbConfigurationDbContext _configurationDbContext;
        public GetDistrictListQueryHandler(MongoDbConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }
        public async Task<IEnumerable<LocationLevel2>> Handle(GetLevel2ListQuery request, CancellationToken cancellationToken)
        {
            return await _configurationDbContext.LocationLevel2s
                .Find(
                     x => string.IsNullOrEmpty(request.Level1Code) || x.Level1Code == request.Level1Code
                )
                .SortBy(x => x.Name)
                .ToListAsync();
        }
    }
}
