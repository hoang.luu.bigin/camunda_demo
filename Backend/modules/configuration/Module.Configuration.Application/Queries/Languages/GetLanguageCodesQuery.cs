﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using _Shared.Application;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Application.Models;
using Module.Configuration.Infrastructure.Data;

namespace Configuration.Application.Queries.Languages
{
    public class GetLanguageCodesQuery : AuthRequestBase<List<LanguageCodeRM>>
    {
        
    }

    public class GetLanguageCodesQueryValidator : AbstractValidator<GetLanguageCodesQuery>
    {

    }

    public class GetLanguageCodesQueryHandler : IRequestHandler<GetLanguageCodesQuery, List<LanguageCodeRM>>
    {
        private readonly ConfigurationDbContext _configurationDbContext;
        public GetLanguageCodesQueryHandler(ConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }

        public async Task<List<LanguageCodeRM>> Handle(GetLanguageCodesQuery request, CancellationToken cancellationToken)
        {
            return (await _configurationDbContext.LanguageCodes
                .AsNoTracking()
                .ToListAsync()
                ).Adapt<List<LanguageCodeRM>>();
        }
    }
}
