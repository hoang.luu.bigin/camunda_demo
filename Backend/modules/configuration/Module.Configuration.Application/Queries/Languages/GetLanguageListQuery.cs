﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Models;
using Module.Configuration.Infrastructure.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Languages
{
    public class GetLanguageListQuery : IRequest<IEnumerable<Language>>
    {
        
    }

    public class GetLanguageListQueryValidator : AbstractValidator<GetLanguageListQuery>
    {
        public GetLanguageListQueryValidator()
        {
        }
    }
    public class GetLanguageListQueryHandler : IRequestHandler<GetLanguageListQuery, IEnumerable<Language>>
    {
        private readonly ConfigurationDbContext _configurationDbContext;
        public GetLanguageListQueryHandler(ConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }

        public async Task<IEnumerable<Language>> Handle(GetLanguageListQuery request, CancellationToken cancellationToken)
        {
            return await _configurationDbContext.Languages.AsNoTracking()
                .ToListAsync();
        }
    }
}
