﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Module.Configuration.Domain.Models;
using Module.Configuration.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Configuration.Application.Queries.Languages
{
    public class GetLanguageByCodeQuery : IRequest<Language>
    {
        public string? LangCode { get; set; }
    }

    public class GetLanguageByCodeQueryValidator : AbstractValidator<GetLanguageByCodeQuery>
    {
        public GetLanguageByCodeQueryValidator()
        {
        }
    }
    public class GetLanguageByCodeQueryHandler : IRequestHandler<GetLanguageByCodeQuery, Language>
    {
        private readonly ConfigurationDbContext _configurationDbContext;
        public GetLanguageByCodeQueryHandler(ConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }

        public Task<Language> Handle(GetLanguageByCodeQuery request, CancellationToken cancellationToken)
        {
            return _configurationDbContext.Languages.AsNoTracking().FirstOrDefaultAsync(x => x.Code == request.LangCode);
        }
    }
}
