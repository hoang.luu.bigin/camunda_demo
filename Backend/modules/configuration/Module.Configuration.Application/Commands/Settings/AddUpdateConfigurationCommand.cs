﻿using _Shared.Application;
using FluentValidation;
using MediatR;
using Module.Configuration.Domain.Constracts;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Configuration.Application.Commands.Settings
{
    public class AddUpdateConfigurationCommand : AuthRequestBase<bool>
    {
        public List<SettingInput> Settings { get; set; } = new List<SettingInput>();
    }
    public class SettingInput 
    {
        public string Id { get; set; }
        public string Value { get; set; }

        public string? Name { get; set; }
        public string? Group { get; set; }
        public string? DataType { get; set; }
        public bool IsPublic { get; set; } = true;
    }


    public class AddUpdateConfigurationCommandValidator
        : AbstractValidator<AddUpdateConfigurationCommand>
    {
        public AddUpdateConfigurationCommandValidator()
        {
        }
    }

    public class AddUpdateConfigurationCommandHandler
        : IRequestHandler<AddUpdateConfigurationCommand, bool>
    {
        private readonly ISettingRepo _settingRepo;

        public AddUpdateConfigurationCommandHandler(ISettingRepo settingRepo)
        {
            _settingRepo = settingRepo;
        }

        public async Task<bool> Handle(AddUpdateConfigurationCommand command, CancellationToken cancellationToken)
        {
            command.Settings.ForEach(setting =>
            {
                _settingRepo.AddOrUpdateSetting(
                        setting.Id, setting.Value, setting.Name,
                        setting.Group, setting.DataType, "", setting.IsPublic);
            });
            
            await _settingRepo.SaveAsync();

            return true;
        }
    }
}
