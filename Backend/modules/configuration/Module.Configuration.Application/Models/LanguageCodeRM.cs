﻿namespace Module.Configuration.Application.Models
{
    public class LanguageCodeRM
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
