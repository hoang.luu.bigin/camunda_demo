﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module.Configuration.Application.Models
{
    public class SettingRM
    {
        public string Id { get; set; }
        public bool IsPublic { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string DataType { get; set; }
    }
}
