﻿using MediatR;
using System;

namespace _Shared.Application
{
    public abstract class AuthRequestBase<T>: IRequest<T>
    {
        protected byte _systemId;
        public byte GetSystemId() => _systemId;
        public void SetSystemId(byte systemId) => _systemId = systemId;

        protected Guid _userId;
        public Guid GetUserId() => _userId;
        public void SetUserId(Guid userId) => _userId = userId;


        protected string _entityType;
        public string GetEntityType() => _entityType;
        public void SetEntityType(string entityType) => _entityType = entityType;


        protected string _entityCode;
        public string GetEntityCode() => _entityCode;
        public void SetEntityCode(string entityCode) => _entityCode = entityCode;

        protected string _entityId;
        public string GetEntityId() => _entityId;
        public void SetEntityId(string entityId) => _entityId = entityId;
    }
}
