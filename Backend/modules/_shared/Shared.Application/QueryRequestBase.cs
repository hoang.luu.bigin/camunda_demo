﻿namespace _Shared.Application
{
    public abstract class QueryRequestBase
    {
        public string? Culture { get; set; } = "vi-VN";
    }
}
