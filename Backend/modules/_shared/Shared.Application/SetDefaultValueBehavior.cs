using MediatR;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace _Shared.Application
{
    public class SetDefaultValueBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            // Create a new instance then we have default values 
            var tempRequest = (TRequest)Activator.CreateInstance(typeof(TRequest));

            foreach (PropertyInfo src in tempRequest.GetType().GetProperties())
            {
                foreach (PropertyInfo dest in request.GetType().GetProperties())
                {
                    if (dest.Name.Equals(src.Name, StringComparison.Ordinal) && dest.CanWrite
                        && src.GetValue(tempRequest) != null && dest.GetValue(request) == null)
                    {
                        // Set default value to request
                        dest.SetValue(request, src.GetValue(tempRequest), null);
                        break;
                    }
                }
            }

            return await next();
        }
    }
}
