﻿namespace _Shared.Application
{
    public interface ISystemIdProvider
    {
        byte SystemId { get; set; }
    }

    public class SystemIdProvider : ISystemIdProvider
    {
       public byte SystemId { get; set; }
    }
}
