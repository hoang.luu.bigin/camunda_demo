﻿using System;

namespace _Shared.Application
{
    public abstract class RMBase
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
