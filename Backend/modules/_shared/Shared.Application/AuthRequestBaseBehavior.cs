using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace _Shared.Application
{
    public class AuthRequestBaseBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private IHttpContextAccessor _httpContextAccessor;

        public AuthRequestBaseBehavior(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (request is AuthRequestBase<TResponse> authRequest)
            {
                string sub = _httpContextAccessor.HttpContext.User.FindFirstValue("sub");
                if (!string.IsNullOrWhiteSpace(sub))
                    authRequest.SetUserId(Guid.Parse(sub));

                var system_id_string = _httpContextAccessor.HttpContext.User.FindFirstValue("system_id") ?? string.Empty;
                var rs = byte.TryParse(system_id_string, out byte system_id);
                if (rs) 
                    authRequest.SetSystemId(system_id);

                authRequest.SetEntityType(_httpContextAccessor.HttpContext.User.FindFirstValue("entity_type"));
                authRequest.SetEntityCode(_httpContextAccessor.HttpContext.User.FindFirstValue("entity_code"));

                string entityId = _httpContextAccessor.HttpContext.User.FindFirstValue("entity_id");
                if (!string.IsNullOrWhiteSpace(entityId))
                    authRequest.SetEntityId(entityId);
            }

            return await next();
        }
    }
}
