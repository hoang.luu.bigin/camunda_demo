﻿using CloudKit.Common.Types;
using Mapster;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace _Shared.Application
{
    public static class MongoDbExtensions
    {
        public static IFindFluent<TDocument, TProjection> Paging<TDocument, TProjection>(this IFindFluent<TDocument, TProjection> query, int start, int length)
        {
            return query.Skip(start)
                          .Limit(length);
        }

        public static IFindFluent<TDocument, TProjection> SortEx<TDocument, TProjection>(this IFindFluent<TDocument, TProjection> query, string orderBy)
        {
            if (!string.IsNullOrEmpty(orderBy))
            {
                string[] sortexps = orderBy.Split(" ");
                SortDefinition<TDocument> sortDefinition = null;
                if (sortexps[1] == "desc")
                    sortDefinition = Builders<TDocument>.Sort.Descending(sortexps[0]).Ascending("_id");
                else
                    sortDefinition = Builders<TDocument>.Sort.Ascending(sortexps[0]).Ascending("_id");
                query = query.Sort(sortDefinition);
            }

            var col = Collation.Simple.With(locale: "vi", caseLevel: true, numericOrdering: true);
            query.Options.Collation = col;

            return query;
        }

        public static async Task<EntityList<TOutDocument>> PagingAndSortAsync<TDocument, TOutDocument, TProjection>(this IFindFluent<TDocument, TProjection> query, int start, int length, string orderBy)
        {
            var totalTask = query.CountDocumentsAsync();
            query = query.SortEx(orderBy)
                          .Paging(start, length);
            return new EntityList<TOutDocument>()
            {
                Total = (int)await totalTask,
                Entities = (await query.ToListAsync()).Adapt<List<TOutDocument>>()
            };
        }

        public static async Task<EntityList<TProjection>> PagingAndSortAsync<TDocument, TProjection>(this IFindFluent<TDocument, TProjection> query, int start, int length, string orderBy)
        {
            var totalTask = query.CountDocumentsAsync();
            query = query.SortEx(orderBy)
                          .Paging(start, length);

            return new EntityList<TProjection>()
            {
                Total = (int)await totalTask,
                Entities = await query.ToListAsync()
            };
        }

        public static Task<TDocument> SingleOrDefaultExAsync<TDocument>(this IMongoCollection<TDocument> collection, Expression<Func<TDocument, bool>> filter, FindOptions options = null, CancellationToken cancellationToken = default)
        {
            return collection.Find(filter, options).Limit(1).SingleOrDefaultAsync(cancellationToken);
        }

        public static Task<TDocument> FirstOrDefaultExAsync<TDocument>(this IMongoCollection<TDocument> collection, Expression<Func<TDocument, bool>> filter, FindOptions options = null, CancellationToken cancellationToken = default)
        {
            return collection.Find(filter, options).Limit(1).FirstOrDefaultAsync(cancellationToken);
        }
    }
}
