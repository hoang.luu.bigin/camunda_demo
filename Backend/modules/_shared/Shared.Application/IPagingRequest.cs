﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _Shared.Application
{
    public interface IPagingRequest
    {
        int Start { get; set; } 
        int Length { get; set; } 
        string? OrderBy { get; set; }
    }
}
