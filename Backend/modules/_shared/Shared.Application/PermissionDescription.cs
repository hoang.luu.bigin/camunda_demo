﻿using System.Collections.Generic;
using System.Linq;

namespace _Shared.Application
{
    public class PermissionDescription
    {
        public string Code { get; set; }
        public string? Description { get; set; }
        public string? DisplayName { get; set; }
        public PermissionType Type { get; set; } = PermissionType.Page;
    }

    public enum PermissionType : byte
    {
        Page = 1,
        Function
    }

    public class PermissionDescriptionGroup
    {
        public string? Code { get; set; }
        public float Priority { get; set; } 
        public IList<PermissionDescription> Permissions { get; set; }
    }

    public class PermissionDescriptionGroupCollection : List<PermissionDescriptionGroup>
    {
        public IEnumerable<PermissionDescription> GetAllPermissions()
        {
            return this.SelectMany(x => x.Permissions); 
        }
    }
}
