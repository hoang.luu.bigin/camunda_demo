﻿using System.Collections.Generic;

namespace _Shared.Application
{
    public interface IPermissionRegistration
    {
        public const string PERMISSION_PREFIX = "Permission."; 
        PermissionDescriptionGroupCollection Register(PermissionDescriptionGroupCollection permissionCollection);
    }
}
