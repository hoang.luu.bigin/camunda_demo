﻿using System;

namespace _Shared.Application
{
    public abstract class ContentRMBase : RMBase
    {
        public Guid Id { get; set; }
        public string? Slug { get; set; }
        public string? SeoTitle { get; set; }
        public string? SeoKeyword { get; set; }
        public string? SeoDescription { get; set; }
        public int Order { get; set; }
        public string Status { get; set; }
        public bool IsFeature { get; set; }
    }
}
