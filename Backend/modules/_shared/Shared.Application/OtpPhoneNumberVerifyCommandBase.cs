﻿using System;

namespace _Shared.Application
{
    public class OtpPhoneNumberVerifyCommandBase
    {
        public Guid OtpRequestId { get; set; }

        public string PhoneNumber { get; set; }

        public string OtpToken { get; set; }
    }
}
