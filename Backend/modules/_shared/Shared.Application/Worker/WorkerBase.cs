﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.Application.Worker
{
    public abstract class WorkerBase : BackgroundService
    {
        protected virtual int Interval { get; } = 10;
        protected readonly ILogger _logger;
        protected readonly IServiceProvider _services;
        public WorkerBase(IServiceProvider services, ILogger logger)
        {
            _logger = logger;
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var serviceScope = _services.CreateScope())
                {
                    var services = serviceScope.ServiceProvider;

                    try
                    {
                        await ExecuteTasks(services);

                        // Use the context here
                    }
                    catch (Exception ex)
                    {
                        var logger = services.GetRequiredService<ILogger<WorkerBase>>();
                        logger.LogError(ex, "An error occurred.");
                    }
                }
                _logger.LogInformation("ProductWorker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(Interval * 1000 * Convert.ToInt32(Environment.GetEnvironmentVariable("Delay") ?? "1"), stoppingToken);
            }
        }

        protected abstract Task ExecuteTasks(IServiceProvider services);
    }
}
