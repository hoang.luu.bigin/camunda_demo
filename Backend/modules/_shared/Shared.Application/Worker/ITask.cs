using System.Threading.Tasks;

namespace Shared.Application.Worker
{
    public interface ITask
    {
        Task<bool> ExecutionAsync();
    }
}