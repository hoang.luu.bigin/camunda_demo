﻿using CloudKit.Infrastructure.OTPService;
using FluentValidation;

namespace _Shared.Application.Validators
{
    public class OtpPhoneNumberVerifyCommandValidator<T> : AbstractValidator<T>
        where T: OtpPhoneNumberVerifyCommandBase
    {
        protected readonly IOtpService _otpService;
        public OtpPhoneNumberVerifyCommandValidator(IOtpService otpService)
        {
            _otpService = otpService;
            RuleFor(x => x.OtpRequestId).NotNull().NotEmpty();
            RuleFor(x => x.PhoneNumber).NotNull().NotEmpty();
            RuleFor(x => x.OtpToken).NotNull().NotEmpty();
            RuleFor(x => x).MustAsync(async (x, cancellation) => {
                return await _otpService.ValidateOtpAsync(x.OtpRequestId, x.PhoneNumber, x.OtpToken);
            }).WithMessage("Otp token is invalid!");
        }
    }
}
