﻿using CloudKit.Common.Types;

namespace _Shared.Application
{
    public class PagingAuthRequestBase<T> : AuthRequestBase<T>, IPagingRequest
    {
        public int Start { get; set; } = 0;
        public int Length { get; set; } = 10;
        public string? OrderBy { get; set; }

    }
}
