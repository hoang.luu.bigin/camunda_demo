﻿namespace _Shared.Application
{
    public abstract class QueryPagingBase : QueryRequestBase
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string? OrderBy { get; set; }
    }
}
