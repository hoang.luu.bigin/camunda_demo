﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Module.Camundas.Application.Commands;
using Module.Camundas.Infrastructure;
using Module.Camundas.Infrastructure.Persistent;

[assembly: HostingStartup(typeof(Canmundas.API.HostingStartup))]
namespace Canmundas.API
{
    public class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.Scan(scan =>
                      scan
                          .FromAssembliesOf(typeof(SaveDataCommand))
                          .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                              .AsImplementedInterfaces()
                              .WithTransientLifetime());
                services.AddValidatorsFromAssemblyContaining(typeof(SaveDataCommand), ServiceLifetime.Transient);

                services.AddSingleton<CamundasDbContext>();
                services.Configure<CamundasDbSettings>(context.Configuration.GetSection("CamundasDbSettings"));

                services.AddSingleton(_ => new BpmnService(context.Configuration["CamundaRestApiUri"]));
            });

            builder.ConfigureAppConfiguration((context, config) =>
            {
            });
        }
    }
}
