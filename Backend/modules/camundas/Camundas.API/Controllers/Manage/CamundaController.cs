﻿using Camunda.Api.Client.ProcessDefinition;
using Camunda.Api.Client.UserTask;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Module.Camundas.Infrastructure;
using Module.Sales.Application.Queries.Orders;
using MongoDB.Bson;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;
using System.Threading.Tasks;

namespace Canmundas.API.Controllers.Manage
{
    //[EnableCors("AllowOriginsPolicy")]
    [ApiController]
    [Route("v1/Manage/[controller]")]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    public class CamundaController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly BpmnService _bpmnService;

        public CamundaController(IMediator mediator, BpmnService bpmnService)
        {
            _mediator = mediator;
            _bpmnService = bpmnService;
        }

        [HttpGet]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> GetData([FromQuery] GetCamundaDataQuery query)
        {
            var rs = await _mediator.Send(query);
            return Ok(rs);
        }
        
        //
        //

        [HttpGet("process-definition")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" },
            Summary = "Root admin used only"
        )]
        public async Task<IActionResult> ProcessDefinitionList([FromQuery] bool latestVersion = true)
        {
            var rs = await _bpmnService.camunda.ProcessDefinitions.Query(new ProcessDefinitionQuery
            {
                LatestVersion = latestVersion
            }).List();
            return Ok(rs);
        }

        [HttpGet("process-definition/key/{key}/form-variables")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> ProcessDefinitionFormVariablesByKey([FromRoute] string key)
        {
            var rs = await _bpmnService.camunda.ProcessDefinitions.ByKey(key).GetFormVariables();
            return Ok(rs);
        }

        [HttpPost("process-definition/key/{key}/submit-form")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> ProcessDefinitionSubmitForm([FromRoute] string key, [FromBody] SubmitStartForm form)
        {
            form.BusinessKey = string.IsNullOrEmpty(form.BusinessKey) ? ObjectId.GenerateNewId().ToString() : form.BusinessKey;
            var rs = await _bpmnService.camunda.ProcessDefinitions.ByKey(key).SubmitForm(form);
            return Ok(rs);
        }

        //
        //

        [HttpGet("process-instance/{processDefinitionKey}")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" },
            Summary = "Root admin used only"
        )]
        public async Task<IActionResult> ProcessInstancesByKey([FromRoute] string processDefinitionKey)
        {
            var rs = await _bpmnService.camunda.ProcessInstances.Query(new Camunda.Api.Client.ProcessInstance.ProcessInstanceQuery
            {
                ProcessDefinitionKey = processDefinitionKey
            }).List();
            return Ok(rs);
        }

        [HttpGet("process-instance/{id}/variables")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> ProcessInstanceVariablesById([FromRoute] string id)
        {
            var rs = await _bpmnService.camunda.ProcessInstances[id].Variables.GetAll();
            return Ok(rs);
        }

        [HttpDelete("process-instance/{id}")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> ProcessInstanceDeleteById([FromRoute] string id)
        {
            await _bpmnService.camunda.ProcessInstances[id].Delete();
            return Ok(true);
        }

        //
        //

        [HttpGet("task/{processInstanceId}/{name}")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> TaskByIdAndKey([FromRoute] string processInstanceId, [FromRoute] string name)
        {
            var rs = await _bpmnService.camunda.UserTasks.Query(new TaskQuery
            {
                ProcessInstanceId = processInstanceId,
                Name = name
            }).List();
            return Ok(rs.FirstOrDefault());
        }

        [HttpGet("task/{id}/form-variables")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> TaskVariablesById([FromRoute] string id)
        {
            var rs = await _bpmnService.camunda.UserTasks[id].GetFormVariables();
            return Ok(rs);
        }

        [HttpPost("task/{id}/submit-form")]
        [SwaggerOperation(
            Tags = new[] { "Camunda.Manage" }
        )]
        public async Task<IActionResult> TaskSubmitFormById([FromRoute] string id, [FromBody] CompleteTask task)
        {
            var rs = await _bpmnService.camunda.UserTasks[id].SubmitFormAndFetchVariables(task);
            return Ok(rs);
        }
    }
}
