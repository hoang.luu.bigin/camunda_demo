﻿using Camunda.Api.Client;
using FluentValidation;
using MediatR;
using Module.Camundas.Domain;
using Module.Camundas.Infrastructure.Persistent;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Camundas.Application.Commands
{
    public class SaveDataCommand : IRequest<string>
    {
        public string? BusinessKey { get; set; }
        public string? ProcessDefinitionKey { get; set; }
        public string? ProcessInstanceId { get; set; }

        public IDictionary<string, VariableValue>? Variables { get; set; }
    }

    public class SaveDataCommandValidator : AbstractValidator<SaveDataCommand>
    {
        public SaveDataCommandValidator()
        {
        }
    }

    public class SaveDataCommandHandler : IRequestHandler<SaveDataCommand, string>
    {
        private readonly CamundasDbContext _camundasDbContext;

        public SaveDataCommandHandler(CamundasDbContext camundasDbContext)
        {
            _camundasDbContext = camundasDbContext;
        }

        public async Task<string> Handle(SaveDataCommand command, CancellationToken cancellationToken)
        {
            var data = new CamundaObject
            {
                ProcessDefinitionKey = command.ProcessDefinitionKey,
                ProcessInstanceId = command.ProcessInstanceId,
                Variables = command.Variables,
            };
            
            if (!string.IsNullOrEmpty(command.BusinessKey) && ObjectId.TryParse(command.BusinessKey, out ObjectId id))
            {
                data.SetId(id.ToString());
            }
            else
            {
                data.SetId(ObjectId.GenerateNewId().ToString());
            }

            await _camundasDbContext.CamundaObjects.ReplaceOneAsync(x =>x.Id == data.Id, data, new ReplaceOptions { IsUpsert = true });

            return data.Id;
        }
    }
}
