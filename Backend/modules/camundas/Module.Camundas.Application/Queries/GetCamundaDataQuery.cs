﻿using FluentValidation;
using MediatR;
using Module.Camundas.Domain;
using Module.Camundas.Infrastructure.Persistent;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Module.Sales.Application.Queries.Orders
{
    public class GetCamundaDataQuery : IRequest<List<CamundaObject>>
    {
    }

    public class GetCamundaDataQueryValidator : AbstractValidator<GetCamundaDataQuery>
    {
        public GetCamundaDataQueryValidator()
        {
        }
    }

    public class GetCamundaDataQueryHandler : IRequestHandler<GetCamundaDataQuery, List<CamundaObject>>
    {
        private readonly CamundasDbContext _camundasDbContext;

        public GetCamundaDataQueryHandler(CamundasDbContext camundasDbContext)
        {
            _camundasDbContext = camundasDbContext;
        }

        public async Task<List<CamundaObject>> Handle(GetCamundaDataQuery query, CancellationToken cancellationToken)
        {
            var data = await _camundasDbContext.CamundaObjects
                .Find(x => true)
                .ToListAsync();

            return data;
        }

    }
}
