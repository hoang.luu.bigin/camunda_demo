using Camunda.Api.Client.ExternalTask;
using Camunda.Worker;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Camundas.Worker.Handlers
{
    [HandlerTopics("Topic_NotifyARC_InvoiceApproved", LockDuration = 10_000)]
    [HandlerVariables("invoiceId")]
    public class NotifyARCInvoiceApproved : IExternalTaskHandler
    {
        private readonly IMediator _mediator;

        public NotifyARCInvoiceApproved(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IExecutionResult> HandleAsync(LockedExternalTask externalTask, CancellationToken cancellationToken)
        {
            //await _mediator.Send(new NotifyARCCommand
            //{
            //    InvoiceId = externalTask.Variables["invoiceId"].AsString()
            //});

            return new CompleteResult { };
        }
    }
}