using Camunda.Api.Client.ExternalTask;
using Camunda.Worker;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Camundas.Worker.Handlers
{
    [HandlerTopics("Topic_Event_ApproveInvoice_Create", LockDuration = 10_000)]
    [HandlerVariables("invoiceId")]
    public class Event_ApproveInvoice_Create : IExternalTaskHandler
    {
        private readonly IMediator _mediator;

        public Event_ApproveInvoice_Create(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IExecutionResult> HandleAsync(LockedExternalTask externalTask, CancellationToken cancellationToken)
        {
            //await _mediator.Send(new UpdateInvoiceByEventCreateCommand
            //{
            //    Id = externalTask.Variables["invoiceId"].AsString()
            //});

            return new CompleteResult 
            {
                Variables = externalTask.Variables
            };
        }
    }
}