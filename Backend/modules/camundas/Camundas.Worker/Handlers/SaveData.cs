using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using Camunda.Worker;
using MediatR;
using Module.Camundas.Application.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Camundas.Worker.Handlers
{
    [HandlerTopics("Topic_SaveData", LockDuration = 10_000)]
    public class SaveData : IExternalTaskHandler
    {
        private readonly IMediator _mediator;

        public SaveData(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IExecutionResult> HandleAsync(LockedExternalTask externalTask, CancellationToken cancellationToken)
        {
            var _includes = externalTask.Variables.FirstOrDefault(x => x.Key == "_includes").Value?.AsString();
            var includes = _includes?.Split(",").Select(x => x.Trim()).ToList() ?? new List<string>();

            var _excludes = externalTask.Variables.FirstOrDefault(x => x.Key == "_excludes").Value?.AsString();
            var excludes = _excludes?.Split(",").Select(x => x.Trim()).ToList() ?? new List<string>();

            var vars = new Dictionary<string, VariableValue>();
            externalTask.Variables?.Keys.ToList().ForEach(key =>
            {
                if ((!key.StartsWith("_") || includes.Contains(key)) && !excludes.Contains(key))
                {
                    vars.Add(key, externalTask.Variables[key]);
                }
            });

            var id = await _mediator.Send(new SaveDataCommand
            {
                BusinessKey = externalTask.BusinessKey ?? externalTask?.Variables?.FirstOrDefault(x => x.Key == "_id").Value?.AsString(),
                ProcessDefinitionKey = externalTask?.ProcessDefinitionKey,
                ProcessInstanceId = externalTask?.ProcessInstanceId,
                Variables = vars
            });

            if (!externalTask?.Variables?.ContainsKey("_id") ?? false)
                externalTask?.Variables?.Add("_id", VariableValue.FromObject(id));

            return new CompleteResult 
            {
                Variables = externalTask?.Variables
            };
        }
    }
}