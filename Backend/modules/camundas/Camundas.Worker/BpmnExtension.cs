using Camunda.Worker;
using Camunda.Worker.Client;
using Camundas.Worker.Handlers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Camundas.Worker
{
    public static class BpmnExtension
    {
        public static IServiceCollection AddCamunda(this IServiceCollection services, string camundaRestApiUri)
        {
            services.AddExternalTaskClient(client =>
            {
                client.BaseAddress = new Uri(camundaRestApiUri);
            });

            services.AddCamundaWorker("dotnetWorker")
                .AddHandler<NotifyARCInvoiceReceived>()
                .AddHandler<NotifyARCInvoiceApproved>()
                .AddHandler<NotifyMoverInvoiceRejected>()

                .AddHandler<Event_ApproveInvoice_Create>()
                .AddHandler<SaveData>()

                .ConfigurePipeline(pipeline =>
                {
                    pipeline.Use(next => async context =>
                    {
                        var logger = context.ServiceProvider.GetRequiredService<ILogger<object>>();
                        logger.LogInformation("Started processing of task {Id}", context.Task.Id);
                        await next(context);
                        logger.LogInformation("Finished processing of task {Id}", context.Task.Id);
                    });
                });

            return services;
        }
    }
}