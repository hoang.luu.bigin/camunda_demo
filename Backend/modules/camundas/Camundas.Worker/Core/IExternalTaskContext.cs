using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using Camunda.Worker.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Camunda.Worker
{
    public interface IExternalTaskContext
    {
        LockedExternalTask Task { get; }

        IExternalTaskClient Client { get; }

        IServiceProvider ServiceProvider { get; }

        [Obsolete("Use Client instead")]
        Task ExtendLockAsync(int newDuration);

        [Obsolete("Use Client instead")]
        Task CompleteAsync(
            Dictionary<string, VariableValue>? variables = null,
            Dictionary<string, VariableValue>? localVariables = null
        );

        [Obsolete("Use Client instead")]
        Task ReportFailureAsync(
            string? errorMessage,
            string? errorDetails,
            int? retries = default,
            int? retryTimeout = default
        );

        [Obsolete("Use Client instead")]
        Task ReportBpmnErrorAsync(
            string errorCode,
            string errorMessage,
            Dictionary<string, VariableValue>? variables = null
        );
    }
}
