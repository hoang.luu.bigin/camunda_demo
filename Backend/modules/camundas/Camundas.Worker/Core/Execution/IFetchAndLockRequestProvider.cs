using Camunda.Api.Client.ExternalTask;

namespace Camunda.Worker.Execution
{
    public interface IFetchAndLockRequestProvider
    {
        /// <summary>
        /// This method is called in the worker before each "fetch and lock" operation
        /// </summary>
        FetchExternalTasks GetRequest();
    }
}
