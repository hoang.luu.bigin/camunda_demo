using Camunda.Api.Client.ExternalTask;

namespace Camunda.Worker.Execution
{
    public interface IEndpointProvider
    {
        ExternalTaskDelegate GetEndpointDelegate(LockedExternalTask externalTask);
    }
}
