using System.Collections.Generic;

namespace Camunda.Worker.Execution
{
    public class HandlerMetadata
    {
        public HandlerMetadata(List<string> topicNames, int lockDuration = Constants.MinimumLockDuration)
        {
            TopicNames = Guard.NotNull(topicNames, nameof(topicNames));
            LockDuration = Guard.GreaterThanOrEqual(lockDuration, Constants.MinimumLockDuration, nameof(lockDuration));
        }

        public List<string> TopicNames { get; }

        public int LockDuration { get; }

        public bool LocalVariables { get; set; }

        /// <summary>Determines whether serializable variable values (typically variables that store custom Java objects) should be deserialized on server side (default false).</summary>
        public bool DeserializeValues { get; set; }

        public List<string>? Variables { get; set; }

        public List<string>? ProcessDefinitionIds { get; set; }

        public List<string>? ProcessDefinitionKeys { get; set; }

        public List<string>? TenantIds { get; set; }
    }
}
