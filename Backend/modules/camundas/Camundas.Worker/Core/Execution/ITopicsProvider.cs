using Camunda.Api.Client.ExternalTask;
using System.Collections.Generic;

namespace Camunda.Worker.Execution
{
    public interface ITopicsProvider
    {
        List<FetchExternalTaskTopic> GetTopics();
    }
}
