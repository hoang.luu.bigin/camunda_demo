using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using Camunda.Worker.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Camunda.Worker.Execution
{
    public sealed class ExternalTaskContext : IExternalTaskContext
    {
        public ExternalTaskContext(LockedExternalTask task, IExternalTaskClient client, IServiceProvider provider)
        {
            Task = Guard.NotNull(task, nameof(task));
            Client = Guard.NotNull(client, nameof(client));
            ServiceProvider = Guard.NotNull(provider, nameof(provider));
        }

        public LockedExternalTask Task { get; }

        public IExternalTaskClient Client { get; }

        public IServiceProvider ServiceProvider { get; }

        public async Task ExtendLockAsync(int newDuration)
        {
            var request = new ExternalTaskExtendLock { WorkerId = Task.WorkerId, NewDuration = newDuration };
            await Client.ExtendLockAsync(Task.Id, request);
        }

        public async Task CompleteAsync(
            Dictionary<string, VariableValue>? variables = null,
            Dictionary<string, VariableValue>? localVariables = null
        )
        {
            var request = new CompleteExternalTask
            {
                WorkerId = Task.WorkerId,
                Variables = variables,
                LocalVariables = localVariables
            };
            await Client.CompleteAsync(Task.Id, request);
        }

        public async Task ReportFailureAsync(
            string? errorMessage,
            string? errorDetails,
            int? retries = default,
            int? retryTimeout = default
        )
        {
            var request = new ExternalTaskFailure
            {
                WorkerId = Task.WorkerId,
                ErrorMessage = errorMessage,
                ErrorDetails = errorDetails,
                Retries = retries ?? 0,
                RetryTimeout = retryTimeout ?? 0
            };
            await Client.ReportFailureAsync(Task.Id, request);
        }

        public async Task ReportBpmnErrorAsync(
            string errorCode,
            string errorMessage,
            Dictionary<string, VariableValue>? variables = null
        )
        {
            var request = new ExternalTaskBpmnError
            {
                WorkerId = Task.WorkerId,
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                Variables = variables
            };
            await Client.ReportBpmnErrorAsync(Task.Id, request);
        }
    }
}
