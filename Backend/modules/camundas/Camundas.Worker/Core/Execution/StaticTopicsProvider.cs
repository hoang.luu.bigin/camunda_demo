using Camunda.Api.Client.ExternalTask;
using System.Collections.Generic;
using System.Linq;

namespace Camunda.Worker.Execution
{
    public sealed class StaticTopicsProvider : ITopicsProvider
    {
        private readonly List<FetchExternalTaskTopic> _topics;

        public StaticTopicsProvider(IEnumerable<HandlerDescriptor> handlerDescriptors)
        {
            _topics = handlerDescriptors.SelectMany(ConvertDescriptorToTopic).ToList();
        }

        private static IEnumerable<FetchExternalTaskTopic> ConvertDescriptorToTopic(HandlerDescriptor descriptor)
        {
            return descriptor.Metadata.TopicNames
                .Select(topicName => MakeTopicRequest(descriptor.Metadata, topicName));
        }

        private static FetchExternalTaskTopic MakeTopicRequest(HandlerMetadata metadata, string topicName) =>
            new FetchExternalTaskTopic(topicName, metadata.LockDuration)
            {
                LocalVariables = metadata.LocalVariables,
                Variables = metadata.Variables,
                ProcessDefinitionIds = metadata.ProcessDefinitionIds,
                ProcessDefinitionKeys = metadata.ProcessDefinitionKeys,
                TenantIds = metadata.TenantIds,
                DeserializeValues = metadata.DeserializeValues,
            };

        public List<FetchExternalTaskTopic> GetTopics()
        {
            return _topics;
        }
    }
}
