using Camunda.Api.Client.ExternalTask;
using Microsoft.Extensions.Options;

namespace Camunda.Worker.Execution
{
    public class LegacyFetchAndLockRequestProvider : IFetchAndLockRequestProvider
    {
        private readonly ITopicsProvider _topicsProvider;
        private readonly FetchAndLockOptions _options;

        public LegacyFetchAndLockRequestProvider(
            ITopicsProvider topicsProvider,
            IOptions<FetchAndLockOptions> options
        )
        {
            _topicsProvider = topicsProvider;
            _options = options.Value;
        }

        public FetchExternalTasks GetRequest()
        {
            var topics = _topicsProvider.GetTopics();

            var fetchAndLockRequest = new FetchExternalTasks
            {
                WorkerId = _options.WorkerId,
                MaxTasks = _options.MaxTasks,
                UsePriority = _options.UsePriority,
                AsyncResponseTimeout = _options.AsyncResponseTimeout,
                Topics = topics
            };

            return fetchAndLockRequest;
        }
    }
}
