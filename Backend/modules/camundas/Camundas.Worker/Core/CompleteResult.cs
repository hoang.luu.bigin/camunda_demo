using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using Camunda.Worker.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Camunda.Worker
{
    public sealed class CompleteResult : IExecutionResult
    {
        public CompleteResult()
        {
        }

        public Dictionary<string, VariableValue>? Variables { get; set; }

        public Dictionary<string, VariableValue>? LocalVariables { get; set; }

        public async Task ExecuteResultAsync(IExternalTaskContext context)
        {
            var externalTask = context.Task;
            var client = context.Client;

            try
            {
                await client.CompleteAsync(externalTask.Id, new CompleteExternalTask
                {
                    WorkerId = externalTask.WorkerId,
                    Variables = Variables,
                    LocalVariables = LocalVariables,
                });
            }
            catch (ClientException e) when (e.StatusCode == HttpStatusCode.InternalServerError)
            {
                var logger = context.ServiceProvider.GetService<ILogger<CompleteResult>>();
                logger?.LogWarning(e, "Failed completion of task {TaskId}. Reason: {Reason}",
                    externalTask.Id, e.Message
                );
                await client.ReportFailureAsync(externalTask.Id, new ExternalTaskFailure
                {
                    WorkerId = externalTask.WorkerId,
                    ErrorMessage = e.ErrorType,
                    ErrorDetails = e.ErrorMessage,
                });
            }
        }
    }
}
