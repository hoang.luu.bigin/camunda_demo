using Camunda.Api.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Xml.Linq;

namespace Camunda.Worker
{
    public static class VariableValueExtension
    {
        public static bool AsBoolean(this VariableValue variable)
        {
            return Convert.ToBoolean(variable.Value);
        }

        public static short AsShort(this VariableValue variable)
        {
            return Convert.ToInt16(variable.Value);
        }

        public static int AsInteger(this VariableValue variable)
        {
            return Convert.ToInt32(variable.Value);
        }

        public static long AsLong(this VariableValue variable)
        {
            return Convert.ToInt64(variable.Value);
        }

        public static double AsDouble(this VariableValue variable)
        {
            return Convert.ToDouble(variable.Value);
        }

        public static string AsString(this VariableValue variable)
        {
            return Convert.ToString(variable.Value);
        }

        public static VariableValue Json(JObject value)
        {
            return VariableValue.FromObject(value.ToString(Formatting.None));
        }

        public static VariableValue Json(object value)
        {
            return VariableValue.FromObject(JsonConvert.SerializeObject(value));
        }

        public static VariableValue Json(object value, JsonSerializerSettings settings)
        {
            return VariableValue.FromObject(JsonConvert.SerializeObject(value, settings));
        }

        public static T AsJson<T>(this VariableValue variable, JsonSerializerSettings? settings = null)
        {
            var stringValue = Convert.ToString(variable.Value);
            if (string.IsNullOrEmpty(stringValue))
                return default;
            return JsonConvert.DeserializeObject<T>(stringValue, settings);
        }

        public static JObject AsJObject(this VariableValue variable)
        {
            var stringValue = Convert.ToString(variable.Value);
            return JObject.Parse(stringValue);
        }

        public static VariableValue Xml(XElement value)
        {
            return VariableValue.FromObject(value.ToString(SaveOptions.DisableFormatting));
        }

        public static XElement AsXElement(this VariableValue variable)
        {
            var stringValue = Convert.ToString(variable.Value);
            return XElement.Parse(stringValue);
        }

        public static bool Equals(this VariableValue variable, VariableValue other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(variable, other)) return true;
            return Equals(variable.Value, other.Value) && variable.Value.GetType() == other.Value.GetType();
        }

        public static bool Equals(this VariableValue variable, object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(variable, obj)) return true;
            if (obj.GetType() != variable.Value.GetType()) return false;
            return Equals(variable, (VariableValue)obj);
        }
    }

}
