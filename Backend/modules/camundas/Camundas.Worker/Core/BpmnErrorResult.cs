using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using Camunda.Worker.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Camunda.Worker
{
    public sealed class BpmnErrorResult : IExecutionResult
    {
        public BpmnErrorResult(string errorCode, string errorMessage, Dictionary<string, VariableValue>? variables = null)
        {
            ErrorCode = Guard.NotEmptyAndNotNull(errorCode, nameof(errorCode));
            ErrorMessage = Guard.NotEmptyAndNotNull(errorMessage, nameof(errorMessage));
            Variables = variables;
        }

        public string ErrorCode { get; }
        public string ErrorMessage { get; }
        public Dictionary<string, VariableValue>? Variables { get; }

        public async Task ExecuteResultAsync(IExternalTaskContext context)
        {
            var externalTask = context.Task;
            var client = context.Client;

            try
            {
                await client.ReportBpmnErrorAsync(
                    externalTask.Id,
                    new ExternalTaskBpmnError
                    {
                        WorkerId = externalTask.WorkerId,
                        ErrorCode = ErrorCode,
                        ErrorMessage = ErrorMessage,
                        Variables = Variables,
                    }
                );
            }
            catch (ClientException e) when (e.StatusCode == HttpStatusCode.InternalServerError)
            {
                var logger = context.ServiceProvider.GetService<ILogger<BpmnErrorResult>>();
                logger?.LogWarning(e, "Failed completion of task {TaskId}. Reason: {Reason}",
                    externalTask.Id, e.Message
                );
                await client.ReportFailureAsync(externalTask.Id, new ExternalTaskFailure
                {
                    WorkerId = externalTask.WorkerId,
                    ErrorMessage = e.ErrorType,
                    ErrorDetails = e.ErrorMessage,
                });
            }
        }
    }
}
