using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Camunda.Worker.Client
{
    public class ExternalTaskClient : IExternalTaskClient
    {
        public readonly CamundaClient camunda;

        public ExternalTaskClient(HttpClient httpClient)
        {
            camunda = CamundaClient.Create(httpClient);
        }

        public Task<List<LockedExternalTask>> FetchAndLockAsync(
            FetchExternalTasks request,
            CancellationToken cancellationToken = default
        )
        {
            return camunda.ExternalTasks.FetchAndLock(request);
        }

        public Task CompleteAsync(
            string taskId, CompleteExternalTask request,
            CancellationToken cancellationToken = default
        )
        {
            return camunda.ExternalTasks[taskId].Complete(request);
        }

        public Task ReportFailureAsync(
            string taskId, ExternalTaskFailure request,
            CancellationToken cancellationToken = default
        )
        {
            return camunda.ExternalTasks[taskId].HandleFailure(request);
        }

        public Task ReportBpmnErrorAsync(
            string taskId, ExternalTaskBpmnError request,
            CancellationToken cancellationToken = default
        )
        {
            return camunda.ExternalTasks[taskId].HandleBpmnError(request);
        }

        public Task ExtendLockAsync(
            string taskId, ExternalTaskExtendLock request,
            CancellationToken cancellationToken = default
        )
        {
            return camunda.ExternalTasks[taskId].ExtendLock(request);
        }
    }
}
