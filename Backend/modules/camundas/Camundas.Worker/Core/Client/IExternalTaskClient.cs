using Camunda.Api.Client.ExternalTask;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Camunda.Worker.Client
{
    public interface IExternalTaskClient
    {
        Task<List<LockedExternalTask>> FetchAndLockAsync(
            FetchExternalTasks request,
            CancellationToken cancellationToken = default
        );

        Task CompleteAsync(
            string taskId, CompleteExternalTask request,
            CancellationToken cancellationToken = default
        );

        Task ReportFailureAsync(
            string taskId, ExternalTaskFailure request,
            CancellationToken cancellationToken = default
        );

        Task ReportBpmnErrorAsync(
            string taskId, ExternalTaskBpmnError request,
            CancellationToken cancellationToken = default
        );

        Task ExtendLockAsync(
            string taskId, ExternalTaskExtendLock request,
            CancellationToken cancellationToken = default
        );
    }
}
