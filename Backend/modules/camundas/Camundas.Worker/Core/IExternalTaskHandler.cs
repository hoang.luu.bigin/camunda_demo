using Camunda.Api.Client.ExternalTask;
using System.Threading;
using System.Threading.Tasks;

namespace Camunda.Worker
{
    public interface IExternalTaskHandler
    {
        Task<IExecutionResult> HandleAsync(LockedExternalTask externalTask, CancellationToken cancellationToken);
    }
}
