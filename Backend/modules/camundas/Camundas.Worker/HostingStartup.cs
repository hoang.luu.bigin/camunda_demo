﻿using Camundas.Worker;
using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(HostingStartup))]
namespace Camundas.Worker
{
    public class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.AddCamunda(context.Configuration["CamundaRestApiUri"]);
            });

            builder.ConfigureAppConfiguration((context, config) =>
            {
            });
        }
    }
}
