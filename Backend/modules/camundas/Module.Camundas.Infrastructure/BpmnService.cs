using Camunda.Api.Client;
using Camunda.Api.Client.Deployment;
using Camunda.Api.Client.Message;
using Camunda.Api.Client.ProcessDefinition;
using Camunda.Api.Client.ProcessInstance;
using Camunda.Api.Client.UserTask;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Module.Camundas.Infrastructure
{
    public class BpmnService
    {
        public readonly CamundaClient camunda;

        public BpmnService(string camundaRestApiUri)
        {
            camunda = CamundaClient.Create(camundaRestApiUri);
        }

        public async Task<string> StartProcessForInvoiceRecieved(string invoiceId, string moverName)
        {
            var processParams = new StartProcessInstance()
                .SetVariable("invoiceId", VariableValue.FromObject(invoiceId))
                .SetVariable("moverName", VariableValue.FromObject(moverName))
                ;

            processParams.BusinessKey = invoiceId;

            var processStartResult = await
                camunda.ProcessDefinitions.ByKey("Process_TrinhLe").StartProcessInstance(processParams);

            return processStartResult.Id;
        }

        public async Task<List<UserTaskInfo>> GetTasksForCandidateGroup(string group, string user)
        {
            var groupTaskQuery = new TaskQuery
            {
                ProcessDefinitionKeys = { "Process_TrinhLe" },
                CandidateGroup = group,
            };
            var groupTasks = await camunda.UserTasks.Query(groupTaskQuery).List();

            if (user != null)
            {
                var userTaskQuery = new TaskQuery
                {
                    ProcessDefinitionKeys = { "Process_TrinhLe" },
                    Assignee = user
                };
                var userTasks = await camunda.UserTasks.Query(userTaskQuery).List();

                groupTasks.AddRange(userTasks);
            }

            return groupTasks;
        }

        public async Task<UserTaskInfo> ClaimTask(string taskId, string user)
        {
            await camunda.UserTasks[taskId].Claim(user);
            var task = await camunda.UserTasks[taskId].Get();
            return task;
        }

        public async Task<UserTaskInfo> CompleteTaskApproveInvoice(string taskId, string invoiceId, string result)
        {
            var task = await camunda.UserTasks[taskId].Get();
            var completeTask = new CompleteTask()
                .SetVariable("invoiceId", VariableValue.FromObject(invoiceId))
                .SetVariable("result", VariableValue.FromObject(result))
                ;
            await camunda.UserTasks[taskId].Complete(completeTask);
            return task;
        }

        public async Task SendMessageInvoicePaid(string orderId)
        {
            await camunda.Messages.DeliverMessage(new CorrelationMessage
            {
                BusinessKey = orderId,
                MessageName = "Message_InvoicePaid"
            });
        }

        public async Task CleanupProcessInstances()
        {
            var instances = await camunda.ProcessInstances
                .Query(new ProcessInstanceQuery
                {
                    ProcessDefinitionKey = "Process_TrinhLe"
                })
                .List();

            if (instances.Count > 0)
            {
                await camunda.ProcessInstances.Delete(new DeleteProcessInstances
                {
                    ProcessInstanceIds = instances.Select(i => i.Id).ToList()
                });
            }
        }
    }
}