﻿using CloudKit.Infrastructure.Data.MongoDb;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Module.Camundas.Domain;
using MongoDB.Driver;

namespace Module.Camundas.Infrastructure.Persistent
{
    public class CamundasDbSettings : CloudKit.Infrastructure.Data.MongoDb.MongoDatabaseSettings
    {
    }

    public class CamundasDbContext : MongoDbBaseContext
    {
        protected override string PrefixTable => "Camundas";

        public CamundasDbContext(
            IOptionsMonitor<CamundasDbSettings> settings,
            ILoggerFactory loggerFactory
            ) : base(settings, loggerFactory)
        {
        }

        public IMongoCollection<CamundaObject> CamundaObjects { get { return GetCollection<CamundaObject>(); } }
    }
}
