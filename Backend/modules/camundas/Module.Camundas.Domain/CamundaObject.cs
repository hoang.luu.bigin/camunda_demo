using Camunda.Api.Client;
using CloudKit.Domain;
using System.Collections.Generic;

namespace Module.Camundas.Domain
{
    public class CamundaObject : EntityBase<string>
    {
        public string? ProcessDefinitionKey { get; set; }
        public string? ProcessInstanceId { get; set; }

        public IDictionary<string, VariableValue>? Variables { get; set; }

        public void SetId(string id)
        {
            Id = id;
        }
    }
}