﻿using CloudKit.Infrastructure.Data.MongoDb;
using IdentityServer4.EntityFramework.Mappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Module.Configuration.Infrastructure.Data;
using Module.Identity.Application.Commands.Admin;
using Module.Identity.Infrastructure.Data;
using MongoDBMigrations;
using Serilog;
using System;

namespace APIHost
{
    public class MigrationRunner
    {
        public async System.Threading.Tasks.Task RunAsync(IServiceProvider serviceProvider, ILogger logger)
        {
            try
            {
                await serviceProvider
                    .GetRequiredService<ConfigurationDbContext>()
                    .Database
                    .MigrateAsync();

                AppIdentityDbContext appIdentityDbContext = serviceProvider.GetRequiredService<AppIdentityDbContext>();
                await appIdentityDbContext.Database.MigrateAsync();

                await serviceProvider
                    .GetRequiredService<IdentityServer4.EntityFramework.DbContexts.PersistedGrantDbContext>()
                    .Database
                    .MigrateAsync();

                var configurationDbContext = serviceProvider
                    .GetRequiredService<IdentityServer4.EntityFramework.DbContexts.ConfigurationDbContext>();
                await configurationDbContext.Database.MigrateAsync();

                if ((await appIdentityDbContext.Histories.FirstOrDefaultAsync(x => x.HistoryId == "InitIdClient")) == null)
                {
                    foreach (var client in Config.Clients)
                    {
                        configurationDbContext.Clients.Add(client.ToEntity());
                    }

                    foreach (var resource in Config.Ids)
                    {
                        configurationDbContext.IdentityResources.Add(resource.ToEntity());
                    }

                    foreach (var resource in Config.Apis)
                    {
                        configurationDbContext.ApiResources.Add(resource.ToEntity());
                    }
                    await configurationDbContext.SaveChangesAsync();

                    appIdentityDbContext.Histories.Add(new History()
                    {
                        HistoryId = "InitIdClient",
                        CreatedAt = DateTime.UtcNow
                    });
                    await appIdentityDbContext.SaveChangesAsync();
                }


                logger.Information("Begin run migration for ConfigurationDatabase");
                var configurationDatabaseSettings = serviceProvider
                    .GetRequiredService<IConfiguration>()
                    .GetSection("ConfigurationDatabaseSettings")
                    .Get<MongoDatabaseSettings>();

                try
                {
                    new MigrationEngine()
                        .UseDatabase(configurationDatabaseSettings.ConnectionString, configurationDatabaseSettings.DatabaseName) //Required to use specific db
                        .UseAssemblyOfType<MongoDbConfigurationDbContext>()
                        .UseSchemeValidation(false)
                        .Run();
                    logger.Information("End run migration for ConfigurationDatabase");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, ex.Message);
                }

                logger.Information("Begin Init Data for identity module");
                var mediator = serviceProvider.GetRequiredService<IMediator>();
                if ((await appIdentityDbContext.Histories
                    .FirstOrDefaultAsync(x => x.HistoryId == "InitRoleAndPermission")) == null
                    )
                {
                    await mediator.Send(new InitCommand());
                    appIdentityDbContext.Histories.Add(new History()
                    {
                        HistoryId = "InitRoleAndPermission",
                        CreatedAt = DateTime.UtcNow
                    });
                    await appIdentityDbContext.SaveChangesAsync();
                }
                if ((await appIdentityDbContext.Histories.FirstOrDefaultAsync(x => x.HistoryId == "InitAdmin")) == null)
                {
                    await mediator.Send(new CreateRootAdminCommand()
                    {
                        Email = "duy.duong@bigin.vn",
                        FullName = "Duy Duong",
                    });
                    appIdentityDbContext.Histories.Add(new History()
                    {
                        HistoryId = "InitAdmin",
                        CreatedAt = DateTime.UtcNow
                    });
                    await appIdentityDbContext.SaveChangesAsync();
                }
                logger.Information("End Init Data for identity module");

            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                System.Threading.Thread.Sleep(10000);
            }
        }
    }
}
