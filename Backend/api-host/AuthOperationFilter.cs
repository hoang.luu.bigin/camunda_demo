﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Reflection;

namespace APIHost
{
    public class AuthOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            AuthorizeAttribute? authorizeAttribute = context.MethodInfo.GetCustomAttribute<AuthorizeAttribute>();
            if (authorizeAttribute == null)
                authorizeAttribute = context.MethodInfo.DeclaringType.GetCustomAttribute<AuthorizeAttribute>();

            if (authorizeAttribute == null || !authorizeAttribute.AuthenticationSchemes.Contains("Bearer"))
            {
                if (context.ApiDescription.RelativePath.Contains("Identity"))
                    operation.Parameters.Add(new OpenApiParameter
                    {
                        Name = "System-Id",
                        In = ParameterLocation.Header,
                        Required = false,
                        Schema = new OpenApiSchema
                        {
                            Type = "number",
                        }
                    });
            }

            if (authorizeAttribute == null)
                return;

            operation.Responses.TryAdd("401", new OpenApiResponse { Description = "Unauthorized" });
            operation.Responses.TryAdd("403", new OpenApiResponse { Description = "Forbidden" });

            operation.Security = new List<OpenApiSecurityRequirement>();

            if (authorizeAttribute.AuthenticationSchemes.Contains("Bearer"))
            {
                var jwtbearerScheme = new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                };
                operation.Security.Add(new OpenApiSecurityRequirement
                {
                    [jwtbearerScheme] = new string[] { }
                });
            }
            if (authorizeAttribute.AuthenticationSchemes.Contains("ApiKey"))
            {
                var apiKeyScheme = new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "ApiKey-InHeader" }
                };
                operation.Security.Add(new OpenApiSecurityRequirement
                {
                    [apiKeyScheme] = new string[] { }
                });
            }
        }
    }
}
