using _Shared.Application;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace APIHost
{
    public class  SystemIdProviderMiddleware
    {
        private readonly RequestDelegate _next;

        public SystemIdProviderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ISystemIdProvider systemIdProvider)
        {
            string? systemId = context.User.FindFirstValue("system_id");

            if (string.IsNullOrEmpty(systemId))
            {
                var bearerHeader = context.Request.Headers["Authorization"].ToString();
                if (!string.IsNullOrEmpty(bearerHeader) && bearerHeader.StartsWith("Bearer"))
                {
                    var bearerToken = bearerHeader.Substring(7);
                    var handler = new JwtSecurityTokenHandler();
                    var token = handler.ReadJwtToken(bearerToken);
                    systemId = token.Claims.FirstOrDefault(x => x.Type == "system_id")?.Value;
                }
            }

            if (string.IsNullOrEmpty(systemId))
                systemId = context.Request.Headers["System-Id"].ToString();

            systemIdProvider.SystemId = string.IsNullOrEmpty(systemId) ? byte.MinValue : byte.Parse(systemId);

            // Call the next delegate/middleware in the pipeline
            await _next(context);
        }
    }
}