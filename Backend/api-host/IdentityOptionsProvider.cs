﻿using Microsoft.Extensions.Configuration;
using Module.Identity.Application;
using System;
using System.Collections.Generic;

namespace APIHost
{
    public class IdentityOptionsProvider : IIdentityOptionsProvider
    {
        private readonly IConfiguration _configuration;
        public IdentityOptionsProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IdentityOptions Resolve(byte systemId, string entityType)
        {
            Dictionary<string, List<IdentityOptions>> idops = _configuration
                .GetSection("IdentityOptions")
                .Get<Dictionary<string, List<IdentityOptions>>>();
            var listOfIdop = idops[systemId.ToString()];
            if (listOfIdop.Count == 1)
                return listOfIdop[0];
            return listOfIdop.Find(x => x.EntityType == entityType);
        }
    }
}
