﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIHost
{
    using AspNetCore.Authentication.ApiKey;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using System.Security.Claims;

    public class ApiKeyProvider : IApiKeyProvider
    {
        private readonly ILogger<ApiKeyProvider> _logger;
        private readonly IConfiguration _configuration;
        public ApiKeyProvider(ILogger<ApiKeyProvider> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public Task<IApiKey> ProvideAsync(string key)
        {
            if (key == _configuration["INBOUND_API_KEY"])
                return Task.FromResult(new ApiKey(string.Empty, "ApiKey") as IApiKey);
            return Task.FromResult(null as IApiKey);
        }
    }
    public class ApiKey : IApiKey
    {
        public ApiKey(string key, string owner, List<Claim> claims = null)
        {
            Key = key;
            OwnerName = owner;
            Claims = claims ?? new List<Claim>();
        }

        public string Key { get; }
        public string OwnerName { get; }
        public IReadOnlyCollection<Claim> Claims { get; }
    }

}
