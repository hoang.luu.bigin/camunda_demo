using CloudKit.Infrastructure.ConfigurationRedis;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NpgsqlTypes;
using Sentry;
using Sentry.Protocol;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.PostgreSQL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace APIHost
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            IHost webHost = CreateHostBuilder(args).Build();
            var runMode = Environment.GetEnvironmentVariable("RunMode");

            if (runMode == "Migration")
            {
                // Create a new scope
                using (var scope = webHost.Services.CreateScope())
                {

                    MigrationRunner migrationRunner = new MigrationRunner();
                    await migrationRunner.RunAsync(scope.ServiceProvider, Log.Logger);
                }
            }
            else
                // Run the WebHost, and start accepting requests
                // There's an async overload, so we may as well use it
                await webHost.RunAsync();
        }

       
        static void ConfigConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder config)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{ctx.HostingEnvironment.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            config.AddRedis(configuration["Redis:Host"], configuration["Redis:Instance"] + "Configuration", true);

            Log.Logger = CreateLogger(configuration);
        }


        private static Logger CreateLogger(IConfigurationRoot configuration)
        {
            try
            {
                string serilogName = configuration["SerilogTo:Name"] ?? "";
                if (serilogName.Equals("PostgreSQL"))
                {
                    string connectionstring = configuration["SerilogTo:ConnectionString"];

                    string tableName = "logs";

                    //Used columns (Key is a column name) 
                    //Column type is writer's constructor parameter
                    IDictionary<string, ColumnWriterBase> columnWriters = new Dictionary<string, ColumnWriterBase>
                    {
                        {"message", new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
                        {"level", new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
                        {"raise_date", new TimestampColumnWriter(NpgsqlDbType.Timestamp) },
                        {"exception", new ExceptionColumnWriter(NpgsqlDbType.Text) },
                        {"props_test", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) },
                        {"client", new SinglePropertyColumnWriter("Client", PropertyWriteMethod.ToString, NpgsqlDbType.Text, "l") },
                        {"machine_name", new SinglePropertyColumnWriter("MachineName", PropertyWriteMethod.ToString, NpgsqlDbType.Text, "l") }
                    };

                    return new LoggerConfiguration()
                                       .WriteTo.PostgreSQL(connectionstring, tableName, columnWriters, needAutoCreateTable: true)
                                       .Enrich.FromLogContext()
                                       .CreateLogger();
                }
                else if(serilogName.Equals("MongoDb"))
                {
                    return new LoggerConfiguration()
                        .WriteTo.MongoDB(configuration["SerilogTo:ConnectionString"])
                        .CreateLogger();
                }
            }
            catch
            {
                return new LoggerConfiguration()
               .ReadFrom.Configuration(configuration)
               .CreateLogger();
            }

            return new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration(ConfigConfiguration);
                    webBuilder.UseSentry();
                    webBuilder.UseStartup<Startup>()
                    .UseSetting(WebHostDefaults.HostingStartupAssembliesKey,
                        string.Join("; ", AssembliesRegistry.AssemblyNames))
                    .UseSerilog();
                });
    }
}
