﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;

namespace APIHost
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource("resourceApi","resourceApi"),
                new ApiResource("notificationApi","notificationApi"),
                new ApiResource("fileServiceApi","fileServiceApi")
            };

      
        static string SPAClientURI => Environment.GetEnvironmentVariable("SPA_CLIENT_URL");
        //  static string SwaggerURI => "https://localhost:20011";
       
        public static IEnumerable<IdentityServer4.Models.Client> Clients =>
            
            new IdentityServer4.Models.Client[]
            {
                
                 new IdentityServer4.Models.Client
                {
                    ClientId = "1STPARTY.APP",
                    ClientName = "FIRST PARTY APP",
                    ClientSecrets =
                    {
                        new Secret("P@ss4FirstPartyApp".Sha256())
                    },
                    RequireConsent = false,
                    AllowedGrantTypes =
                     {"external" , "refresh_token", "password"}
                    ,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "resourceApi",
                        "fileServiceApi",
                        "notificationApi"
                    },
                    AllowOfflineAccess = true,
                    RequireClientSecret = false,
                    AccessTokenLifetime = Environment.GetEnvironmentVariable("AccessTokenLifetime") == null
                     ? 3600
                     : int.Parse(Environment.GetEnvironmentVariable("AccessTokenLifetime")),
                     UpdateAccessTokenClaimsOnRefresh = true,
                    
                    RefreshTokenUsage = TokenUsage.ReUse,
                    AbsoluteRefreshTokenLifetime = 2592000,
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    SlidingRefreshTokenLifetime = 2592000,
                },

            };
    }
}