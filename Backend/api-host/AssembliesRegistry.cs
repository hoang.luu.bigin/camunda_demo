﻿using System.Collections.Generic;

namespace APIHost
{
    public static class AssembliesRegistry
    {
        public static List<string> AssemblyNames = new List<string>()
        {
            "Configuration.API",
            "Identity.API",
            "Sales.API",
            "Camundas.API",
            "Camundas.Worker",
        };
    }
}
