using _Shared.Application;
using AspNetCore.Authentication.ApiKey;
using AspNetCoreRateLimit;
using CloudKit.Common.Extensions;
using CloudKit.Domain;
using CloudKit.Infrastructure.Mailer;
using CloudKit.Infrastructure.Notification;
using CloudKit.Infrastructure.OTPService.DependencyInjection;
using CloudKit.Infrastructure.SmsServices;
using CloudKit.Infrastructure.SmsServices.DependencyInjection;
using CloudKit.Infrastructure.ValidationModel;
using CloudKit.UI.AspNetCore;
using iAspNetcore.Middleware.LogRequest;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Configuration;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using Module.Identity.Application;
using Module.Identity.Domain;
using Module.Identity.Domain.Models;
using Module.Identity.Infrastructure.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NotificationService.Client.DI;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace APIHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            Environment = hostEnvironment;
            _isMigrationMode = Configuration["RunMode"] == "Migration";
            GlobalMongoRegistration.Register();
        }
        private IWebHostEnvironment Environment { get; set; }

        private readonly bool _isMigrationMode;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddNewtonsoftJson(x =>
                {
                    x.SerializerSettings.ContractResolver = new CustomContractResolver()
                    {
                        NamingStrategy = new CamelCaseNamingStrategy(),
                    };
                    x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    //x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            IdentityModelEventSource.ShowPII = true; //Add this line
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;

            AddDbContexts(services);
            services.AddMediatR(typeof(Startup));
            if (!_isMigrationMode)
            {
                services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthRequestBaseBehavior<,>));
                services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            }
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(SetDefaultValueBehavior<,>));

            AddSwaggerServices(services);
            AddLocalizationServices(services);
            AddAuthentication(services);
            AddIS4Services(services);
            AddRateLimitServices(services);
            services.AddSmsServices(Configuration.GetSection("SmsSettings"));
            services.AddOTPServices(Configuration.GetSection("OTPServiceSettings"));
            services.AddScoped<ISystemIdProvider, SystemIdProvider>();

            AddNotificationServices(services);
            AddDataProtectionServices(services);

            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
            });
        }

        private void AddDataProtectionServices(IServiceCollection services)
        {
            IDataProtectionBuilder dataProtectionBuilder = services.AddDataProtection()
               .SetApplicationName("Redis:Instance")
               .PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(Configuration["Redis:Host"]),
                                   Configuration["Redis:Instance"] + "DataProtection");
            if (!Environment.IsDevelopment())
                dataProtectionBuilder.ProtectKeysWithCertificate(
                     new X509Certificate2("camunda.pfx", Configuration["Is4CertificatePassword"]));
        }
        private void AddRateLimitServices(IServiceCollection services)
        {
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));

            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();

            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
        }
        private void AddLocalizationServices(IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(opts =>
            {
                var supportedCultures = new List<CultureInfo>
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("vi-VN")
                };

                opts.DefaultRequestCulture = new RequestCulture("vi-VN");
                // Formatting numbers, dates, etc.
                opts.SupportedCultures = supportedCultures;
                // UI strings that we have localized.
                opts.SupportedUICultures = supportedCultures;
                opts.RequestCultureProviders = new List<IRequestCultureProvider>
                {
                    new QueryStringRequestCultureProvider()
                };

            });
        }
        private void AddDbContexts(IServiceCollection services)
        {
            services.AddDbContext<AppIdentityDbContext>(options =>
            {
                options.UseMySql(Configuration.GetConnectionString("IdentityConnection"));
                options.EnableSensitiveDataLogging();
            });

            services.AddStackExchangeRedisCache(o =>
            {
                o.Configuration = Configuration["Redis:Host"];
                o.InstanceName = Configuration["Redis:Instance"];
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAnyOrigin",
                builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        ;
                });
            });

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowOriginsPolicy",
            //    builder =>
            //    {
            //        builder
            //            .WithOrigins(Configuration.GetSection("CorsOrigins").Get<string[]>())
            //            .AllowAnyHeader()
            //            .AllowAnyMethod()
            //            .AllowCredentials()
            //            ;
            //    });
            //});
        }
        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
               .AddIdentityServerAuthentication(options =>
               {
                   options.Authority = Configuration["IdentityServer:Authority"];
                   options.RequireHttpsMetadata = false;
                   options.ApiName = "resourceApi";
                   options.JwtValidationClockSkew = TimeSpan.Zero;
               })
               .AddApiKeyInHeader<ApiKeyProvider>(options =>
               {
                   options.Realm = "ApiKey";
                   options.KeyName = "API-KEY";   // Your api key name which the clients will require to send the key.
               });


        }
        private void AddNotificationServices(IServiceCollection services)
        {
            services.AddNotificationClientServices(Configuration.GetSection("NotificationServiceSettings"));
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISmsService, InternalSmsService>();
            services.AddTransient<INotificationService, CloudKit.Infrastructure.Notification.NotificationService>();
        }
        private void AddIS4Services(IServiceCollection services)
        {
            services
                .AddIdentity<AppUser, AppRole>(options =>
                {
                    options.Password.RequiredLength = 8;
                    options.SignIn.RequireConfirmedEmail = true;
                    options.User.RequireUniqueEmail = false;
                    options.Tokens.PasswordResetTokenProvider = options.Tokens.ChangeEmailTokenProvider = "ShortLiveTokenProvider";
                })
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders()
                .AddShortLiveTokenProvider();

            services.AddSingleton<IIdentityOptionsProvider, IdentityOptionsProvider>();

            var builder = services.AddIdentityServer(options =>
            {
                options.UserInteraction = new UserInteractionOptions()
                {
                    LogoutUrl = "/account/logout",
                    LoginUrl = "/account/login",
                    LoginReturnUrlParameter = "returnUrl"
                };
                options.IssuerUri = Configuration["IdentityServer:Authority"];
                options.PublicOrigin = Configuration["IdentityServer:Authority"];
            })
            .AddConfigurationStore(options =>
            {
                foreach (var p in options.GetType().GetProperties())
                {
                    if (p.PropertyType == typeof(IdentityServer4.EntityFramework.Options.TableConfiguration))
                    {
                        object o = p.GetGetMethod().Invoke(options, null);
                        PropertyInfo q = o.GetType().GetProperty("Name");

                        string tableName = q.GetMethod.Invoke(o, null) as string;
                        o.GetType().GetProperty("Name").SetMethod.Invoke(o, new object[] { $"idn{tableName}".ToUnderscoreCase() });

                    }
                }

                // Configure DB Context connection string and migrations assembly where migrations are stored  
                options.ConfigureDbContext = builder => builder.UseMySql(
                    Configuration.GetConnectionString("IdentityConnection"),
                    sql => sql.MigrationsAssembly(typeof(AppIdentityDbContext).GetTypeInfo().Assembly.GetName().Name)
                );
            })
            .AddOperationalStore(options =>
            {
                foreach (var p in options.GetType().GetProperties())
                {
                    if (p.PropertyType == typeof(IdentityServer4.EntityFramework.Options.TableConfiguration))
                    {
                        object o = p.GetGetMethod().Invoke(options, null);
                        PropertyInfo q = o.GetType().GetProperty("Name");

                        string tableName = q.GetMethod.Invoke(o, null) as string;
                        o.GetType().GetProperty("Name").SetMethod.Invoke(o, new object[] { $"idn{tableName}".ToUnderscoreCase() });
                    }
                }

                options.EnableTokenCleanup = true;
                // Configure DB Context connection string and migrations assembly where migrations are stored  
                options.ConfigureDbContext = builder => builder.UseMySql(
                    Configuration.GetConnectionString("IdentityConnection"),
                    sql => sql.MigrationsAssembly(typeof(AppIdentityDbContext).GetTypeInfo().Assembly.GetName().Name)
                );
            })
            .AddAspNetIdentity<AppUser>()
            .AddExtensionGrantValidator<ExternalGrantValidator>()
            .AddProfileService<ProfileService>();
            ;

            // not recommended for production - you need to store your key material somewhere secure
            if (Environment.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                var rsaCertificate = new X509Certificate2(
                    Path.Combine(Environment.ContentRootPath, "camunda.pfx"), Configuration["Is4CertificatePassword"]);
                builder.AddSigningCredential(rsaCertificate);
            }
        }
        private static void AddSwaggerServices(IServiceCollection services)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            services.AddSwaggerExamplesFromAssemblies(assemblies);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
                c.CustomSchemaIds(x => x.FullName);

                c.EnableAnnotations();
                c.ExampleFilters();
                c.OperationFilter<AddResponseHeadersFilter>(); // [SwaggerResponseHeader]
                // Set the comments path for the Swagger JSON and UI.
                foreach (var assembly in assemblies)
                {
                    var xmlFile = $"{assembly.GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    if (File.Exists(xmlPath))
                        c.IncludeXmlComments(xmlPath);
                }
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "You have to get token at /connect/token and put it into request header. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT"
                });
                c.AddSecurityDefinition("ApiKey-InHeader", new OpenApiSecurityScheme
                {
                    Description = "You have to put the secret key into header",
                    Name = "API-KEY",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<AuthOperationFilter>();
                c.DocumentFilter<AuthDocumentFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseIpRateLimiting();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<ExceptionHandlerMiddleware>();
            }

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseSwagger();

            if (Configuration["LogRequest"] == "true")
                app.UseiAspNetcoreLogRequest();

            app.UsePathBase(Configuration["PathBase"] ?? "");
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.OAuthClientId("SwaggerClient");
                c.OAuthAppName("Swagger Client");
                c.OAuthScopeSeparator(" ");
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                c.DisplayRequestDuration();
            });

            app.UseStaticFiles();
            //app.UseHttpsRedirection();
            app.UseCors("AllowAnyOrigin");
            app.UseMiddleware<CustomIdentityServerMiddleware>();

            app.UseRouting();
            app.UseMiddleware<SystemIdProviderMiddleware>();
            app.UseIdentityServer();
            app.UseAuthorization();
            app.UseResponseCompression();

            app.UseEndpoints(endpoints =>
            {
                // app
                endpoints.MapDefaultControllerRoute();
            });
        }
    }

    public class CustomContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            if ((property.DeclaringType.IsAssignableFrom(typeof(EntityBase<int>)) || property.DeclaringType.IsAssignableFrom(typeof(EntityBase<Guid>))) && property.PropertyName == nameof(EntityBase<Guid>.IsDeleted))
            {
                property.ShouldSerialize = i => false;
                property.Ignored = true;
            }
            return property;
        }
    }
}
