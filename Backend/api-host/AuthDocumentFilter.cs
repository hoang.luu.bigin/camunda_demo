﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace APIHost
{
    public class AuthDocumentFilter : IDocumentFilter
    {

        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var pathItem = new OpenApiPathItem();
            pathItem.AddOperation(OperationType.Post, new OpenApiOperation()
            {
                Tags = new List<OpenApiTag> { new OpenApiTag() { Name = "Identity" } },
                Summary = "Get Access Token",
                Parameters = new List<OpenApiParameter>()
                { 
                    new OpenApiParameter()
                    {
                        Name = "System-Id",
                        In = ParameterLocation.Header,
                        Required = false
                    }
                },
                RequestBody = new OpenApiRequestBody()
                {
                    Content = new Dictionary<string, OpenApiMediaType>()
                    {
                        ["application/json"] = new OpenApiMediaType()
                        {
                            
                            Schema = new OpenApiSchema()
                            {
                                

                                Properties = new Dictionary<string, OpenApiSchema>()
                                {
                                    ["grant_type"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    },
                                    ["client_id"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    },
                                    ["scope"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    },
                                    ["username"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    },
                                    ["password"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    },
                                    ["external_provider"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    }
                                    ,
                                    ["external_token"] = new OpenApiSchema()
                                    {
                                        Type = "string"
                                    }
                                }
                            },
                            Examples = new Dictionary<string, OpenApiExample>()
                            {
                                ["Password"] = new OpenApiExample()
                                {
                                    Value = new OpenApiObject()
                                    {
                                        ["grant_type"] = new OpenApiString("password"),
                                        ["client_id"] = new OpenApiString("client_id"),
                                        ["scope"] = new OpenApiString("offline_access profile resourceApi"),
                                        ["username"] = new OpenApiString("username"),
                                        ["password"] = new OpenApiPassword("password"),
                                    }
                                },
                                ["External"] = new OpenApiExample()
                                {
                                    Value = new OpenApiObject()
                                    {
                                        ["grant_type"] = new OpenApiString("external"),
                                        ["client_id"] = new OpenApiString("client_id"),
                                        ["scope"] = new OpenApiString("offline_access profile resourceApi"),
                                        ["external_provider"] = new OpenApiString("external_provider"),
                                        ["external_token"] = new OpenApiPassword("external_token"),
                                    }
                                },
                                ["External"] = new OpenApiExample()
                                {
                                    Value = new OpenApiObject()
                                    {
                                        ["grant_type"] = new OpenApiString("external"),
                                        ["client_id"] = new OpenApiString("client_id"),
                                        ["scope"] = new OpenApiString("offline_access profile resourceApi"),
                                        ["external_provider"] = new OpenApiString("facebook"),
                                        ["external_token"] = new OpenApiPassword("external_token"),
                                    }
                                },
                                ["External_Phone_OTP"] = new OpenApiExample()
                                {
                                    Value = new OpenApiObject()
                                    {
                                        ["grant_type"] = new OpenApiString("external"),
                                        ["client_id"] = new OpenApiString("client_id"),
                                        ["scope"] = new OpenApiString("offline_access profile resourceApi"),
                                        ["external_provider"] = new OpenApiString("phoneOtp"),
                                        ["external_token"] = new OpenApiPassword("phone|otpRequestId|otpToken"),
                                    }
                                },
                                ["RefreshToken"] = new OpenApiExample()
                                {
                                    Value = new OpenApiObject()
                                    {
                                        ["grant_type"] = new OpenApiString("refresh_token"),
                                        ["client_id"] = new OpenApiString("client_id"),
                                        ["refresh_token"] = new OpenApiPassword("refresh_token_value"),
                                    }
                                }
                            }
                        }
                    }
                },
                Responses = new OpenApiResponses()
                {
                    ["200"] = new OpenApiResponse()
                    {
                        Description = "Success",
                        Content = new Dictionary<string, OpenApiMediaType>()
                        {
                            ["application/json"] = new OpenApiMediaType()
                            {
                                Schema = new OpenApiSchema()
                                {
                                    Properties = new Dictionary<string, OpenApiSchema>()
                                    {
                                        ["access_token"] = new OpenApiSchema()
                                        {
                                            Type = "string"
                                        },
                                        ["expires_in"] = new OpenApiSchema()
                                        {
                                            Type = "integer"
                                        },
                                        ["token_type"] = new OpenApiSchema()
                                        {
                                            Type = "string",
                                            Example = new OpenApiString("Bearer")
                                        },
                                        ["scope"] = new OpenApiSchema()
                                        {
                                            Type = "string"
                                        },
                                        ["refresh_token"] = new OpenApiSchema()
                                        {
                                            Type = "string"
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ,
                    ["400"] = new OpenApiResponse()
                    {
                        Description = "Bad Request",
                        Content = new Dictionary<string, OpenApiMediaType>()
                        {
                            ["application/json"] = new OpenApiMediaType()
                            {
                                Schema = new OpenApiSchema()
                                {
                                    Properties = new Dictionary<string, OpenApiSchema>()
                                    {
                                        ["error"] = new OpenApiSchema()
                                        {
                                            Type = "string",
                                            Example = new OpenApiString("invalid_client")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            swaggerDoc.Paths.Add("/Connect/Token", pathItem);

            var orderedPaths = swaggerDoc.Paths
                .OrderBy(x => x.Value?.Operations?.FirstOrDefault().Value?.Tags?.FirstOrDefault()?.Name ?? string.Empty)
                .ToList();
            swaggerDoc.Paths.Clear();
            orderedPaths.ForEach(x => swaggerDoc.Paths.Add(x.Key, x.Value));
        }
    }
}
