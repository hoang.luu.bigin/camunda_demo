import i18n from './i18n'
import axios from './axios'
import auth from './auth'
import sentry from './sentry'

export default {
  axios,
  auth,
  i18n,
  sentry,
}
