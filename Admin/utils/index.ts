export const objectUtils = {
  isEmpty(obj: Object) {
    return (
      obj && // 👈 null and undefined check
      Object.keys(obj).length === 0 &&
      obj.constructor === Object
    )
  },
  getUTC(date = '') {
    const d = date ? new Date(date) : new Date()
    return new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()))
  },
  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
      /[xy]/g,
      function (c) {
        const r = (Math.random() * 16) | 0
        const v = c === 'x' ? r : (r & 0x3) | 0x8
        return v.toString(16)
      }
    )
  },
  mongoObjectId() {
    const timestamp = ((new Date().getTime() / 1000) | 0).toString(16)
    return (
      timestamp +
      'xxxxxxxxxxxxxxxx'
        .replace(/[x]/g, function () {
          return ((Math.random() * 16) | 0).toString(16)
        })
        .toLowerCase()
    )
  },
}
