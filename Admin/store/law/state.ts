import { UserObject, UserSearchParamsObject } from '~/models/law'

const state = () =>
  ({
    users: {
      total: 0,
      entities: [],
    },
    usersSearchParams: {
      SearchString: '',
      Status: '',
      Start: 0,
      Length: 12,
      OrderBy: 'updatedAt desc',
    },
    usersCurrentPage: 1,
    usersLoading: false,
    usersOrderBy: {
      field: 'updatedAt',
      order: 'desc',
    },
  } as {
    users: {
      total: number
      entities: UserObject[]
    }
    usersSearchParams: UserSearchParamsObject
    usersCurrentPage: number
    usersLoading: boolean
    usersOrderBy: {
      field: string
      order: string
    }
  })

export type LawState = ReturnType<typeof state>

export default state
