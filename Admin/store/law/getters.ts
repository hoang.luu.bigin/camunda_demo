import { GetterTree } from 'vuex'

import { RootState } from '..'
import { LawState } from './state'

const getters: GetterTree<LawState, RootState> = {
  usersParamsTouched: (state) => {
    return state.usersSearchParams.Status !== ''
  },
}

export default getters
