import { MutationTree } from 'vuex'
import { LawState } from './state'
import { UserObject, UserSearchParamsObject } from '~/models/law'

export enum MUTATIONS {
  SET_USER_TOTAL = 'SET_USER_TOTAL',
  SET_USER_ENTITIES = 'SET_USER_ENTITIES',
  SET_USER_SEARCH_PARAMS = 'SET_USER_SEARCH_PARAMS',
  SET_USER_CURRENT_PAGE = 'SET_USER_CURRENT_PAGE',
  SET_USER_LOADING = 'SET_USER_LOADING',
  SET_USER_ORDER_BY = 'SET_USER_ORDER_BY',
}

const mutations: MutationTree<LawState> = {
  // User management
  [MUTATIONS.SET_USER_TOTAL](state: LawState, payload: number) {
    state.users.total = payload
  },
  [MUTATIONS.SET_USER_ENTITIES](state: LawState, payload: UserObject[]) {
    state.users.entities = payload
  },
  [MUTATIONS.SET_USER_SEARCH_PARAMS](
    state: LawState,
    payload: UserSearchParamsObject
  ) {
    state.usersSearchParams = payload
  },
  [MUTATIONS.SET_USER_CURRENT_PAGE](state: LawState, payload: number) {
    state.usersCurrentPage = payload
  },
  [MUTATIONS.SET_USER_LOADING](state: LawState, payload: boolean) {
    state.usersLoading = payload
  },
  [MUTATIONS.SET_USER_ORDER_BY](
    state: LawState,
    payload: {
      field: string
      order: string
    }
  ) {
    state.usersOrderBy = payload
  },
}

export default mutations
