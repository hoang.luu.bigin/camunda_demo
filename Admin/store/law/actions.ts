import { ActionTree } from 'vuex'
import cloneDeep from 'lodash/cloneDeep'
import { RootState } from '..'
import { LawState } from './state'
import { MUTATIONS } from './mutations'
import { UserObject, UserSearchParamsObject } from '~/models/law'

const actions: ActionTree<LawState, RootState> = {
  // User management
  async fetchUsers({ commit, dispatch, state }) {
    try {
      dispatch('setUserLoading', true)

      const { total, entities } = await this.$services.law.getUsers(
        state.usersSearchParams
      )

      commit(MUTATIONS.SET_USER_TOTAL, total)
      commit(MUTATIONS.SET_USER_ENTITIES, entities)
    } catch (error: any) {
      // eslint-disable-next-line no-console
      console.log('[fetchUsers]', error)
    } finally {
      dispatch('setUserLoading', false)
    }
  },
  setUsersSearchParams({ commit, dispatch }, payload: UserSearchParamsObject) {
    commit(MUTATIONS.SET_USER_SEARCH_PARAMS, payload)
    dispatch('fetchUsers', payload)
  },
  setUsersCurrentPage({ commit, state }, payload: number) {
    const params = cloneDeep(state.usersSearchParams)
    params.Start = params.Length * (payload - 1)
    commit(MUTATIONS.SET_USER_CURRENT_PAGE, payload)
  },
  setUserLoading({ commit }, payload: boolean) {
    commit(MUTATIONS.SET_USER_LOADING, payload)
  },
  async createUser({ dispatch }, payload: UserObject) {
    const rs = await this.$services.law.createUser(payload)
    dispatch('fetchUsers')
    return rs
  },
  async updateUser({ dispatch }, payload: UserObject) {
    const rs = await this.$services.law.updateUser(payload)
    dispatch('fetchUsers')
    return rs
  },
  setUsersOrderBy({ commit }, payload: string) {
    commit(MUTATIONS.SET_USER_ORDER_BY, payload)
  },
}

export default actions
