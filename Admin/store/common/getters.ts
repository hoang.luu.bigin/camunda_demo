import { GetterTree } from 'vuex'

import { RootState } from '..'
import { CommonState } from './state'

const getters: GetterTree<CommonState, RootState> = {}

export default getters
