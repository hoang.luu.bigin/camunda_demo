const state = () =>
  ({
    loading: false,
    nations: [],
  } as {
    loading: boolean
    nations: { key?: string; value?: string }[]
  })

export type CommonState = ReturnType<typeof state>

export default state
