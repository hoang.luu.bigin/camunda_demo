import { ActionTree } from 'vuex'
import { RootState } from '..'
import { CommonState } from './state'
import { MUTATIONS } from './mutations'

const actions: ActionTree<CommonState, RootState> = {
  setLoading({ commit }, payload) {
    commit(MUTATIONS.SET_LOADING, payload)
  },
  async fetchCountries({ commit }) {
    try {
      const payload = await this.$services.common.getCountries()
      commit(
        MUTATIONS.SET_COUNTRIES,
        payload.map((item) => ({
          key: item.code,
          value: item.fullName,
        }))
      )
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('[FetchCountries]', error)
    }
  },
}

export default actions
