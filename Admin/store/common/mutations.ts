import { MutationTree } from 'vuex'
import { CommonState } from './state'

export enum MUTATIONS {
  SET_LOADING = 'SET_LOADING',
  SET_COUNTRIES = 'SET_COUNTRIES',
}

const mutations: MutationTree<CommonState> = {
  [MUTATIONS.SET_LOADING](state: CommonState, payload: boolean) {
    state.loading = payload
  },
  [MUTATIONS.SET_COUNTRIES](state: CommonState, payload: any) {
    state.nations = payload
  },
}

export default mutations
