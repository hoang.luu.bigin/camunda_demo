import { GetterTree, MutationTree, ActionTree } from 'vuex'

export const state = () => ({})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {}

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('common/fetchCountries')
  },
}

export const getters: GetterTree<RootState, RootState> = {}
