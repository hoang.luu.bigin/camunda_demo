import en from '../../../core/modules/i18n/locales/en-US'
import { lawFieldsVuelidate } from './lawFieldsVuelidate'

export const vuelidate = {
  messages: { ...en.vuelidate.messages },
  fields: {
    email: 'Email',
    password: 'Password',
    retypePassword: 'Re-type password',
    currentPassword: 'Current password',
    newPassword: 'New password',
    retypeNewPassword: 'Re-type new password',
    ...lawFieldsVuelidate,
  },
}
