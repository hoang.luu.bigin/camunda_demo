export const lawFieldsVuelidate = {
  name: 'Name',
  message: 'Message',
  type: 'Type',
  scenarioName: 'Scenario Name',
  purpose: 'Purpose',
  nation: 'Country',
  state: 'State',
  county: 'County',
  city: 'City',
  status: 'Status',

  segmentContent: 'Segment Content',
  fromValue: 'From Value',
  toValue: 'To Value',
  value: 'Value',

  code: 'Code',
  target: 'Target',
  dataType: 'Data type',
  inputType: 'Input type',
  operators: 'Operators',
  options: 'Options',

  created: 'Created',
  updated: 'Updated',

  category: 'Category',
  content: 'Content',

  description: 'Description',

  query: 'Query',

  companyName: 'Company name',
  accountEmail: 'Primary account email',
  accountName: 'Primary account name',
  contactNumber: 'Contact number',
  registrationDate: 'Registration date',
  chargeType: 'Charge type',
  validPeriod: 'Valid period',
}
