import vi from '../../../core/modules/i18n/locales/vi-VN'
import { lawFieldsVuelidate } from './lawFieldsVuelidate'

export const vuelidate = {
  messages: { ...vi.vuelidate.messages },
  fields: {
    email: 'Email',
    password: 'Mật khẩu',
    retypePassword: 'Mật khẩu xác nhận',
    currentPassword: 'Mật khẩu hiện tại',
    newPassword: 'Mật khẩu mới',
    retypeNewPassword: 'Mật khẩu mới xác nhận',
    ...lawFieldsVuelidate,
  },
}
