import config from './core/config'

export default {
  head: {
    publicRuntimeConfig: {
      baseURL: process.env.NUXT_ENV_API_URL,
    },
    title: 'Home',
    titleTemplate: '%s - Virgil Admin Portal',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href:
          'https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&display=swap',
      },
    ],
  },
  loading: '~/components/app/AppLoading.vue',
  router: {
    middleware: ['auth'],
  },
  css: ['@/assets/scss/app.scss'],
  plugins: [
    '@/core/plugins/axios.ts',
    '@/core/plugins/vuelidate.ts',
    '@/core/plugins/bigin-ui.ts',
    '@/core/plugins/filter.ts',
    '@/plugins/services.ts',
    '@/plugins/filter.ts',
  ],
  components: true,
  buildModules: ['@nuxt/typescript-build'],
  modules: [
    'nuxt-i18n',
    '@nuxtjs/auth-next',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
  ],
  axios: config.axios,
  auth: config.auth,
  i18n: config.i18n,
  sentry: config.sentry,
  build: {
    transpile: ['@nuxtjs/auth-next', '@bigin/bigin-ui-vue'],
    devMiddleware: {
      headers: {
        'Cache-Control': 'no-store',
        Vary: '*',
      },
    },
    babel: {
      compact: true,
      plugins: [['@babel/plugin-proposal-private-methods', { loose: true }]],
    },
  },
  // styleResources: {
  //   scss: ['@bigin/bigin-ui-ds/src/scss/_settings.scss'],
  // },
}
