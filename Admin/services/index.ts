import { NuxtAxiosInstance } from '@nuxtjs/axios'
import commonService, { CommonService } from './CommonService'
import fileService, { FileService } from './FileService'
import lawService, { LawService } from './LawService'

import identityService, {
  IdentityService,
} from '~/core/services/IdentityService'

export interface ServicesInstance {
  identity: IdentityService
  common: CommonService
  file: FileService
  law: LawService
}

export default ($axios: NuxtAxiosInstance) => ({
  identity: identityService($axios),
  common: commonService($axios),
  file: fileService($axios),
  law: lawService($axios),
})
