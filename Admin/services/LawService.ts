import { NuxtAxiosInstance } from '@nuxtjs/axios'
import qs from 'query-string'
import { UserObject, UserSearchParamsObject } from '~/models/law'

type EntityList<T> = {
  total: number
  entities: T[]
}

export interface LawService {
  getUsers(params: UserSearchParamsObject): Promise<EntityList<UserObject>>
  getUserById(id: string): Promise<UserObject>
  createUser(data: UserObject): Promise<string>
  updateUser(data: UserObject): Promise<string>
  changeUserStatus(data: { id: string; status: boolean }): Promise<boolean>
  checkExistsEmail(email: string): Promise<boolean>
  // deleteUser(id: string): Promise<boolean>

  // camunda api
  getDataList(): Promise<any[]>
  getProcessDefinitionFormVariables(key: string): Promise<any>
  startProcessDefinition(key: string, data: any): Promise<any>
  deleteProcessInstance(id: string): Promise<any>
  getTaskByProcessInstanceIdAndName(
    processInstanceId: string,
    name: string
  ): Promise<any>
  getTaskVariables(id: string): Promise<any>
  submitTask(id: string, data: any): Promise<any>
}

export default ($axios: NuxtAxiosInstance) => ({
  // camunda api
  convertFormData(form: any): any[] {
    console.log('form', form)

    const includes =
      form._includes?.value?.split(',').map((x: string) => x.trim()) || []
    const excludes =
      form._excludes?.value?.split(',').map((x: string) => x.trim()) || []

    const keys = Object.keys(form).filter(
      (key) =>
        (!key.startsWith('_') || includes.includes(key)) &&
        !excludes.includes(key)
    )
    const formData = keys.map((key) => ({
      key,
      type: form[key].type,
      value: form[key].value,
      meta: JSON.parse(form['__' + key]?.value || '{}'),
    }))
    formData.sort(
      (a, b) =>
        (a.meta.index || Number.MAX_VALUE) - (b.meta.index || Number.MAX_VALUE)
    )

    formData.forEach((item) => {
      if (item.meta.options?.length && !item.meta.options[0]?.key) {
        item.meta.options = item.meta.options.map((x: string) => ({
          key: x,
          label: x,
        }))
      }
      if (item.meta.inputType === 'number') item.value = +item.value
      else if (item.meta.inputType === 'date') item.value = new Date(item.value)
      else if (item.meta.inputType === 'checkbox')
        item.value =
          !!item.value && item.value !== 'false' && item.value !== '0'
    })

    console.log('formData', [...formData])

    return formData
  },
  getDataList(): Promise<any[]> {
    return $axios.$get(`/v1/Manage/Camunda`)
  },
  getProcessDefinitionFormVariables(key: string): Promise<any> {
    return $axios.$get(
      `/v1/Manage/Camunda/process-definition/key/${key}/form-variables`
    )
  },
  startProcessDefinition(key: string, data: any): Promise<any> {
    return $axios.$post(
      `/v1/Manage/Camunda/process-definition/key/${key}/submit-form`,
      data
    )
  },
  deleteProcessInstance(id: string): Promise<any> {
    return $axios.$delete(`/v1/Manage/Camunda/process-instance/${id}`)
  },
  getTaskByProcessInstanceIdAndName(
    processInstanceId: string,
    name: string
  ): Promise<any> {
    return $axios.$get(`/v1/Manage/Camunda/task/${processInstanceId}/${name}`)
  },
  getTaskVariables(id: string): Promise<any> {
    return $axios
      .$get(`/v1/Manage/Camunda/task/${id}/form-variables`)
      .then((x) => this.convertFormData(x))
  },
  submitTask(id: string, data: any): Promise<any> {
    return $axios.$post(`/v1/Manage/Camunda/task/${id}/submit-form`, data)
  },

  // User management
  getUsers(params: UserSearchParamsObject): Promise<EntityList<UserObject>> {
    return $axios.$get(`/v1/Identity/Admin?${qs.stringify(params)}`)
  },
  getUserById(id: string): Promise<UserObject> {
    return $axios.$get(`/v1/Identity/Admin/${id}`)
  },
  createUser(data: UserObject): Promise<string> {
    return $axios.$post(`/v1/Identity/Admin`, data)
  },
  updateUser(data: UserObject): Promise<string> {
    return $axios.$put(`/v1/Identity/Admin`, data)
  },
  changeUserStatus(data: { id: string; status: boolean }): Promise<boolean> {
    return $axios.$put(`/v1/Identity/Admin/ChangeStatus`, data)
  },
  checkExistsEmail: (email: string): Promise<boolean> => {
    const awsAxios = $axios.create()
    delete awsAxios.defaults.headers.common.Authorization
    return awsAxios.$get(
      `/v1/Identity/Account/CheckExistsEmail?${qs.stringify({ email })}`,
      {
        headers: { 'System-Id': 1 },
      }
    )
  },
})
