import { NuxtAxiosInstance } from '@nuxtjs/axios'
const fileService =
  process.env.NUXT_ENV_FILES_SERVICE_URL ||
  process.env.NUXT_ENV_API_URL + 'file'

export interface FileService {
  uploadFile<T = any>(file: any): Promise<T>
  uploadFiles<T = any>(files: any): Promise<T>
  uploadPrivateFile<T = any>(file: any): Promise<T>
  uploadPrivateFiles<T = any>(files: any): Promise<T>
}

export default ($axios: NuxtAxiosInstance) => ({
  uploadFile: (file: any): Promise<any> =>
    $axios.$post(fileService + '/Files', file),
  uploadFiles: (files: any): Promise<any> =>
    $axios.$post(fileService + '/Files/Multiple', files),
  uploadPrivateFile: (file: any): Promise<any> =>
    $axios.$post(fileService + '/Me', file),
  uploadPrivateFiles: (files: any): Promise<any> =>
    $axios.$post(fileService + '/Me/Multiple', files),
})
