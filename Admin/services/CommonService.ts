import { NuxtAxiosInstance } from '@nuxtjs/axios'
import qs from 'query-string'
import { CountryObject, LocationObject } from '~/models/common'

export interface CommonService {
  getCountries<T = CountryObject[]>(): Promise<T>
  getLocationLevel1s(CountryCode: string): Promise<LocationObject[]>
  getLocationLevel2s(Level1Code: string): Promise<LocationObject[]>
  getLocationLevel3s(
    Level1Code: string,
    Level2Code: string
  ): Promise<LocationObject[]>
}

export default ($axios: NuxtAxiosInstance) => ({
  getCountries(): Promise<CountryObject[]> {
    return $axios.$get('/v1/Location/GetCountries')
  },
  getLocationLevel1s(CountryCode: string = ''): Promise<LocationObject[]> {
    return $axios.$get(
      `/v1/Location/GetLocationLevel1s?${qs.stringify({ CountryCode })}`
    )
  },
  getLocationLevel2s(Level1Code: string = ''): Promise<LocationObject[]> {
    return $axios.$get(
      `/v1/Location/GetLocationLevel2s?${qs.stringify({ Level1Code })}`
    )
  },
  getLocationLevel3s(
    Level1Code: string = '',
    Level2Code: string = ''
  ): Promise<LocationObject[]> {
    return $axios.$get(
      `/v1/Location/GetLocationLevel3s?${qs.stringify({
        Level1Code,
        Level2Code,
      })}`
    )
  },
})
