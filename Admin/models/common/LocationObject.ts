import { EntityBase } from './EntityBase'

export interface LocationObject extends EntityBase {
  countryCode?: string
  level1Code?: string
  code?: string
  name?: string
  fullName?: string
}
