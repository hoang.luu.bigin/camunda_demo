export class EntityBase<T = string> {
  createdAt?: Date | string | null
  updatedAt?: Date | string | null
  createdBy?: any | null
  updatedBy?: any | null
  isDeleted?: boolean | null
  id?: T | null
}
