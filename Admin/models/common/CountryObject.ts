import { EntityBase } from './EntityBase'

export interface CountryObject extends EntityBase {
  code?: string
  name?: string
  fullName?: string
}
