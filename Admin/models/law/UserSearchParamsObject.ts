export interface UserSearchParamsObject {
  SearchString?: string
  Status?: boolean | string
  Start: number
  Length: number
  OrderBy: string
}
