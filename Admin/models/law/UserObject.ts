import { EntityBase } from '../common'

export class UserObject extends EntityBase {
  avatar?: string
  fullname?: string
  phoneNumber?: string
  email?: string
  isActive?: boolean
}
